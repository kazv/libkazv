find_path(vodozemac_INCLUDE_DIR
  NAMES vodozemac.h
)
find_library(vodozemac_LIBRARY
  NAMES vodozemac
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(vodozemac
  FOUND_VAR vodozemac_FOUND
  REQUIRED_VARS
    vodozemac_LIBRARY
    vodozemac_INCLUDE_DIR
)

if(vodozemac_FOUND AND NOT TARGET vodozemac::vodozemac)
  add_library(vodozemac::vodozemac UNKNOWN IMPORTED)
  set_target_properties(vodozemac::vodozemac PROPERTIES
    IMPORTED_LOCATION "${vodozemac_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${vodozemac_INCLUDE_DIR}"
    )
endif()
