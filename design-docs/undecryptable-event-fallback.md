
# Undecryptable event fallbacks

For events that cannot be decrypted, `event.decrypted()` will be false, `event.type()` will be `m.room.message`, and `event.content().get().at("msgtype")` can have the following values:

## xyz.tusooa.kazv.not.yet.decrypted

This means we haven't even tried to decrypt this event. The most probable case is that we do not have the corresponding session key.

## moe.kazv.mxc.cannot.decrypt

This means that the corresponding session is known, but there are errors in the decryption.

The event content will have the following properties:

- `moe.kazv.mxc.errcode` The error code.
- `moe.kazv.mxc.error` The error message.
- `moe.kazv.mxc.raw` The raw data associated with the error.

### Possible error codes
#### `MOE.KAZV.MXC_DECRYPT_ERROR`

There is an error from libkazvcrypto when decrypting the event.

`moe.kazv.mxc.error` contains the error message.

`moe.kazv.mxc.raw` is always `null`.

#### `M_NOT_JSON`

The decrypted message is not valid json.

`moe.kazv.mxc.error` contains the error message from the json library when parsing the decrypted message.

`moe.kazv.mxc.raw` contains the decrypted message as string.

#### `MOE.KAZV.MXC_DEVICE_KEY_UNKNOWN`

The sender's device key is unknown. Only for olm-encrypted events.

`moe.kazv.mxc.raw` is the decrypted json.

#### `MOE.KAZV.MXC_BAD_SENDER`

The sender specified in the decrypted event does not match the sender in the original event. Only for olm-encrypted events.

`moe.kazv.mxc.raw` is the decrypted json.

#### `MOE.KAZV.MXC_BAD_RECIPIENT`

The recipient specified in the decrypted event does not match the current user. Only for olm-encrypted events.

`moe.kazv.mxc.raw` is the decrypted json.

#### `MOE.KAZV.MXC_BAD_RECIPIENT_KEYS`

The recipient keys specified in the decrypted event do not match the current user. Only for olm-encrypted events.

`moe.kazv.mxc.raw` is the decrypted json.

#### `MOE.KAZV.MXC_BAD_SENDER_KEYS`

The sender keys specified in the decrypted event do not match the sender keys we received from the homeserver. Only for olm-encrypted events.

`moe.kazv.mxc.raw` is the decrypted json.

#### `MOE.KAZV.MXC_BAD_ROOM_ID`

The room id specified in the decrypted event do not match the one we received from the homeserver. Only for megolm-encrypted events.

`moe.kazv.mxc.raw` is the decrypted json.

#### `MOE.KAZV.MXC_UNKNOWN_ALGORITHM`

The encryption algorithm is unknown.

`moe.kazv.mxc.raw` is the decrypted json.

#### `M_BAD_JSON`

The decrypted json does not have a valid format.

`moe.kazv.mxc.error` contains the error message from the json library when accessing required properties (e.g. `at()` or `template get()`).

`moe.kazv.mxc.raw` is the decrypted json.
