
# Using the libkazvfixtures library

We created a fixtures library to provide developers with a way to make mock models with sensible defaults.

All functions lie in the `Kazv::Factory` namespace. The header file to include is `<testfixtures/factory.hpp>`.

```cpp
#include <factory.hpp>

using namespace Kazv::Factory;
```

## The make* factory functions

These functions take a `ComposedModifier<ModelType>`, where `ModelType` is the class of the fixture that you want to create.

A `ComposedModifier<ModelType>` is basically a function that takes a `ModelType &` and modify it, so you can put a lambda here:

```cpp
auto client = makeClient([](ClientModel &m) { m.userId = "@some-user:example.com"; });
// client.userId == "@some-user:example.com"
```

Just as its name suggests, you can compose multiple `ComposedModifier` with `operator|()`, and they will execute in the sequence in which they are composed:

```cpp
auto client = makeClient(
    ComposedModifier<ClientModel>([](ClientModel &m) { m.serverUrl = "tusooa.xyz"; })
    | ComposedModifier<ClientModel>([](ClientModel &m) { m.userId = "@some-user:" + m.serverUrl; })
);
// client.serverUrl == "tusooa.xyz"
// client.userId == "@some-user:tusooa.xyz"
```

A `ComposedModifier<ModelType>` is an `std::function<void(ModelType &)>` but it won't throw when a default-constructed instance is being invoked, instead it would just do nothing, so you can write the following when you do not need any particular tweak:

```cpp
auto client = makeClient({});
```

## ComposedModifiers

To make our life easier, we defined some commonly used modifiers. The most generic one is `withAttr()`, which takes a pointer to a class member and a value. When invoked, it sets that member to the value provided.

```cpp
auto client = makeClient(
    withAttr(&ClientModel::serverUrl, "tusooa.xyz")
    | withAttr(&ClientModel::token, "token")
);
// client.serverUrl == "tusooa.xyz"
// client.token == "token"

auto room = makeRoom(withAttr(&RoomModel::encrypted, true));
// room.encrypted == true
```

Some modifiers add values to a mapping, for example `withRoom()`, `withRoomState()`, `withRoomTimeline()`, `withRoomAccountData()`. They take care of the keys automatically so instead of writing

```cpp
auto room = makeRoom();
auto client = makeClient(
    [room](auto &m) { m.roomList.rooms = m.roomList.rooms.set(room.roomId, room); }
);
```

you can just do

```cpp
auto client = makeClient(
    withRoom(makeRoom({}))
);
```
