
# Change log

## 0.8.0

### Security
- Prevent session replacement attack. https://iron.lily-is.land/D202 by tusooa

### Added
- Add Room::localEcho and related test. https://iron.lily-is.land/D156 by nannanko
- Port InboundGroupSession to vodozemac. https://iron.lily-is.land/D159 by tusooa
- Port OutboundGroupsession to vodozemac. https://iron.lily-is.land/D160 by tusooa
- Port Crypto and Session to vodozemac. https://iron.lily-is.land/D161 by tusooa
- Add function to get the original m.relates_to object of events. https://iron.lily-is.land/D193 by tusooa

### Fixed
- Do not add an optional null json value to request body. https://iron.lily-is.land/D162 by tusooa
- Fix gap incorrectly introduced when limited field is missing from sync. https://iron.lily-is.land/D163 by tusooa
- Make invalid megolm sessions copyable. https://iron.lily-is.land/D164 by tusooa
- Fix room account data not emitted properly. https://iron.lily-is.land/D165 by tusooa
- Ensure fallback has been inserted when decrypt failure occurs. https://iron.lily-is.land/D173 by tusooa
- Set cryptopp as private link lib for libkazvcrypto. https://iron.lily-is.land/D184 by tusooa
- Fix stuck on generating one-time keys. https://iron.lily-is.land/D186 by tusooa
- Update local read marker before posting read receipt. https://iron.lily-is.land/D204 by tusooa

### Internal changes
- Add generate-libolm-crypto-dump tool. https://iron.lily-is.land/D157 by tusooa
- Find vodozemac as a dependency. https://iron.lily-is.land/D158 by tusooa
- Add junit report. https://lily-is.land/kazv/libkazv/-/merge_requests/79 by tusooa
- Ask lager and immer to always use exceptions. https://iron.lily-is.land/D185 by tusooa
- Use podman to build images and enable ccache. https://lily-is.land/kazv/libkazv/-/merge_requests/81 by tusooa
- Add test to verify it accepts a room key event. https://iron.lily-is.land/D203 by tusooa

## 0.7.0

### Security
- Leave the lower 8 bytes as 0 in iv of AES256CTRDesc::fromRandom(). https://iron.lily-is.land/D153 by tusooa

### Added
- Add hard logout. https://lily-is.land/kazv/libkazv/-/merge_requests/78 by April Simone
- Implement calculating local notifications. https://iron.lily-is.land/D106 by tusooa
- Update local notifications when syncing. https://iron.lily-is.land/D107 by tusooa
- Add Room::pinEvents() and Room::unpinEvents(). https://iron.lily-is.land/D130 by tusooa
- Add initialState to Client::createRoom(). https://iron.lily-is.land/D138 by tusooa

### Fixed
- Fix kazvtestfixtures withEventSenderId(). https://iron.lily-is.land/D105 by tusooa
- Update tutorial 0 to reflect thread-safety problems. https://iron.lily-is.land/D115 by tusooa

### Removed
- Remove ci builds for ubuntu 20.04. https://iron.lily-is.land/D95 by tusooa

### Internal changes
- Add -fexceptions to kazvbase compile options. https://iron.lily-is.land/D101 by tusooa
- Set cmake indent to 2 in .editorconfig. https://iron.lily-is.land/D102 by tusooa
- Add docs for action-mock-utils.hpp. https://iron.lily-is.land/D113 by tusooa
- Delete TWIM entries. https://iron.lily-is.land/D128 by tusooa
- Update and reformat docs. https://iron.lily-is.land/D134 by tusooa
- Rewrite architecture documentation. https://iron.lily-is.land/D135 by tusooa

## 0.6.0

### Added
- Build on ubuntu 22.04 in CI. https://iron.lily-is.land/D75
- Calculate a map from session id to undecrypted event ids in Room. https://iron.lily-is.land/D67
- Create function Crypto::hasInboundGroupSession(). https://iron.lily-is.land/D68

### Fixed
- Fix possibly dangling reference to a temporary. https://iron.lily-is.land/D61
- Remove sender key from KeyOfGroupSession. https://iron.lily-is.land/D69
- Do not verify sender key and sender device id for megolm events. https://iron.lily-is.land/D70
- Make tryDecryptEvents() use and update Room::undecryptedEvents. https://iron.lily-is.land/D71
- Do not reveal session key when encrypting an event. https://iron.lily-is.land/D74
- Amend room state when paginating back. https://iron.lily-is.land/D85

### Removed
- Remove AppendTimelineAction and PrependTimelineAction. https://iron.lily-is.land/D62
- Remove EncryptOlmEventAction and ClientModel::olmEncrypt(). https://iron.lily-is.land/D63

### Internal changes
- Create a mock helper for contexts. https://iron.lily-is.land/D64
- Visualize coverage report in pipeline. https://iron.lily-is.land/D73

## 0.5.0

### Added
- Add action to send different to-device messages to multiple devices. https://iron.lily-is.land/D25
- Support encrypting olm separately for each device. https://iron.lily-is.land/D26
- Send only relevant ciphertext to each device when sharing room keys. https://iron.lily-is.land/D27

### Internal changes
- Fix not pushing code for branch commits. https://iron.lily-is.land/D30
- Exclude tests, testfixtures and examples from coverage report. https://iron.lily-is.land/D31
- Split each test into its own executable. https://iron.lily-is.land/D35

## 0.4.0

### Security
- Do not calculate transaction id from event content. https://iron.lily-is.land/D28

### Added
- Implement removing local echo. https://lily-is.land/kazv/libkazv/-/merge_requests/70
- Add constant-time cursors for room timeline. https://lily-is.land/kazv/libkazv/-/merge_requests/72
- Support getting read receipts. https://lily-is.land/kazv/libkazv/-/merge_requests/73
- Add reader for related events in Room. https://lily-is.land/kazv/libkazv/-/merge_requests/74
- Add heroMemberEvents function to RoomModel. https://iron.lily-is.land/D9
- Support posting read receipts. https://iron.lily-is.land/D15

### Fixed
- Fix an event event shown not decrypted when it is being sent. https://lily-is.land/kazv/libkazv/-/merge_requests/71
- Fix PostReceiptAction sending out a null json body. https://iron.lily-is.land/D16

### Removed
- Remove unused util.hpp. https://iron.lily-is.land/D8

### Internal changes
- Rework on code review process. https://lily-is.land/kazv/libkazv/-/merge_requests/75

## 0.3.1

### Fixed
- Make it build under windows. https://lily-is.land/kazv/libkazv/-/merge_requests/68g

## 0.3.0

### Added
- Error handling for async functions. https://lily-is.land/kazv/libkazv/-/merge_requests/21
- Add device management functions to Client. https://lily-is.land/kazv/libkazv/-/merge_requests/29
- Allow Sdk::createSecondaryRoot() to have its initial value. https://lily-is.land/kazv/libkazv/-/merge_requests/31
- Add local echo support. https://lily-is.land/kazv/libkazv/-/merge_requests/33
- Recalculate which megolm sessions need to be rotated after changing trust level settings. https://lily-is.land/kazv/libkazv/-/merge_requests/34
- Implement redactions. https://lily-is.land/kazv/libkazv/-/merge_requests/36
- Update to csapi v1.3. https://lily-is.land/kazv/libkazv/-/merge_requests/40
- Add room tagging functionality. https://lily-is.land/kazv/libkazv/-/merge_requests/41
- Implement getting member events of typing users in a room. https://lily-is.land/kazv/libkazv/-/merge_requests/42
- Implement getting and setting account data. https://lily-is.land/kazv/libkazv/-/merge_requests/44
- Handle invite state. https://lily-is.land/kazv/libkazv/-/merge_requests/46
- Add getter for individual room message. https://lily-is.land/kazv/libkazv/-/merge_requests/47
- Add libkazvtestfixtures library. https://lily-is.land/kazv/libkazv/-/merge_requests/51
- Add cursor for joined member events. https://lily-is.land/kazv/libkazv/-/merge_requests/54
- Track event relationships. https://lily-is.land/kazv/libkazv/-/merge_requests/55
- Implement a minimum set of push rules. https://lily-is.land/kazv/libkazv/-/merge_requests/58
- Implement power levels handling. https://lily-is.land/kazv/libkazv/-/merge_requests/59
- Add functions about get member ids and events by membership. https://lily-is.land/kazv/libkazv/-/merge_requests/62
- Make Client::stopSyncing() return a Promise. https://lily-is.land/kazv/libkazv/-/merge_requests/64
- Add preset param to Client::createRoom(). https://lily-is.land/kazv/libkazv/-/merge_requests/66

### Fixed
- Make compile under gcc12. https://lily-is.land/kazv/libkazv/-/merge_requests/23
- Will now compile with Catch2-3. https://lily-is.land/kazv/libkazv/-/merge_requests/24
- Fix Room::encrypted not implemented. https://lily-is.land/kazv/libkazv/-/merge_requests/25
- Fix joining room with room alias. https://lily-is.land/kazv/libkazv/-/merge_requests/28
- Do not run tests that require Internet in pipeline. https://lily-is.land/kazv/libkazv/-/merge_requests/35
- Get the correct room avatar when set. https://lily-is.land/kazv/libkazv/-/merge_requests/43
- Fix pagination. https://lily-is.land/kazv/libkazv/-/merge_requests/45
- Make work with cpr-1.10. https://lily-is.land/kazv/libkazv/-/merge_requests/48
- Fix log spamming and session key revealing. https://lily-is.land/kazv/libkazv/-/merge_requests/50
- Fix store test with ASAN. https://lily-is.land/kazv/libkazv/-/merge_requests/52
- Fix AES256CTRDesc use-after-free error. https://lily-is.land/kazv/libkazv/-/merge_requests/53
- Avoid creation of multiple room cursors. https://lily-is.land/kazv/libkazv/-/merge_requests/57
- Minimize the copying of Crypto to improve performance. https://lily-is.land/kazv/libkazv/-/merge_requests/61
- Include `<random>` properly in random-generator.hpp. https://lily-is.land/kazv/libkazv/-/merge_requests/63
- Improve reducer performance by using immer::diff to compare device lists. https://lily-is.land/kazv/libkazv/-/merge_requests/65

## 0.2.1

- Fix crash when receiving a redacted encrypted event. https://lily-is.land/kazv/libkazv/-/commit/98870fa04fa78361f5092cd77f88895e4a228d34

## 0.2.0

- Support streaming uploads. https://gitlab.com/kazv/libkazv/-/merge_requests/1
- Fix ctx.dispatch() returning promises resolving to empty EffectStatus. https://gitlab.com/kazv/libkazv/-/commit/c7796a6ab8325773bf47fe83254a377204d6abcf
- Prevent leaking full file path when uploading to matrix server by using only basename for the request.
- Deal with timeline gaps properly. https://lily.kazv.moe/kazv/libkazv/-/merge_requests/1
- Record state events in timeline. https://lily.kazv.moe/kazv/libkazv/-/merge_requests/2
- Add support for streaming download. https://lily.kazv.moe/kazv/libkazv/-/merge_requests/3
- Add support for Boost.Serialization. https://lily.kazv.moe/kazv/libkazv/-/merge_requests/4
- Support encrypted attachments. https://lily.kazv.moe/kazv/libkazv/-/merge_requests/5
- Allow custom random generator with crypto. https://lily.kazv.moe/kazv/libkazv/-/merge_requests/6
- Support auto-discovery. https://lily.kazv.moe/kazv/libkazv/-/merge_requests/12
- Support profile API. https://lily.kazv.moe/kazv/libkazv/-/merge_requests/13
- Support kick/ban API. https://lily-is.land/kazv/libkazv/-/merge_requests/15
- Support room heroes. https://lily-is.land/kazv/libkazv/-/merge_requests/16
- Add coverage report. https://lily-is.land/kazv/libkazv/-/merge_requests/17

### Deprecated

- `makeDefaultEncryptedSdk()` is now deprecated. Use `makeDefaultSdkWithCryptoRandom()` instead.
