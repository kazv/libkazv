/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>


#include "outbound-group-session-p.hpp"
#include "crypto-util-p.hpp"
#include <debug.hpp>
#include "time-util.hpp"

namespace Kazv
{
    OutboundGroupSessionPrivate::OutboundGroupSessionPrivate()
        : session(std::nullopt)
    {
    }

    OutboundGroupSessionPrivate::OutboundGroupSessionPrivate(
        RandomTag,
        [[maybe_unused]] RandomData random,
        Timestamp creationTime)
        : session(std::nullopt)
        , creationTime(creationTime)
    {
        session = checkVodozemacError([&]() {
            return vodozemac::megolm::new_group_session();
        });

        if (session.has_value()) {
            valid = true;
            initialSessionKey = sessionKey();
        }
    }

    OutboundGroupSessionPrivate::OutboundGroupSessionPrivate(const OutboundGroupSessionPrivate &that)
        : session(std::nullopt)
        , creationTime(that.creationTime)
        , initialSessionKey(that.initialSessionKey)
    {
        if (that.valid) {
            valid = unpickle(that.pickle());
        }
    }

    std::string OutboundGroupSessionPrivate::pickle() const
    {
        auto pickleData = session.value()->pickle(VODOZEMAC_PICKLE_KEY);
        return static_cast<std::string>(pickleData);
    }

    bool OutboundGroupSessionPrivate::unpickle(std::string pickleData)
    {
        session = checkVodozemacError([&]() {
            return vodozemac::megolm::group_session_from_pickle(pickleData, VODOZEMAC_PICKLE_KEY);
        });
        return session.has_value();
    }

    bool OutboundGroupSessionPrivate::unpickleFromLibolm(std::string pickleData)
    {
        session = checkVodozemacError([&]() {
            return vodozemac::megolm::group_session_from_libolm_pickle(pickleData, rust::Slice<const unsigned char>(OLM_PICKLE_KEY.data(), OLM_PICKLE_KEY.size()));
        });
        return session.has_value();
    }

    std::size_t OutboundGroupSession::constructRandomSize()
    {
        return 0;
    }

    OutboundGroupSession::OutboundGroupSession()
        : m_d(new OutboundGroupSessionPrivate)
    {
    }

    OutboundGroupSession::OutboundGroupSession(RandomTag, RandomData random, Timestamp creationTime)
        : m_d(new OutboundGroupSessionPrivate(RandomTag{}, std::move(random), std::move(creationTime)))
    {
    }


    OutboundGroupSession::~OutboundGroupSession() = default;

    OutboundGroupSession::OutboundGroupSession(const OutboundGroupSession &that)
        : m_d(new OutboundGroupSessionPrivate(*that.m_d))
    {
    }

    OutboundGroupSession::OutboundGroupSession(OutboundGroupSession &&that)
        : m_d(std::move(that.m_d))
    {
    }

    OutboundGroupSession &OutboundGroupSession::operator=(const OutboundGroupSession &that)
    {
        m_d.reset(new OutboundGroupSessionPrivate(*that.m_d));
        return *this;
    }

    OutboundGroupSession &OutboundGroupSession::operator=(OutboundGroupSession &&that)
    {
        m_d = std::move(that.m_d);
        return *this;
    }

    bool OutboundGroupSession::valid() const
    {
        return m_d && m_d->valid;
    }

    std::string OutboundGroupSession::encrypt(std::string plainText)
    {
        auto res = checkVodozemacError([&]() {
            return m_d->session.value()->encrypt(rust::Str(plainText));
        });

        if (!res.has_value()) {
            return std::string();
        }

        return static_cast<std::string>(res.value()->to_base64());
    }

    std::string OutboundGroupSessionPrivate::sessionKey()
    {
        auto key = session.value()->session_key()->to_base64();

        return static_cast<std::string>(key);
    }

    std::string OutboundGroupSession::sessionKey()
    {
        return m_d->sessionKey();
    }

    std::string OutboundGroupSession::initialSessionKey() const
    {
        return m_d->initialSessionKey;
    }

    std::string OutboundGroupSession::sessionId()
    {
        auto id = m_d->session.value()->session_id();
        return static_cast<std::string>(id);
    }

    int OutboundGroupSession::messageIndex()
    {
        return m_d->session.value()->message_index();
    }

    Timestamp OutboundGroupSession::creationTimeMs() const
    {
        return m_d->creationTime;
    }

    void to_json(nlohmann::json &j, const OutboundGroupSession &s)
    {
        j = nlohmann::json::object();
        j["version"] = 1;
        j["valid"] = s.m_d->valid;
        j["creationTime"] = s.m_d->creationTime;
        j["initialSessionKey"] = s.m_d->initialSessionKey;
        if (s.m_d->valid) {
            j["session"] = s.m_d->pickle();
        }
    }

    void from_json(const nlohmann::json &j, OutboundGroupSession &s)
    {
        s.m_d->valid = j.at("valid");
        s.m_d->creationTime = j.at("creationTime");
        s.m_d->initialSessionKey = j.at("initialSessionKey");
        if (s.m_d->valid) {
            if (j.contains("version") && j["version"] == 1) {
                s.m_d->valid = s.m_d->unpickle(j.at("session"));
            } else {
                s.m_d->valid = s.m_d->unpickleFromLibolm(j.at("session"));
            }
        }
    }

}
