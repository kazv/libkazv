/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <debug.hpp>

#include "crypto-p.hpp"
#include "session-p.hpp"


namespace Kazv
{
    SessionPrivate::SessionPrivate()
        : session(std::nullopt)
    {
    }

    SessionPrivate::SessionPrivate(OutboundSessionTag,
                                   RandomTag,
                                   RandomData random,
                                   CryptoPrivate &cryptoD,
                                   std::string theirIdentityKey,
                                   std::string theirOneTimeKey)
        : SessionPrivate()
    {
        assert(random.size() >= Session::constructOutboundRandomSize());

        session = checkVodozemacError([&]() {
            auto identityKey = vodozemac::types::curve_key_from_base64(rust::Str(theirIdentityKey));
            auto oneTimeKey = vodozemac::types::curve_key_from_base64(rust::Str(theirOneTimeKey));
            return cryptoD.account.value()->create_outbound_session(
                *identityKey,
                *oneTimeKey
            );
        });

        valid = session.has_value();
    }

    SessionPrivate::SessionPrivate(InboundSessionTag,
                   CryptoPrivate &cryptoD,
                   std::string theirIdentityKey,
                   std::string message)
        : SessionPrivate()
    {
        auto res = checkVodozemacError([&]() {
            auto identityKey = vodozemac::types::curve_key_from_base64(rust::Str(theirIdentityKey));
            auto msg = vodozemac::olm::olm_message_from_parts(vodozemac::olm::OlmMessageParts{
                    0, // pre-key message
                    message,
                });
            return cryptoD.account.value()->create_inbound_session(
                *identityKey,
                *msg
            );
        });

        valid = res.has_value();

        if (valid) {
            session = std::move(res->session);
            firstDecrypted = std::string(res->plaintext.begin(), res->plaintext.end());
        }
    }

    SessionPrivate::SessionPrivate(const SessionPrivate &that)
        : SessionPrivate()
    {
        if (that.valid) {
            valid = unpickle(that.pickle());
        }
    }

    std::string SessionPrivate::pickle() const
    {
        auto pickleData = session.value()->pickle(VODOZEMAC_PICKLE_KEY);
        return static_cast<std::string>(pickleData);
    }

    bool SessionPrivate::unpickle(std::string pickleData)
    {
        session = checkVodozemacError([&]() {
            return vodozemac::olm::session_from_pickle(pickleData, VODOZEMAC_PICKLE_KEY);
        });

        return session.has_value();
    }

    bool SessionPrivate::unpickleFromLibolm(std::string pickleData)
    {
        session = checkVodozemacError([&]() {
            return vodozemac::olm::session_from_libolm_pickle(
                pickleData,
                rust::Slice<const unsigned char>(OLM_PICKLE_KEY.data(), OLM_PICKLE_KEY.size()));
        });

        return session.has_value();
    }

    MaybeString SessionPrivate::takeFirstDecrypted()
    {
        auto res = std::move(firstDecrypted);
        firstDecrypted = std::nullopt;
        if (res.has_value()) {
            return res.value();
        } else {
            return NotBut("No first decrypted available");
        }
    }

    std::size_t Session::constructOutboundRandomSize()
    {
        return 0;
    }

    Session::Session()
        : m_d(new SessionPrivate)
    {
    }

    Session::Session(OutboundSessionTag,
                     RandomTag,
                     RandomData data,
                     CryptoPrivate &cryptoD,
                     std::string theirIdentityKey,
                     std::string theirOneTimeKey)
        : m_d(new SessionPrivate{
                OutboundSessionTag{},
                RandomTag{},
                std::move(data),
                cryptoD,
                theirIdentityKey,
                theirOneTimeKey})
    {
    }

    Session::Session(InboundSessionTag,
                     CryptoPrivate &cryptoD,
                     std::string theirIdentityKey,
                     std::string theirOneTimeKey)
        : m_d(new SessionPrivate{
                InboundSessionTag{},
                cryptoD,
                theirIdentityKey,
                theirOneTimeKey})
    {
    }

    Session::~Session() = default;

    Session::Session(const Session &that)
        : m_d(new SessionPrivate(*that.m_d))
    {
    }

    Session::Session(Session &&that)
        : m_d(std::move(that.m_d))
    {
    }

    Session &Session::operator=(const Session &that)
    {
        m_d.reset(new SessionPrivate(*that.m_d));
        return *this;
    }

    Session &Session::operator=(Session &&that)
    {
        m_d = std::move(that.m_d);
        return *this;
    }


    bool Session::matches(std::string message)
    {
        auto res = checkVodozemacError([&]() {
            auto msg = vodozemac::olm::olm_message_from_parts(vodozemac::olm::OlmMessageParts{
                    0, // pre-key
                    message,
                });
            return m_d->session.value()->session_matches(*msg);
        });

        return res.has_value() && res.value();
    }

    bool Session::valid() const
    {
        // maybe a moved-from state, so check m_d first
        return m_d && m_d->valid;
    }

    MaybeString Session::decrypt(int type, std::string message)
    {
        auto res = checkVodozemacError([&]() {
            auto msg = vodozemac::olm::olm_message_from_parts(vodozemac::olm::OlmMessageParts{
                    static_cast<std::size_t>(type),
                    message,
                });
            return m_d->session.value()->decrypt(*msg);
        });

        if (!res.has_value()) {
            return NotBut(res.reason());
        }

        return std::string(res.value().begin(), res.value().end());
    }

    std::size_t Session::encryptRandomSize() const
    {
        return 0;
    }

    std::pair<int, std::string> Session::encryptWithRandom(RandomData random, std::string plainText)
    {
        assert(random.size() >= encryptRandomSize());

        auto res = checkVodozemacError([&]() {
            return m_d->session.value()->encrypt(plainText);
        });

        if (!res.has_value()) {
            return { -1, "" };
        }

        auto [type, msg] = res.value()->to_parts();

        return { type, static_cast<std::string>(msg) };
    }

    void to_json(nlohmann::json &j, const Session &s)
    {
        j = nlohmann::json::object({
                {"valid", s.m_d->valid},
                {"version", 1},
                {"data", s.m_d->valid ? s.m_d->pickle() : std::string()}
            });
    }

    void from_json(const nlohmann::json &j, Session &s)
    {
        if (j.at("valid").template get<bool>()) {
            if (j.contains("version") && j["version"] == 1) {
                s.m_d->valid = s.m_d->unpickle(j.at("data"));
            } else {
                s.m_d->valid = s.m_d->unpickleFromLibolm(j.at("data"));
            }
        }
    }

}
