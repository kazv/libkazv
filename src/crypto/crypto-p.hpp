/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <vodozemac.h>

#include <unordered_map>

#include "crypto.hpp"
#include "crypto-util.hpp"
#include "crypto-util-p.hpp"
#include "session.hpp"
#include "inbound-group-session.hpp"
#include "outbound-group-session.hpp"

namespace Kazv
{
    using SessionList = std::vector<Session>;

    struct CryptoPrivate
    {
        CryptoPrivate();
        CryptoPrivate(RandomTag, RandomData data);
        CryptoPrivate(const CryptoPrivate &that);
        ~CryptoPrivate();

        std::optional<rust::Box<vodozemac::olm::Account>> account;
        immer::map<std::string /* algorithm */, int> uploadedOneTimeKeysCount;
        int numUnpublishedKeys{0};
        std::unordered_map<std::string /* theirCurve25519IdentityKey */, Session> knownSessions;
        std::unordered_map<KeyOfGroupSession, InboundGroupSession> inboundGroupSessions;

        std::unordered_map<std::string /* roomId */, OutboundGroupSession> outboundGroupSessions;

        bool valid{true};

        std::string pickle() const;
        bool unpickle(std::string data);
        bool unpickleFromLibolm(std::string data);

        std::string ed25519IdentityKey() const;
        std::string curve25519IdentityKey() const;

        MaybeString decryptOlm(nlohmann::json content);
        // Here we need the full event for eventId and originServerTs
        MaybeString decryptMegOlm(nlohmann::json eventJson);

        /// returns whether the session is successfully established
        bool createInboundSession(std::string theirCurve25519IdentityKey,
                                  std::string message);

        bool createInboundGroupSession(KeyOfGroupSession k, std::string sessionKey, std::string ed25519Key);

        bool reuseOrCreateOutboundGroupSession(RandomData random, Timestamp timeMs,
                                               std::string roomId, std::optional<MegOlmSessionRotateDesc> desc);
    };

}
