/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>
#include <array>
#include <vector>
#include <cstdint>
#include <maybe.hpp>

namespace Kazv
{
    static const std::array<std::uint8_t, 32> VODOZEMAC_PICKLE_KEY = {};
    static const std::vector<std::uint8_t> OLM_PICKLE_KEY = {'x', 'x', 'x'};

    template<class Func>
    static auto checkVodozemacError(Func &&func) -> Maybe<std::invoke_result_t<Func>>
    {
        try {
            return std::forward<Func>(func)();
        } catch (const std::exception &e) {
            return NotBut(e.what());
        }
    }
}
