/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include "session.hpp"
#include <vodozemac.h>

namespace Kazv
{
    struct SessionPrivate
    {
        SessionPrivate();

        SessionPrivate(OutboundSessionTag,
                       RandomTag,
                       RandomData data,
                       CryptoPrivate &cryptoD,
                       std::string theirIdentityKey,
                       std::string theirOneTimeKey);

        SessionPrivate(InboundSessionTag,
                       CryptoPrivate &cryptoD,
                       std::string theirIdentityKey,
                       std::string message);

        SessionPrivate(const SessionPrivate &that);
        ~SessionPrivate() = default;

        std::optional<rust::Box<vodozemac::olm::Session>> session;

        bool valid{false};
        std::optional<std::string> firstDecrypted;

        std::string pickle() const;
        bool unpickle(std::string data);
        bool unpickleFromLibolm(std::string data);

        MaybeString takeFirstDecrypted();
    };
}
