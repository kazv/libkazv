/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2021 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>


#include <cryptopp/aes.h>
#include <cryptopp/ccm.h>

#include "base64.hpp"

#include "aes-256-ctr.hpp"

namespace Kazv
{
    struct AES256CTRDesc::Private
    {
        using ImplT = CryptoPP::CTR_Mode<CryptoPP::AES>::Encryption;

        Private() : valid(false) {}

        Private(DataT keyRaw, DataT ivRaw)
            : keyRaw(keyRaw)
            , ivRaw(ivRaw)
        {
            if (keyRaw.size() != keySize || ivRaw.size() != ivSize) {
                valid = false;
            } else {
                valid = true;
                key = encodeBase64(keyRaw, Base64Opts::urlSafe);
                iv = encodeBase64(ivRaw);
                algo.SetKeyWithIV(
                    reinterpret_cast<const unsigned char *>(keyRaw.data()),
                    keyRaw.size(),
                    reinterpret_cast<const unsigned char *>(ivRaw.data())
                );
            }
        }

        Private(const Private &that)
            : valid(that.valid)
            , key(that.key)
            , iv(that.iv)
            , keyRaw(that.keyRaw)
            , ivRaw(that.ivRaw)
            , position(that.position)
            , algo()
        {
            if (valid) {
                // HACK: CryptoPP's cipher does not support copying, even though the compiler won't complain if I call its copy constructor.
                // ASAN will give out a use-after-free error if the original is destroyed. (e.g. by assigning it another value)
                // https://matrix.to/#/!BEyHIhspTdqZpzmHrz:tusooa.xyz/$o1FNH4Pvmgoq2QYf6aQK0RH2r6V8SlqnBrMkjN5N2Uk?via=tusooa.xyz&via=matrix.org&via=mozilla.org
                algo.SetKeyWithIV(
                    reinterpret_cast<const unsigned char *>(keyRaw.data()),
                    keyRaw.size(),
                    reinterpret_cast<const unsigned char *>(ivRaw.data())
                );
                algo.Seek(position);
            }
        }

        bool valid{true};
        std::string key;
        std::string iv;
        DataT keyRaw;
        DataT ivRaw;
        std::size_t position{};
        ImplT algo;
    };

    AES256CTRDesc::AES256CTRDesc(RawTag, DataT key, DataT iv)
        : m_d(std::make_unique<Private>(key, iv))
    {
    }


    AES256CTRDesc::AES256CTRDesc(std::string key, std::string iv)
        : AES256CTRDesc(RawTag{}, decodeBase64(key, Base64Opts::urlSafe), decodeBase64(iv))
    {
    }

    KAZV_DEFINE_COPYABLE_UNIQUE_PTR(AES256CTRDesc, m_d)

    AES256CTRDesc::~AES256CTRDesc() = default;

    bool AES256CTRDesc::valid() const
    {
        return m_d && m_d->valid;
    }

    std::string AES256CTRDesc::key() const
    {
        return m_d->key;
    }

    std::string AES256CTRDesc::iv() const
    {
        return m_d->iv;
    }

    auto AES256CTRDesc::process(DataT data) const & -> Result
    {
        auto next = *this;
        auto res = next.processInPlace(data);
        return { std::move(next), std::move(res) };
    }

    auto AES256CTRDesc::process(DataT data) && -> Result
    {
        auto next = std::move(*this);
        auto res = next.processInPlace(data);
        return { std::move(next), std::move(res) };
    }

    auto AES256CTRDesc::processInPlace(DataT data) -> DataT
    {
        m_d->position += data.size();
        m_d->algo.ProcessString(reinterpret_cast<unsigned char *>(data.data()), data.size());
        return data;
    }
}
