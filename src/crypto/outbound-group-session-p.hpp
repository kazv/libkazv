/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include "outbound-group-session.hpp"

#include <vodozemac.h>

#include <immer/map.hpp>

namespace Kazv
{
    struct OutboundGroupSessionPrivate
    {
        /// to be deprecated
        OutboundGroupSessionPrivate();
        OutboundGroupSessionPrivate(RandomTag,
                                    RandomData random,
                                    Timestamp creationTime);
        OutboundGroupSessionPrivate(const OutboundGroupSessionPrivate &that);
        ~OutboundGroupSessionPrivate() = default;

        std::optional<rust::Box<vodozemac::megolm::GroupSession>> session;

        bool valid{false};

        Timestamp creationTime;

        std::string initialSessionKey;

        std::string pickle() const;
        bool unpickle(std::string pickleData);
        bool unpickleFromLibolm(std::string pickleData);

        std::string sessionKey();
    };

}
