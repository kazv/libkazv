/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include "libkazv-config.hpp"

#include <optional>
#include <functional>
#include <nlohmann/json.hpp>

namespace Kazv
{
    template<class Jsonish, class Key, class Func>
    bool hasAtThat(Jsonish &&j, Key &&k, Func &&f)
    {
        return j.contains(k)
            && std::invoke(std::forward<Func>(f), std::forward<Jsonish>(j)[std::forward<Key>(k)]);
    }

    // https://spec.matrix.org/v1.8/client-server-api/#conditions-1
    // non-compound canonical JSON values: strings, integers in the range of [-(2**53)+1, (2**53)-1], booleans, and null.
    inline bool isNonCompoundCanonicalJsonValue(const nlohmann::json &j)
    {
        return j.is_string()
            || (j.is_number_integer()
                && j.template get<long long>() <= 9007199254740991LL
                && j.template get<long long>() >= -9007199254740991LL)
            || j.is_boolean()
            || j.is_null();
    }

    template<class Jsonish, class RangeT>
    std::optional<std::decay_t<Jsonish>> getInJson(Jsonish &&j, RangeT &&path, std::size_t index = 0)
    {
        if (index >= path.size()) {
            return std::forward<Jsonish>(j);
        }

        auto currentSegment = path[index];
        if (j.is_object() && j.contains(currentSegment)) {
            return getInJson(std::forward<Jsonish>(j)[currentSegment], std::forward<RangeT>(path), index + 1);
        }

        return std::nullopt;
    }
}
