/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include "libkazv-config.hpp"

#include <tuple>

namespace Kazv
{
    template<class T>
    [[nodiscard]] auto getIn(T &&item)
    {
        return std::forward<T>(item);
    }

    template<class ImmerT, class K, class ...Keys>
    [[nodiscard]] auto getIn(ImmerT &&container, K &&key, Keys &&...keys)
    {
        return getIn(std::forward<ImmerT>(container)[std::forward<K>(key)], std::forward<Keys>(keys)...);
    }

    template<class T>
    [[nodiscard]] auto setIn(T, T newVal)
    {
        return newVal;
    }

    template<class ImmerT, class K>
    [[nodiscard]] auto setIn(ImmerT container, std::decay_t<decltype(getIn(std::declval<ImmerT>(), std::declval<K>()))> newVal, K &&key) -> std::decay_t<ImmerT>
    {
        return std::move(container).set(std::forward<K>(key), std::move(newVal));
    }

    template<class ImmerT, class K, class ...Keys>
    [[nodiscard]] auto setIn(ImmerT container, std::decay_t<decltype(getIn(std::declval<ImmerT>(), std::declval<K>(), std::declval<Keys>()...))> newVal, K &&key, Keys &&...keys) -> std::decay_t<ImmerT>
    {
        auto oldItem = getIn(container, key);
        return std::move(container).set(
            std::forward<K>(key),
            setIn(oldItem, std::move(newVal), std::forward<Keys>(keys)...)
        );
    }

    template<class T, class Func>
    [[nodiscard]] auto updateIn(T oldVal, Func func) -> T
    {
        return std::move(func)(oldVal);
    }

    template<class ImmerT, class Func, class ...Keys>
    [[nodiscard]] auto updateIn(ImmerT container, Func func, Keys &&...keys) -> ImmerT
    {
        auto oldVal = getIn(container, keys...);
        auto newVal = std::move(func)(std::move(oldVal));
        return setIn(std::move(container), std::move(newVal), std::forward<Keys>(keys)...);
    }
}
