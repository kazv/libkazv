/*
 * Copyright (C) 2020 Tusooa Zhu
 *
 * This file is part of libkazv.
 *
 * libkazv is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * libkazv is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with libkazv.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "basejob.hpp"

namespace Kazv
{
    class GetLoginFlowsJob : public BaseJob
    {
    public:
        GetLoginFlowsJob(std::string serverUrl);
    };
    class LoginJob : public BaseJob
    {
    public:
        LoginJob(std::string serverUrl,
                 std::string username,
                 std::string password,
                 std::optional<std::string> deviceName);
    };
};
