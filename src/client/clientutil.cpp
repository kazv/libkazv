/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include "client-model.hpp"
#include "clientutil.hpp"

namespace Kazv
{
    std::string increaseTxnId(std::string cur)
    {
        return std::to_string(std::stoull(cur) + 1);
    }

    std::string getTxnId(Event /* event */, ClientModel &m)
    {
        auto txnId =
            std::to_string(std::chrono::system_clock::now()
                .time_since_epoch().count())
            + m.nextTxnId;

        m.nextTxnId = increaseTxnId(m.nextTxnId);

        return txnId;
    }
}
