/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <lager/reader.hpp>

#include <copy-helper.hpp>
#include "push-rules-desc.hpp"
#include "client-model.hpp"

namespace Kazv
{
    /**
     * A class to handle a notification.
     */
    class NotificationHandler
    {
    public:
        NotificationHandler(lager::reader<ClientModel> client);
        KAZV_DECLARE_COPYABLE(NotificationHandler);
        ~NotificationHandler();

        /**
         * Handle a notification and get the actions to be taken.
         *
         * Only push rules are taken into consideration.
         * Your own event will not be ruled out if there is no
         * corresponding push rule.
         *
         * @param e The event to process.
         * @return A PushAction for the event.
         */
        PushAction handleNotification(const Event &e) const;

    private:
        struct Private;
        std::unique_ptr<Private> m_d;
        KAZV_DECLARE_THREAD_ID();
        KAZV_DECLARE_EVENT_LOOP_THREAD_ID_KEEPER(0);
    };
}
