/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <zug/transducer/filter.hpp>
#include <zug/transducer/cat.hpp>

#include "encryption.hpp"

#include <immer-utils.hpp>
#include <debug.hpp>
#include "cursorutil.hpp"
#include "status-utils.hpp"

namespace Kazv
{
    using namespace CryptoConstants;

    static json convertSignature(const ClientModel &m, std::string signature)
    {
        auto j = json::object();
        j[m.userId] = json::object();
        j[m.userId][ed25519 + ":" + m.deviceId] = signature;

        return j;
    }

    ClientResult updateClient(ClientModel m, UploadIdentityKeysAction)
    {
        if (! m.crypto) {
            kzo.client.warn() << "Client::crypto is invalid, ignoring it." << std::endl;
            return { std::move(m), lager::noop };
        }

        auto keys =
            immer::map<std::string, std::string>{}
             .set(ed25519 + ":" + m.deviceId, m.constCrypto().ed25519IdentityKey())
             .set(curve25519 + ":" + m.deviceId, m.constCrypto().curve25519IdentityKey());

        DeviceKeys k {
            m.userId,
            m.deviceId,
            {olmAlgo, megOlmAlgo},
            keys,
            {} // signatures to be added soon
        };

        auto j = json(k);

        auto sig = m.withCrypto([&](auto &crypto) { return crypto.sign(j); });

        k.signatures = convertSignature(m, sig);

        auto job = m.job<UploadKeysJob>()
            .make(k)
            .withData(json{{"is", "identityKeys"}});

        kzo.client.dbg() << "Uploading identity keys" << std::endl;

        m.addJob(std::move(job));

        return { std::move(m), lager::noop };
    }

    ClientResult updateClient(ClientModel m, GenerateAndUploadOneTimeKeysAction a)
    {
        if (! m.crypto) {
            kzo.client.warn() << "Client::crypto is invalid, ignoring it." << std::endl;
            return { std::move(m), simpleFail };
        }

        kzo.client.dbg() << "Generating " << a.numToGen << " one-time keys..." << std::endl;

        auto maxNumKeys = m.constCrypto().maxNumberOfOneTimeKeys();
        auto numLocalKeys = m.constCrypto().numUnpublishedOneTimeKeys();
        auto numStoredKeys = m.constCrypto().uploadedOneTimeKeysCount(signedCurve25519) + numLocalKeys;
        auto numKeysToGenerate = a.numToGen;
        auto genKeysLimit = maxNumKeys - numStoredKeys;

        if (numKeysToGenerate > genKeysLimit) {
            numKeysToGenerate = genKeysLimit;
        }

        if (numLocalKeys <= 0 && numKeysToGenerate <= 0) { // we have enough already
            kzo.client.dbg() << "We have enough one-time keys. Ignoring this." << std::endl;
            return { std::move(m), lager::noop };
        }

        if (numKeysToGenerate > 0) {
            m.withCrypto([&](auto &c) { c.genOneTimeKeysWithRandom(a.random, numKeysToGenerate); });
        }
        kzo.client.dbg() << "Generating done." << std::endl;

        auto keys = m.constCrypto().unpublishedOneTimeKeys();

        auto cv25519Keys = keys.at(curve25519);
        json oneTimeKeys = json::object();
        for (auto [id, keyStr] : cv25519Keys.items()) {
            json keyObject = json::object();
            keyObject["key"] = keyStr;
            keyObject["signatures"] = convertSignature(m, m.withCrypto([&](auto &c) { return c.sign(keyObject); }));

            oneTimeKeys[signedCurve25519 + ":" + id] = keyObject;
        }

        auto job = m.job<UploadKeysJob>()
            .make(
                std::nullopt, // deviceKeys
                oneTimeKeys)
            .withData(json{{"is", "oneTimeKeys"}});

        kzo.client.dbg() << "Uploading one time keys" << std::endl;

        m.addJob(std::move(job));

        return { std::move(m), lager::noop };

    };

    ClientResult processResponse(ClientModel m, UploadKeysResponse r)
    {
        if (! m.crypto) {
            kzo.client.warn() << "Client::crypto is invalid, ignoring it." << std::endl;
            return { std::move(m), lager::noop };
        }

        auto is = r.dataStr("is");

        if (is == "identityKeys") {
            if (! r.success()) {
                kzo.client.dbg() << "Uploading identity keys failed" << std::endl;
                m.addTrigger(UploadIdentityKeysFailed{r.errorCode(), r.errorMessage()});
                return { std::move(m), failWithResponse(r) };
            }

            kzo.client.dbg() << "Uploading identity keys successful" << std::endl;
            m.addTrigger(UploadIdentityKeysSuccessful{});

            m.identityKeysUploaded = true;
        } else {
            if (! r.success()) {
                kzo.client.dbg() << "Uploading one-time keys failed" << std::endl;
                m.addTrigger(UploadOneTimeKeysFailed{r.errorCode(), r.errorMessage()});
                return { std::move(m), failWithResponse(r) };
            }

            kzo.client.dbg() << "Uploading one-time keys successful" << std::endl;
            m.addTrigger(UploadOneTimeKeysSuccessful{});

            m.withCrypto([&](auto &c) { c.markOneTimeKeysAsPublished(); });
        }

        m.withCrypto([&](auto &c) { c.setUploadedOneTimeKeysCount(r.oneTimeKeyCounts()); });

        return { std::move(m), lager::noop };
    }

    static JsonWrap cannotDecryptEvent(
        const std::string &reason,
        const std::string &errcode,
        const json &raw)
    {
        return json{
            {"type", "m.room.message"},
            {"content", {
                {"msgtype","moe.kazv.mxc.cannot.decrypt"},
                {"body", "**This message cannot be decrypted due to " + reason + ".**"},
                {"moe.kazv.mxc.error", reason},
                {"moe.kazv.mxc.errcode", errcode},
                {"moe.kazv.mxc.raw", raw},
            }},
        };
    }

    // returns std::nullopt on success, and an error event on failure
    static std::optional<JsonWrap> verifyEvent(ClientModel &m, Event e, const json &plainJson)
    {
        try {
            std::string algo = e.originalJson().get().at("content").at("algorithm");
            if (algo == olmAlgo) {
                std::string senderCurve25519Key = e.originalJson().get()
                    .at("content").at("sender_key");

                auto deviceInfoOpt = m.deviceLists.findByCurve25519Key(e.sender(), senderCurve25519Key);

                if (! deviceInfoOpt) {
                    kzo.client.dbg() << "Device key " << senderCurve25519Key
                                     << " unknown, thus invalid" << std::endl;
                    return cannotDecryptEvent(
                        "device key unknown",
                        "MOE.KAZV.MXC_DEVICE_KEY_UNKNOWN",
                        plainJson
                    );
                }

                auto deviceInfo = deviceInfoOpt.value();

                if (! (plainJson.at("sender") == e.sender())) {
                    kzo.client.dbg() << "Sender does not match, thus invalid" << std::endl;
                    return cannotDecryptEvent(
                        "sender does not match",
                        "MOE.KAZV.MXC_BAD_SENDER",
                        plainJson
                    );
                }
                if (! (plainJson.at("recipient") == m.userId)) {
                    kzo.client.dbg() << "Recipient does not match, thus invalid" << std::endl;
                    return cannotDecryptEvent(
                        "recipient does not match",
                        "MOE.KAZV.MXC_BAD_RECIPIENT",
                        plainJson
                    );
                }
                if (! (plainJson.at("recipient_keys").at(ed25519) == m.constCrypto().ed25519IdentityKey())) {
                    kzo.client.dbg() << "Recipient key does not match, thus invalid" << std::endl;
                    return cannotDecryptEvent(
                        "recipient keys do not match",
                        "MOE.KAZV.MXC_BAD_RECIPIENT_KEYS",
                        plainJson
                    );
                }
                auto thisEd25519Key = plainJson.at("keys").at(ed25519).get<std::string>();
                if (thisEd25519Key != deviceInfo.ed25519Key) {
                    kzo.client.dbg() << "Sender ed25519 key does not match, thus invalid" << std::endl;
                    return cannotDecryptEvent(
                        "sender keys do not match",
                        "MOE.KAZV.MXC_BAD_SENDER_KEYS",
                        plainJson
                    );
                }
            } else if (algo == megOlmAlgo) {
                auto roomId = plainJson.at("room_id").get<std::string>();
                if (roomId.empty() ||
                    roomId != e.originalJson().get().at("room_id").template get<std::string>()) {
                    kzo.client.dbg() << "Room id does not match, thus invalid" << std::endl;
                    return cannotDecryptEvent(
                        "room id does not match",
                        "MOE.KAZV.MXC_BAD_ROOM_ID",
                        plainJson
                    );
                }
            } else {
                kzo.client.dbg() << "Unknown algorithm, thus invalid" << std::endl;
                return cannotDecryptEvent(
                    "unknown algorithm",
                    "MOE.KAZV.MXC_UNKNOWN_ALGORITHM",
                    plainJson
                );
            }
        } catch (const std::exception &exception) {
            kzo.client.dbg() << "json format is not correct, thus invalid" << std::endl;
            return cannotDecryptEvent(
                exception.what(),
                "M_BAD_JSON",
                plainJson
            );
        }

        return std::nullopt;
    }

    static Event decryptEvent(ClientModel &m, Event e)
    {
        // no need for decryption
        if (e.decrypted() || (! e.encrypted())) {
            return e;
        }

        kzo.client.dbg() << "About to decrypt event: "
                         << e.id() << std::endl;

        auto maybePlainText = m.withCrypto([&](Crypto &c) {
            return c.decrypt(e.originalJson().get());
        });

        if (! maybePlainText) {
            kzo.client.dbg() << "Cannot decrypt: " << maybePlainText.reason() << std::endl;
            return e.setDecryptedJson(
                cannotDecryptEvent(
                    maybePlainText.reason(),
                    "MOE.KAZV.MXC_DECRYPT_ERROR",
                    json(nullptr)
                ),
                Event::NotDecrypted);
        } else {
            try {
                auto plainJson = json::parse(maybePlainText.value());
                auto error = verifyEvent(m, e, plainJson);
                auto valid = !error.has_value();
                if (valid) {
                    kzo.client.dbg() << "The decrypted event is valid." << std::endl;
                }
                return valid
                    ? e.setDecryptedJson(plainJson, Event::Decrypted)
                    : e.setDecryptedJson(
                        error.value(),
                        Event::NotDecrypted);
            } catch (const std::exception &exception) {
                return e.setDecryptedJson(
                    cannotDecryptEvent(
                        exception.what(),
                        "M_NOT_JSON",
                        maybePlainText.value()
                    ),
                    Event::NotDecrypted
                );
            }
        }
    }

    ClientModel tryDecryptEvents(ClientModel m)
    {
        if (! m.crypto) {
            kzo.client.dbg() << "We have no encryption enabled--ignoring decryption request" << std::endl;
            return m;
        }

        kzo.client.dbg() << "Trying to decrypt events..." << std::endl;

        auto decryptFunc = [&](auto e) { return decryptEvent(m, e); };

        auto takeOutRoomKeyEvents =
            [&](auto e) {
                if (e.type() != "m.room_key") {
                    // Leave it as it is
                    return true;
                }

                // It is a room key event, but unencrypted.
                // Per matrix spec, we should not trust it as a E2EE key.
                // matrix spec also says we should make sure it's Olm-encrypted.
                // This is realized by verifying all MegOlm-encrypted events have
                // a room_id.
                if (!e.encrypted()) {
                    kzo.client.warn() << "Received an unencrypted room key event. Ignoring." << std::endl;
                    return false;
                }

                try {
                    auto content = e.content();
                    std::string roomId = content.get().at("room_id");
                    std::string sessionId = content.get().at("session_id");
                    kzo.client.dbg() << "Got a room key for room " << roomId
                        << ", session id: " << sessionId << std::endl;
                    std::string sessionKey = content.get().at("session_key");

                    auto k = KeyOfGroupSession{roomId, sessionId};

                    std::string ed25519Key = e.decryptedJson().get().at("keys").at(ed25519);

                    if (m.withCrypto([&](auto &c) { return c.createInboundGroupSession(k, sessionKey, ed25519Key); })) {
                        return false; // such that this event is removed
                    } else {
                        kzo.client.warn() << "The session exists and cannot be merged. Someone is trying to do a session-replace attack." << std::endl;
                        kzo.client.dbg() << "sender key is " << ed25519Key << std::endl;
                        return true;
                    }
                } catch (...) {
                    kzo.client.dbg() << "cannot create group session";
                    return false;
                }
                return true;
            };

        m.toDevice = intoImmer(
            EventList{},
            zug::map(decryptFunc)
            | zug::filter(takeOutRoomKeyEvents),
            std::move(m.toDevice));

        auto decryptEventInRoom =
            [&](auto id, auto room) {
                if (! room.encrypted) {
                    return;
                } else {
                    auto messages = room.messages;
                    auto undecryptedEvents = room.undecryptedEvents;
                    for (auto [sessionId, eventIds] : undecryptedEvents) {
                        if (m.constCrypto().hasInboundGroupSession({
                            room.roomId,
                            sessionId,
                        })) {
                            auto nextEventIds = intoImmer(
                                immer::flex_vector<std::string>{},
                                zug::filter([&](auto eventId) {
                                    auto event = room.messages[eventId];
                                    auto decrypted = decryptFunc(event);
                                    room.messages = std::move(room.messages)
                                        .set(eventId, decrypted);

                                    return !decrypted.decrypted();
                                }),
                                eventIds
                            );
                            if (nextEventIds.empty()) {
                                room.undecryptedEvents = std::move(room.undecryptedEvents).erase(sessionId);
                            } else {
                                room.undecryptedEvents = std::move(room.undecryptedEvents).set(sessionId, nextEventIds);
                            }
                        }
                    }

                    m.roomList.rooms = std::move(m.roomList.rooms).set(id, room);
                }
            };

        auto rooms = m.roomList.rooms;
        for (auto [id, room]: rooms) {
            decryptEventInRoom(id, room);
        }

        return m;
    }

    std::optional<BaseJob> clientPerform(ClientModel m, QueryKeysAction a)
    {
        if (! m.crypto) {
            kzo.client.dbg() << "We have no encryption enabled--ignoring this" << std::endl;
            return std::nullopt;
        }

        immer::map<std::string, immer::array<std::string>> deviceKeys;
        auto encryptedUsers = m.deviceLists.outdatedUsers();

        if (encryptedUsers.empty()) {
            kzo.client.dbg() << "Keys are up-to-date." << std::endl;
            return std::nullopt;
        }

        kzo.client.dbg() << "We need to query keys for: " << std::endl;
        for (auto userId: encryptedUsers) {
            kzo.client.dbg() << userId << std::endl;
            deviceKeys = std::move(deviceKeys).set(userId, {});
        }
        kzo.client.dbg() << "^" << std::endl;

        auto job = m.job<QueryKeysJob>()
            .make(std::move(deviceKeys),
                  std::nullopt, // timeout
                  a.isInitialSync ? std::nullopt : m.syncToken
                );
        return job;
    }

    ClientResult updateClient(ClientModel m, QueryKeysAction a)
    {
        auto jobOpt = clientPerform(m, a);
        if (jobOpt) {
            m.addJob(jobOpt.value());
        }

        return { std::move(m), lager::noop };
    }

    ClientResult processResponse(ClientModel m, QueryKeysResponse r)
    {
        if (! m.crypto) {
            kzo.client.dbg() << "We have no encryption enabled--ignoring this" << std::endl;
            return { std::move(m), simpleFail };
        }

        if (! r.success()) {
            kzo.client.dbg() << "query keys failed: " << r.errorCode() << r.errorMessage() << std::endl;
            return { std::move(m), failWithResponse(r) };
        }

        kzo.client.dbg() << "Received a query key response" << std::endl;

        auto usersMap = r.deviceKeys();

        for (auto [userId, deviceMap] : usersMap) {
            for (auto [deviceId, deviceInfo] : deviceMap) {
                kzo.client.dbg() << "Key for " << userId
                                 << "/" << deviceId
                                 << ": " << json(deviceInfo).dump()
                                 << std::endl;
                m.withCrypto([&](Crypto &c) {
                    m.deviceLists.addDevice(userId, deviceId, deviceInfo, c);
                });
            }
            m.deviceLists.markUpToDate(userId);
        }

        return { std::move(m), lager::noop };
    }

    ClientResult updateClient(ClientModel m, ClaimKeysAction a)
    {
        if (! m.crypto) {
            kzo.client.dbg() << "We have no encryption enabled--ignoring this" << std::endl;
            return { std::move(m), lager::noop };
        }

        kzo.client.dbg() << "claim keys for: " << json(a.devicesToSend).dump() << std::endl;

        auto keyMap = immer::map<std::string, immer::map<std::string /* deviceId */,
                                                         std::string /* curve25519IdentityKey */>>{};


        for (auto [userId, devices] : a.devicesToSend) {
            kzo.client.dbg() << "Iterating through user " << userId << std::endl;
            auto deviceToKey = immer::map<std::string, std::string>{};
            for (auto deviceId : devices) {
                kzo.client.dbg() << "Device: " << deviceId << std::endl;
                auto infoOpt = m.deviceLists.get(userId, deviceId);
                if (infoOpt) {
                    kzo.client.dbg() << "Got device info, curve25519 key is: " << infoOpt.value().curve25519Key << std::endl;
                    deviceToKey = std::move(deviceToKey)
                        .set(deviceId, infoOpt.value().curve25519Key);
                } else {
                    kzo.client.dbg() << "Did not get device info" <<  std::endl;
                }
            }
            keyMap = std::move(keyMap).set(userId, deviceToKey);
        }

        auto devicesToClaimKeys = m.withCrypto([&](auto &c) { return c.devicesMissingOutboundSessionKey(keyMap); });

        kzo.client.dbg() << "Really claim keys for: " << json(devicesToClaimKeys).dump() << std::endl;

        auto oneTimeKeys = immer::map<std::string, immer::map<std::string, std::string>>{};
        for (auto [userId, devices] : devicesToClaimKeys) {
            auto devKeys = immer::map<std::string, std::string>{};
            for (auto deviceId: devices) {
                devKeys = std::move(devKeys).set(deviceId, signedCurve25519);
            }
            oneTimeKeys = std::move(oneTimeKeys).set(userId, devKeys);
        }

        auto job = m.job<ClaimKeysJob>()
            .make(std::move(oneTimeKeys))
            .withData(json{
                    {"roomId", a.roomId},
                    {"sessionId", a.sessionId},
                    {"sessionKey", a.sessionKey},
                    {"devicesToSend", a.devicesToSend},
                    {"random", a.random}
                });
        m.addJob(std::move(job));

        return { std::move(m), lager::noop };
    }

    ClientResult processResponse(ClientModel m, ClaimKeysResponse r)
    {
        if (! m.crypto) {
            kzo.client.dbg() << "We have no encryption enabled--ignoring this" << std::endl;
            return { std::move(m), simpleFail };
        }

        if (! r.success()) {
            kzo.client.dbg() << "claim keys failed" << std::endl;
            m.addTrigger(ClaimKeysFailed{r.errorCode(), r.errorMessage()});
            return { std::move(m), failWithResponse(r) };
        }

        kzo.client.dbg() << "claim keys successful" << std::endl;

        kzo.client.dbg() << "Json body: " << r.jsonBody().get().dump() << std::endl;

        auto roomId = r.dataStr("roomId");
        auto sessionKey = r.dataStr("sessionKey");
        auto sessionId = r.dataStr("sessionId");
        auto devicesToSend =
            immer::map<std::string, immer::flex_vector<std::string>>(r.dataJson("devicesToSend"));
        auto random = r.dataJson("random").template get<RandomData>();

        // create outbound sessions for those devices
        auto oneTimeKeys = r.oneTimeKeys();

        for (auto [userId, deviceMap] : oneTimeKeys) {
            for (auto [deviceId, keyVar] : deviceMap) {
                auto keys = keyVar.get();
                for (auto [keyId, key] : keys.items()) {
                    auto deviceInfoOpt = m.deviceLists.get(userId, deviceId);
                    if (deviceInfoOpt) {
                        auto deviceInfo = deviceInfoOpt.value();
                        kzo.client.dbg() << "Verifying key for " << userId
                                         << "/" << deviceId
                                         << key.dump()
                                         << " with ed25519 key "
                                         << deviceInfo.ed25519Key << std::endl;
                        auto verified = m.withCrypto([&](auto &c) { return c.verify(key, userId, deviceId, deviceInfo.ed25519Key); });
                        kzo.client.dbg() << (verified ? "passed" : "did not pass") << std::endl;
                        if (verified && key.contains("key")) {
                            auto theirOneTimeKey = key.at("key");
                            kzo.client.dbg() << "creating outbound session for it" << std::endl;
                            m.withCrypto([&](auto &c) { c.createOutboundSessionWithRandom(random, deviceInfo.curve25519Key, theirOneTimeKey); });
                            random.erase(0, Crypto::createOutboundSessionRandomSize());
                            kzo.client.dbg() << "done" << std::endl;
                        }
                    }
                }
            }
        }

        auto eventJson = json{
            {"content", {{"algorithm", megOlmAlgo},
                         {"room_id", roomId},
                         {"session_id", sessionId},
                         {"session_key", sessionKey}}},
            {"type", "m.room_key"}
        };

        auto event = Event(JsonWrap(eventJson));

        return {
            std::move(m),
            [event](auto &&) { return EffectStatus{ /* success = */ true, json{{ "keyEvent", event.originalJson() }} }; }
        };
    }

    ClientResult updateClient(ClientModel m, EncryptMegOlmEventAction a)
    {
        auto [encryptedEvent, maybeKey] = m.megOlmEncrypt(a.e, a.roomId, a.timeMs, a.random);

        return {
            std::move(m),
            [=](auto && /* ctx */) {
                auto retJson = json::object({
                        {"encrypted", encryptedEvent.originalJson()},
                    });
                if (maybeKey.has_value()) {
                    retJson["key"] = maybeKey.value();
                }
                return EffectStatus(/* succ = */ true, retJson);
            }
        };
    }

    ClientResult updateClient(ClientModel m, SetDeviceTrustLevelAction a)
    {
        auto maybeOldInfo = m.deviceLists.get(a.userId, a.deviceId);
        if (!maybeOldInfo) {
            return {
                std::move(m),
                [=](auto && /* ctx */) {
                    auto retJson = json::object({
                        {"error", "No such device"},
                        {"errorCode", "MOE_KAZV_MXC_KAZV_NO_SUCH_DEVICE"},
                    });

                    return EffectStatus(/* succ = */ false, retJson);
                }
            };
        }

        m.deviceLists.deviceLists = updateIn(
            std::move(m.deviceLists.deviceLists),
            [a](auto device) {
                device.trustLevel = a.trustLevel;
                return device;
            },
            a.userId,
            a.deviceId
        );

        return { m, lager::noop };
    }

    ClientResult updateClient(ClientModel m, SetTrustLevelNeededToSendKeysAction a)
    {
        m.trustLevelNeededToSendKeys = a.trustLevel;
        return { std::move(m), lager::noop };
    }

    ClientResult updateClient(ClientModel m, PrepareForSharingRoomKeyAction a)
    {
        auto messages = m.olmEncryptSplit(a.e, a.devices, a.random);

        auto txnId = getTxnId(Event(), m);

        m.roomList = RoomListModel::update(
            std::move(m.roomList),
            UpdateRoomAction{
                a.roomId,
                AddPendingRoomKeyAction{
                    PendingRoomKeyEvent{txnId, messages}
                }
            }
        );

        return { std::move(m), [txnId](auto &&) {
            return EffectStatus(/* succ = */ true, json::object({{"txnId", txnId}}));
        } };
    }
}
