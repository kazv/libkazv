/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include "status-utils.hpp"

#include "account-data.hpp"

namespace Kazv
{
    ClientResult updateClient(ClientModel m, SetAccountDataPerRoomAction a)
    {
        m.addJob(m.job<SetAccountDataPerRoomJob>()
            .make(m.userId, a.roomId, a.accountDataEvent.type(), a.accountDataEvent.content()));

        return { std::move(m), lager::noop };
    }

    ClientResult processResponse(ClientModel m, SetAccountDataPerRoomResponse r)
    {
        if (!r.success()) {
            return { std::move(m), failWithResponse(r) };
        }
        return { std::move(m), lager::noop };
    }

    ClientResult updateClient(ClientModel m, SetAccountDataAction a)
    {
        m.addJob(m.job<SetAccountDataJob>()
            .make(m.userId, a.accountDataEvent.type(), a.accountDataEvent.content()));

        return { std::move(m), lager::noop };
    }

    ClientResult processResponse(ClientModel m, SetAccountDataResponse r)
    {
        if (!r.success()) {
            return { std::move(m), failWithResponse(r) };
        }
        return { std::move(m), lager::noop };
    }
}
