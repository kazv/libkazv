/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <csapi/account-data.hpp>

#include "client-model.hpp"

namespace Kazv
{
    ClientResult updateClient(ClientModel m, SetAccountDataPerRoomAction a);
    ClientResult processResponse(ClientModel m, SetAccountDataPerRoomResponse r);

    ClientResult updateClient(ClientModel m, SetAccountDataAction a);
    ClientResult processResponse(ClientModel m, SetAccountDataResponse r);
}
