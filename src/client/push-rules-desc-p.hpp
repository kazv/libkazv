/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <optional>

#include <event.hpp>

#include "push-rules-desc.hpp"

namespace Kazv
{
    std::vector<std::string> splitPath(std::string path);

    bool matchGlob(std::string target, std::string pattern);

    std::pair<bool, json> validateRule(std::string ruleSetName, const json &rule);

    Event validatePushRules(const Event &e);

    struct PushRulesDescPrivate
    {
        PushRulesDescPrivate(const Event &e);

        Event pushRulesEvent;

        std::optional<std::pair<std::string, json>> matchRule(const Event &e, const RoomModel &room) const;

        std::optional<std::pair<std::string, json>> matchRuleSet(std::string ruleSetName, const Event &e, const RoomModel &room) const;

        bool matchP(std::string ruleSetName, const json &rule, const Event &e, const RoomModel &room) const;

        PushAction handleRule(std::string ruleSetName, const json &rule, const Event &e, const RoomModel &room) const;
    };
}
