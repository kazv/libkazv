/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <tuple>
#include <variant>

#include <lager/context.hpp>
#include <context.hpp>

#include "room/room-model.hpp"

namespace Kazv
{
    using namespace Api;
    class JobInterface;
    class EventInterface;

    struct LoginAction;
    struct TokenLoginAction;
    struct LogoutAction;
    struct HardLogoutAction;
    struct GetWellknownAction;
    struct GetVersionsAction;
    struct SyncAction;
    struct SetShouldSyncAction;
    struct PostInitialFiltersAction;
    struct SetAccountDataAction;
    struct PaginateTimelineAction;
    struct SendMessageAction;
    struct SendStateEventAction;
    struct SaveLocalEchoAction;
    struct UpdateLocalEchoStatusAction;
    struct RedactEventAction;
    struct CreateRoomAction;
    struct GetRoomStatesAction;
    struct GetStateEventAction;
    struct InviteToRoomAction;
    struct JoinRoomByIdAction;
    struct EmitKazvEventsAction;
    struct JoinRoomAction;
    struct LeaveRoomAction;
    struct ForgetRoomAction;
    struct KickAction;
    struct BanAction;
    struct UnbanAction;
    struct SetAccountDataPerRoomAction;

    struct ProcessResponseAction;
    struct SetTypingAction;
    struct PostReceiptAction;
    struct SetReadMarkerAction;

    struct UploadContentAction;
    struct DownloadContentAction;
    struct DownloadThumbnailAction;

    struct SendToDeviceMessageAction;
    struct SendMultipleToDeviceMessagesAction;

    struct UploadIdentityKeysAction;
    struct GenerateAndUploadOneTimeKeysAction;
    struct QueryKeysAction;
    struct ClaimKeysAction;
    struct EncryptMegOlmEventAction;
    struct SetDeviceTrustLevelAction;
    struct SetTrustLevelNeededToSendKeysAction;
    struct PrepareForSharingRoomKeyAction;

    struct GetUserProfileAction;
    struct SetAvatarUrlAction;
    struct SetDisplayNameAction;

    struct ResubmitJobAction;

    struct ClientModel;

    using ClientAction = std::variant<
        RoomListAction,

        LoginAction,
        TokenLoginAction,
        LogoutAction,
        HardLogoutAction,
        GetWellknownAction,
        GetVersionsAction,

        SyncAction,
        SetShouldSyncAction,
        PostInitialFiltersAction,
        SetAccountDataAction,

        PaginateTimelineAction,
        SendMessageAction,
        SendStateEventAction,
        SaveLocalEchoAction,
        UpdateLocalEchoStatusAction,
        RedactEventAction,
        CreateRoomAction,
        GetRoomStatesAction,
        GetStateEventAction,
        InviteToRoomAction,
        JoinRoomByIdAction,

        JoinRoomAction,
        LeaveRoomAction,
        ForgetRoomAction,
        KickAction,
        BanAction,
        UnbanAction,
        SetAccountDataPerRoomAction,

        ProcessResponseAction,

        SetTypingAction,
        PostReceiptAction,
        SetReadMarkerAction,

        UploadContentAction,
        DownloadContentAction,
        DownloadThumbnailAction,

        SendToDeviceMessageAction,
        SendMultipleToDeviceMessagesAction,

        UploadIdentityKeysAction,
        GenerateAndUploadOneTimeKeysAction,
        QueryKeysAction,
        ClaimKeysAction,
        EncryptMegOlmEventAction,
        SetDeviceTrustLevelAction,
        SetTrustLevelNeededToSendKeysAction,
        PrepareForSharingRoomKeyAction,

        GetUserProfileAction,
        SetAvatarUrlAction,
        SetDisplayNameAction,

        ResubmitJobAction
        >;

    using ClientEffect = Effect<ClientAction, lager::deps<>>;

    using ClientResult = std::pair<ClientModel, ClientEffect>;
}
