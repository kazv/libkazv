/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <memory>
#include <optional>

#include <copy-helper.hpp>

namespace Kazv
{
    struct RoomModel;
    class Event;

    struct PushRulesDescPrivate;

    /**
     * Describe what actions are to be taken for a specific event.
     */
    struct PushAction
    {
        /// Whether the client should notify about this event.
        bool shouldNotify;
        /// The sound of the notification as defined in the spec.
        /// If the notification should not ring, it is set as
        /// `std::nullopt`.
        std::optional<std::string> sound;
        /// Whether the client should highlight the event.
        bool shouldHighlight;
    };

    class PushRulesDesc
    {
    public:
        /**
         * Construct an invalid PushRulesDesc.
         */
        PushRulesDesc();

        /**
         * Construct a PushRulesDesc from pushRulesEvent.
         */
        PushRulesDesc(const Event &pushRulesEvent);
        ~PushRulesDesc();

        KAZV_DECLARE_COPYABLE(PushRulesDesc)

        /**
         * Check whether this PushRulesDesc is valid.
         */
        bool valid() const;

        /**
         * Check whether we should send a notification about event `e`.
         *
         * @param e The event to check.
         * @param room The model of the room where the event is in.
         * @return Action about event `e`.
         */
         PushAction handle(const Event &e, const RoomModel &room) const;

    private:
        std::unique_ptr<PushRulesDescPrivate> m_d;
    };
}
