/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>
#include <context.hpp>
#include <basejob.hpp>

namespace Kazv
{
    namespace detail
    {
        struct SimpleFailT
        {
            template<class Context>
            EffectStatus operator()(Context &&) const {
                return EffectStatus(/* succ = */ false);
            };
        };

        struct ReturnEffectStatusT
        {
            EffectStatus st;

            template<class Context>
            EffectStatus operator()(Context &&) const
            {
                return st;
            };
        };
    }

    /**
     * A effect that returns a failed EffectStatus upon invocation.
     */
    constexpr auto simpleFail = detail::SimpleFailT{};

    /**
     * A effect that returns a failed EffectStatus upon invocation.
     */
    detail::ReturnEffectStatusT failWithResponse(const BaseJob::Response &r);

    /**
     * An effect that returns a failed EffectStatus with the given
     * error code and message.
     */
    detail::ReturnEffectStatusT failEffect(std::string errorCode, std::string errorMsg);
}
