/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */


#pragma once
#include <libkazv-config.hpp>

#include <string>
#include <variant>
#include <immer/flex_vector.hpp>
#include <immer/map.hpp>

#include <serialization/immer-flex-vector.hpp>
#include <serialization/immer-box.hpp>
#include <serialization/immer-map.hpp>
#include <serialization/immer-array.hpp>

#include <csapi/sync.hpp>
#include <event.hpp>

#include <crypto.hpp>
#include "push-rules-desc.hpp"
#include "local-echo.hpp"
#include "clientutil.hpp"

namespace Kazv
{
    struct PendingRoomKeyEvent
    {
        std::string txnId;
        immer::map<std::string, immer::map<std::string, Event>> messages;
    };

    PendingRoomKeyEvent makePendingRoomKeyEventV0(std::string txnId, Event event, immer::map<std::string, immer::flex_vector<std::string>> devices);

    struct ReadReceipt
    {
        std::string eventId;
        Timestamp timestamp;
    };

    template<class Archive>
    void serialize(Archive &ar, ReadReceipt &r, std::uint32_t const /* version */)
    {
        ar & r.eventId & r.timestamp;
    }

    bool operator==(const ReadReceipt &a, const ReadReceipt &b);
    bool operator!=(const ReadReceipt &a, const ReadReceipt &b);

    struct EventReader
    {
        std::string userId;
        Timestamp timestamp;
    };

    bool operator==(const EventReader &a, const EventReader &b);
    bool operator!=(const EventReader &a, const EventReader &b);

    struct AddStateEventsAction
    {
        immer::flex_vector<Event> stateEvents;
    };

    /// Go from the back of stateEvents to the beginning,
    /// adding the event to room state only if the room
    /// has no state event with that state key.
    struct MaybeAddStateEventsAction
    {
        immer::flex_vector<Event> stateEvents;
    };

    struct AddToTimelineAction
    {
        /// Events from oldest to latest
        immer::flex_vector<Event> events;
        std::optional<std::string> prevBatch;
        std::optional<bool> limited;
        std::optional<std::string> gapEventId;
    };

    struct AddAccountDataAction
    {
        immer::flex_vector<Event> events;
    };

    struct ChangeMembershipAction
    {
        RoomMembership membership;
    };

    struct ChangeInviteStateAction
    {
        immer::flex_vector<Event> events;
    };

    struct AddEphemeralAction
    {
        EventList events;
    };

    struct SetLocalDraftAction
    {
        std::string localDraft;
    };

    struct SetRoomEncryptionAction
    {
    };

    struct MarkMembersFullyLoadedAction
    {
    };

    struct SetHeroIdsAction
    {
        immer::flex_vector<std::string> heroIds;
    };

    struct AddLocalEchoAction
    {
        LocalEchoDesc localEcho;
    };

    struct RemoveLocalEchoAction
    {
        std::string txnId;
    };

    struct AddPendingRoomKeyAction
    {
        PendingRoomKeyEvent pendingRoomKeyEvent;
    };

    struct RemovePendingRoomKeyAction
    {
        std::string txnId;
    };

    struct UpdateJoinedMemberCountAction
    {
        std::size_t joinedMemberCount;
    };

    struct UpdateInvitedMemberCountAction
    {
        std::size_t invitedMemberCount;
    };

    /// Update local notifications to include the new events
    ///
    /// Precondition: newEvents are already in room.messages
    struct AddLocalNotificationsAction
    {
        EventList newEvents;
        PushRulesDesc pushRulesDesc;
        std::string myUserId;
    };

    /// Remove local notifications that are already read
    struct RemoveReadLocalNotificationsAction
    {
        std::string myUserId;
    };

    /// Update the local read marker, removing any read notifications before it.
    struct UpdateLocalReadMarkerAction
    {
        std::string localReadMarker;
        std::string myUserId;
    };

    inline bool operator==(const PendingRoomKeyEvent &a, const PendingRoomKeyEvent &b)
    {
        return a.txnId == b.txnId && a.messages == b.messages;
    }

    inline bool operator!=(const PendingRoomKeyEvent &a, const PendingRoomKeyEvent &b)
    {
        return !(a == b);
    }

    inline const double ROOM_TAG_DEFAULT_ORDER = 2;

    template<class Archive>
    void serialize(Archive &ar, PendingRoomKeyEvent &e, std::uint32_t const version)
    {
        if (version < 1) {
            // loading an older version where there is only one event
            std::string txnId;
            Event event;
            immer::map<std::string, immer::flex_vector<std::string>> devices;
            ar & txnId & event & devices;
            e = makePendingRoomKeyEventV0(
                std::move(txnId), std::move(event), std::move(devices));
        } else {
            ar & e.txnId & e.messages;
        }
    }

    /**
     * Get the sort key for a timeline event.
     *
     * If the key is larger, the event should be placed
     * at the more recent end of the timeline.
     *
     * @param e The event to get the sort key for.
     * @return The sort key. You MUST use `auto` to store the
     * result.
     */
    auto sortKeyForTimelineEvent(Event e) -> std::tuple<Timestamp, std::string>;

    struct RoomModel
    {
        using Membership = RoomMembership;
        using ReverseEventRelationshipMap = immer::map<
            std::string /* related event id */,
            immer::map<std::string /* relation type */, immer::flex_vector<std::string /* relater event id */>>>;

        std::string roomId;
        immer::map<KeyOfState, Event> stateEvents;
        immer::map<KeyOfState, Event> inviteState;
        // Smaller indices mean earlier events
        // (oldest) 0 --------> n (latest)
        immer::flex_vector<std::string> timeline;
        immer::map<std::string, Event> messages;
        immer::map<std::string, Event> accountData;
        Membership membership{};
        std::string paginateBackToken;
        /// whether this room has earlier events to be fetched
        bool canPaginateBack{true};

        immer::map<std::string /* eventId */, std::string /* prevBatch */> timelineGaps;

        immer::map<std::string, Event> ephemeral;

        std::string localDraft;

        bool encrypted{false};
        /// a marker to indicate whether we need to rotate
        /// the session key earlier than it expires
        /// (e.g. when a user in the room's device list changed
        /// or when someone joins or leaves)
        bool shouldRotateSessionKey{true};

        bool membersFullyLoaded{false};
        immer::flex_vector<std::string> heroIds;

        immer::flex_vector<LocalEchoDesc> localEchoes;

        immer::flex_vector<PendingRoomKeyEvent> pendingRoomKeyEvents;

        ReverseEventRelationshipMap reverseEventRelationships;

        std::size_t joinedMemberCount{0};
        std::size_t invitedMemberCount{0};

        /// The local read marker for this room. Indicates that
        /// you have read up to this event.
        std::string localReadMarker;
        /// The local unread count for this room.
        std::size_t localUnreadCount{0};
        /// The local unread notification count for this room.
        /// XXX this is never used.
        std::size_t localNotificationCount{0};
        /// Read receipts for all users
        immer::map<std::string /* userId */, ReadReceipt> readReceipts;
        /// A map from event id to a list of users that has read
        /// receipt at that point
        immer::map<
            std::string /* eventId */,
            immer::flex_vector<std::string /* userId */>> eventReadUsers;

        /// A map from the session id to a list of event ids of events
        /// that cannot (yet) be decrypted.
        immer::map<
            std::string /* sessionId */,
            immer::flex_vector<std::string /* eventId */>> undecryptedEvents;

        immer::flex_vector<std::string> unreadNotificationEventIds;

        immer::flex_vector<std::string> joinedMemberIds() const;
        immer::flex_vector<std::string> invitedMemberIds() const;
        immer::flex_vector<std::string> knockedMemberIds() const;
        immer::flex_vector<std::string> leftMemberIds() const;
        immer::flex_vector<std::string> bannedMemberIds() const;

        EventList joinedMemberEvents() const;
        EventList invitedMemberEvents() const;
        EventList knockedMemberEvents() const;
        EventList leftMemberEvents() const;
        EventList bannedMemberEvents() const;
        EventList heroMemberEvents() const;

        MegOlmSessionRotateDesc sessionRotateDesc() const;

        bool hasUser(std::string userId) const;

        std::optional<LocalEchoDesc> getLocalEchoByTxnId(std::string txnId) const;
        std::optional<PendingRoomKeyEvent> getPendingRoomKeyEventByTxnId(std::string txnId) const;

        immer::map<std::string, double> tags() const;

        Event makeAddTagEvent(std::string tagId, std::optional<double> order) const;
        Event makeRemoveTagEvent(std::string tagId) const;

        /**
         * Fill in reverseEventRelationships by gathering
         * the relationships specified in `newEvents`
         *
         * @param newEvents The events that just came in after last time event relationships
         * are gathered.
         */
        void generateRelationships(EventList newEvents);

        void regenerateRelationships();

        /**
         * Fill in undecryptedEvents by gathering
         * the session ids specified in `newEvents`.
         *
         * @param newEvents New incoming events.
         */
        void addToUndecryptedEvents(EventList newEvents);

        void recalculateUndecryptedEvents();

        using Action = std::variant<
            AddStateEventsAction,
            MaybeAddStateEventsAction,
            AddToTimelineAction,
            AddAccountDataAction,
            ChangeMembershipAction,
            ChangeInviteStateAction,
            AddEphemeralAction,
            SetLocalDraftAction,
            SetRoomEncryptionAction,
            MarkMembersFullyLoadedAction,
            SetHeroIdsAction,
            AddLocalEchoAction,
            RemoveLocalEchoAction,
            AddPendingRoomKeyAction,
            RemovePendingRoomKeyAction,
            UpdateJoinedMemberCountAction,
            UpdateInvitedMemberCountAction,
            AddLocalNotificationsAction,
            RemoveReadLocalNotificationsAction,
            UpdateLocalReadMarkerAction
            >;

        static RoomModel update(RoomModel r, Action a);
    };

    using RoomAction = RoomModel::Action;

    inline bool operator==(const RoomModel &a, const RoomModel &b)
    {
        return a.roomId == b.roomId
            && a.stateEvents == b.stateEvents
            && a.inviteState == b.inviteState
            && a.timeline == b.timeline
            && a.messages == b.messages
            && a.accountData == b.accountData
            && a.membership == b.membership
            && a.paginateBackToken == b.paginateBackToken
            && a.canPaginateBack == b.canPaginateBack
            && a.timelineGaps == b.timelineGaps
            && a.ephemeral == b.ephemeral
            && a.localDraft == b.localDraft
            && a.encrypted == b.encrypted
            && a.shouldRotateSessionKey == b.shouldRotateSessionKey
            && a.membersFullyLoaded == b.membersFullyLoaded
            && a.heroIds == b.heroIds
            && a.localEchoes == b.localEchoes
            && a.pendingRoomKeyEvents == b.pendingRoomKeyEvents
            && a.reverseEventRelationships == b.reverseEventRelationships
            && a.joinedMemberCount == b.joinedMemberCount
            && a.localReadMarker == b.localReadMarker
            && a.localUnreadCount == b.localUnreadCount
            && a.localNotificationCount == b.localNotificationCount
            && a.readReceipts == b.readReceipts
            && a.eventReadUsers == b.eventReadUsers
            && a.undecryptedEvents == b.undecryptedEvents
            && a.unreadNotificationEventIds == b.unreadNotificationEventIds
            ;
    }

    struct UpdateRoomAction
    {
        std::string roomId;
        RoomAction roomAction;
    };

    struct RoomListModel
    {
        immer::map<std::string, RoomModel> rooms;

        inline auto at(std::string id) const { return rooms.at(id); }
        inline auto operator[](std::string id) const { return rooms[id]; }
        inline bool has(std::string id) const { return rooms.find(id); }

        using Action = std::variant<
            UpdateRoomAction
            >;
        static RoomListModel update(RoomListModel l, Action a);
    };

    using RoomListAction = RoomListModel::Action;

    inline bool operator==(const RoomListModel &a, const RoomListModel &b)
    {
        return a.rooms == b.rooms;
    }

    template<class Archive>
    void serialize(Archive &ar, RoomModel &r, std::uint32_t const version)
    {
        ar
            & r.roomId
            & r.stateEvents
            & r.inviteState

            & r.timeline
            & r.messages
            & r.accountData
            & r.membership
            & r.paginateBackToken
            & r.canPaginateBack

            & r.timelineGaps
            & r.ephemeral

            & r.localDraft

            & r.encrypted
            & r.shouldRotateSessionKey

            & r.membersFullyLoaded
            ;

        if (version >= 1) {
            ar
                & r.heroIds
                ;
        }
        if (version >= 2) {
            ar & r.localEchoes;
        }
        if (version >= 3) {
            ar & r.pendingRoomKeyEvents;
        }
        if (version >= 4) {
            ar & r.reverseEventRelationships;
        } else { // must be reading from an older version
            if constexpr (typename Archive::is_loading()) {
                r.regenerateRelationships();
            }
        }
        if (version >= 5) {
            ar & r.joinedMemberCount & r.invitedMemberCount;
        }
        if (version >= 6) {
            ar
                & r.localReadMarker
                & r.localUnreadCount
                & r.localNotificationCount
                & r.readReceipts
                & r.eventReadUsers;
        }
        if (version >= 7) {
            ar & r.undecryptedEvents;
        } else {
            if constexpr (typename Archive::is_loading()) {
                r.recalculateUndecryptedEvents();
            }
        }
        if (version >= 8) {
            ar & r.unreadNotificationEventIds;
        }
    }

    template<class Archive>
    void serialize(Archive &ar, RoomListModel &l, std::uint32_t const /*version*/)
    {
        ar & l.rooms;
    }
}

BOOST_CLASS_VERSION(Kazv::PendingRoomKeyEvent, 1)
BOOST_CLASS_VERSION(Kazv::ReadReceipt, 0)
BOOST_CLASS_VERSION(Kazv::RoomModel, 8)
BOOST_CLASS_VERSION(Kazv::RoomListModel, 0)
