/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>


#include <lager/util.hpp>
#include <zug/sequence.hpp>
#include <zug/transducer/map.hpp>
#include <zug/transducer/filter.hpp>

#include "debug.hpp"

#include "room-model.hpp"
#include "cursorutil.hpp"
#include "immer-utils.hpp"

inline const auto receiptTypes = immer::flex_vector<std::string>{"m.read", "m.read.private"};

template<class Func>
static std::string getMaxInTimeline(std::string a, std::string b, Func sortKey)
{
    if (a.empty() && b.empty()) {
        return std::string();
    } else {
        // for an unexisting event, event id is empty and timestamp is 0
        // for an existing event, event id is not empty and timestamp >= 0
        // so this handles all cases even when we do not have the corresponding event
        return std::max(a, b, [=](const std::string &x, const std::string &y) {
            return sortKey(x) < sortKey(y);
        });
    }
}

namespace Kazv
{
    PendingRoomKeyEvent makePendingRoomKeyEventV0(std::string txnId, Event event, immer::map<std::string, immer::flex_vector<std::string>> devices)
    {
        immer::map<std::string, immer::map<std::string, Event>> messages;
        for (auto [userId, deviceIds] : devices) {
            messages = setIn(std::move(messages), immer::map<std::string, Event>(), userId);
            for (auto deviceId : deviceIds) {
                messages = setIn(
                    std::move(messages),
                    event,
                    userId, deviceId
                );
            }
        }
        return PendingRoomKeyEvent{txnId, messages};
    }

    bool operator==(const ReadReceipt &a, const ReadReceipt &b)
    {
        return a.eventId == b.eventId && a.timestamp == b.timestamp;
    }

    bool operator!=(const ReadReceipt &a, const ReadReceipt &b)
    {
        return !(a == b);
    }

    bool operator==(const EventReader &a, const EventReader &b)
    {
        return a.userId == b.userId && a.timestamp == b.timestamp;
    }

    bool operator!=(const EventReader &a, const EventReader &b)
    {
        return !(a == b);
    }

    auto sortKeyForTimelineEvent(Event e) -> std::tuple<Timestamp, std::string>
    {
        return std::make_tuple(e.originServerTs(), e.id());
    }

    RoomModel RoomModel::update(RoomModel r, Action a)
    {
        return lager::match(std::move(a))(
            [&](AddStateEventsAction a) {
                r.stateEvents = merge(std::move(r.stateEvents), a.stateEvents, keyOfState);

                // If m.room.encryption state event appears,
                // configure the room to use encryption.
                if (r.stateEvents.find(KeyOfState{"m.room.encryption", ""})) {
                    auto newRoom = update(std::move(r), SetRoomEncryptionAction{});
                    r = std::move(newRoom);
                }
                return r;
            },
            [&](MaybeAddStateEventsAction a) {
                for (auto it = a.stateEvents.rbegin();
                     it != a.stateEvents.rend();
                     ++it) {
                    const auto &e = *it;
                    auto k = keyOfState(e);
                    if (!r.stateEvents.count(k)) {
                        r.stateEvents = std::move(r.stateEvents).set(k, e);
                    }
                }
                return r;
            },
            [&](AddToTimelineAction a) {
                auto eventIds = intoImmer(immer::flex_vector<std::string>(),
                                          zug::map(keyOfTimeline), a.events);

                auto oldMessages = r.messages;
                r.messages = merge(std::move(r.messages), a.events, keyOfTimeline);
                auto exists =
                    [=](auto eventId) -> bool {
                        return !! oldMessages.find(eventId);
                    };
                auto key =
                    [=](auto eventId) {
                        // sort first by timestamp, then by id
                        return sortKeyForTimelineEvent(r.messages[eventId]);
                    };

                auto handleRedaction =
                    [&r](const auto &event) {
                        if (event.type() == "m.room.redaction") {
                            auto origJson = event.originalJson().get();
                            if (origJson.contains("redacts") && origJson.at("redacts").is_string()) {
                                auto redactedEventId = origJson.at("redacts").template get<std::string>();
                                if (r.messages.find(redactedEventId)) {
                                    r.messages = std::move(r.messages).update(redactedEventId, [&origJson](const auto &eventToBeRedacted) {
                                        auto newJson = eventToBeRedacted.originalJson().get();
                                        newJson.merge_patch(json{
                                                {"unsigned", {{"redacted_because", std::move(origJson)}}},
                                            });
                                        newJson["content"] = json::object();
                                        return Event(newJson);
                                    });
                                }
                            }
                        }
                        return event;
                    };

                immer::for_each(a.events, handleRedaction);

                r.timeline = sortedUniqueMerge(r.timeline, eventIds, exists, key);

                // If this is a pagination request, gapEventId
                // should have value. If this is a sync request,
                // gapEventId does not have value. The pagination
                // request does not have the limited field, and
                // whether it has more paginate back token is determined
                // by the presence of the prevBatch parameter.
                // In sync request, limited may not be specified,
                // and thus, if limited does not have value, it means
                // it is not limited.
                if (((a.limited.has_value() && a.limited.value())
                        || a.gapEventId.has_value())
                    && a.prevBatch.has_value()) {
                    // this sync is limited, add a Gap here
                    if (!eventIds.empty()) {
                        r.timelineGaps = std::move(r.timelineGaps).set(eventIds[0], a.prevBatch.value());
                    }
                }

                // remove the original Gap, as it is resolved
                if (a.gapEventId.has_value()) {
                    r.timelineGaps = std::move(r.timelineGaps).erase(a.gapEventId.value());
                }

                // remove all Gaps between the gapped event and the first event in this batch
                if (!eventIds.empty() && a.gapEventId.has_value()) {
                    auto cmp = [=](auto a, auto b) {
                                   return key(a) < key(b);
                               };
                    auto thisBatchStart = std::equal_range(r.timeline.begin(), r.timeline.end(), eventIds[0], cmp).first;
                    auto origBatchStart = std::equal_range(thisBatchStart, r.timeline.end(), a.gapEventId.value(), cmp).first;

                    // Safety assert: we do not want to execute the for_each if the range is empty,
                    // or it will go out of bounds.
                    if (thisBatchStart.index() < origBatchStart.index()) {
                        std::for_each(thisBatchStart + 1, origBatchStart,
                            [&](auto eventId) {
                                r.timelineGaps = std::move(r.timelineGaps).erase(eventId);
                            });
                    }
                }

                // remove all local echoes that are received
                for (const auto &e : a.events) {
                    auto jw = e.originalJson();
                    const auto &json = jw.get();
                    if (json.contains("unsigned")
                        && json["unsigned"].contains("transaction_id")
                        && json["unsigned"]["transaction_id"].is_string()) {
                        r = update(std::move(r), RemoveLocalEchoAction{json["unsigned"]["transaction_id"].template get<std::string>()});
                    }
                }

                // calculate event relationships
                r.generateRelationships(a.events);

                r.addToUndecryptedEvents(a.events);

                return r;
            },
            [&](AddAccountDataAction a) {
                r.accountData = merge(std::move(r.accountData), a.events, keyOfAccountData);
                return r;
            },
            [&](ChangeMembershipAction a) {
                r.membership = a.membership;
                return r;
            },
            [&](ChangeInviteStateAction a) {
                r.inviteState = merge(immer::map<KeyOfState, Event>{}, a.events, keyOfState);
                return r;
            },
            [&](AddEphemeralAction a) {
                auto processReceipt = [&](Event e) {
                    const auto content = e.content().get();
                    for (auto [eventId, receipts] : content.items()) {
                        if (!receipts.is_object()) {
                            continue;
                        }
                        for (auto receiptType : receiptTypes) {
                            if (!(receipts.contains(receiptType)
                                    && receipts[receiptType].is_object())) {
                                continue;
                            }

                            for (auto [user, receipt]: receipts[receiptType].items()) {
                                ReadReceipt readReceipt{
                                    eventId,
                                    0,
                                };

                                if (receipt.is_object() && receipt.contains("ts")
                                    && receipt["ts"].is_number()) {
                                    readReceipt.timestamp = receipt["ts"].template get<Timestamp>();
                                }

                                // Remove old receipts
                                if (r.readReceipts.count(user)) {
                                    auto oldReceiptEventId = r.readReceipts[user].eventId;
                                    if (r.eventReadUsers.count(oldReceiptEventId)) {
                                        auto remaining =
                                            intoImmer(
                                                immer::flex_vector<std::string>{},
                                                zug::filter([user=user](auto userId) {
                                                    return userId != user;
                                                }),
                                                r.eventReadUsers[oldReceiptEventId]
                                            );

                                        if (remaining.empty()) {
                                            r.eventReadUsers = std::move(r.eventReadUsers).erase(oldReceiptEventId);
                                        } else {
                                            r.eventReadUsers = std::move(r.eventReadUsers).set(oldReceiptEventId, remaining);
                                        }
                                    }
                                }

                                // Add new receipt
                                r.readReceipts = std::move(r.readReceipts).set(user, readReceipt);
                                auto oldReadUsers = r.eventReadUsers[eventId];
                                r.eventReadUsers = std::move(r.eventReadUsers).set(eventId, oldReadUsers.push_back(user));
                            }
                        }
                    }
                };

                for (auto e : a.events) {
                    if (e.type() == "m.receipt") {
                        processReceipt(e);
                    }
                }
                r.ephemeral = merge(std::move(r.ephemeral), a.events, keyOfEphemeral);
                return r;
            },
            [&](SetLocalDraftAction a) {
                r.localDraft = a.localDraft;
                return r;
            },
            [&](SetRoomEncryptionAction) {
                r.encrypted = true;
                return r;
            },
            [&](MarkMembersFullyLoadedAction) {
                r.membersFullyLoaded = true;
                return r;
            },
            [&](SetHeroIdsAction a) {
                r.heroIds = a.heroIds;
                return r;
            },
            [&](AddLocalEchoAction a) {
                auto it = std::find_if(r.localEchoes.begin(), r.localEchoes.end(), [a](const auto &desc) {
                    return desc.txnId == a.localEcho.txnId;
                });
                if (it == r.localEchoes.end()) {
                    r.localEchoes = std::move(r.localEchoes).push_back(a.localEcho);
                } else {
                    r.localEchoes = std::move(r.localEchoes).set(it.index(), a.localEcho);
                }
                return r;
            },
            [&](RemoveLocalEchoAction a) {
                auto it = std::find_if(r.localEchoes.begin(), r.localEchoes.end(), [a](const auto &desc) {
                    return desc.txnId == a.txnId;
                });
                if (it != r.localEchoes.end()) {
                    r.localEchoes = std::move(r.localEchoes).erase(it.index());
                }
                return r;
            },
            [&](AddPendingRoomKeyAction a) {
                auto it = std::find_if(r.pendingRoomKeyEvents.begin(), r.pendingRoomKeyEvents.end(), [a](const auto &p) {
                    return p.txnId == a.pendingRoomKeyEvent.txnId;
                });
                if (it == r.pendingRoomKeyEvents.end()) {
                    r.pendingRoomKeyEvents = std::move(r.pendingRoomKeyEvents).push_back(a.pendingRoomKeyEvent);
                } else {
                    r.pendingRoomKeyEvents = std::move(r.pendingRoomKeyEvents).set(it.index(), a.pendingRoomKeyEvent);
                }
                return r;
            },
            [&](RemovePendingRoomKeyAction a) {
                auto it = std::find_if(r.pendingRoomKeyEvents.begin(), r.pendingRoomKeyEvents.end(), [a](const auto &desc) {
                    return desc.txnId == a.txnId;
                });
                if (it != r.pendingRoomKeyEvents.end()) {
                    r.pendingRoomKeyEvents = std::move(r.pendingRoomKeyEvents).erase(it.index());
                }
                return r;
            },
            [&](UpdateJoinedMemberCountAction a) {
                r.joinedMemberCount = a.joinedMemberCount;
                return r;
            },
            [&](UpdateInvitedMemberCountAction a) {
                r.invitedMemberCount = a.invitedMemberCount;
                return r;
            },
            [&](AddLocalNotificationsAction a) {
                auto k = [r](const auto &id) {
                    return sortKeyForTimelineEvent(r.messages[id]);
                };

                auto readReceiptForCurrentUser = getMaxInTimeline(r.readReceipts[a.myUserId].eventId, r.localReadMarker, k);

                auto newEventIds = intoImmer(
                    immer::flex_vector<std::string>{},
                    zug::map(&Event::id),
                    a.newEvents
                );

                auto needToAddPredicate = [a, r, k, readReceiptForCurrentUser](const auto &eid) {
                    if (!readReceiptForCurrentUser.empty()
                        && k(readReceiptForCurrentUser) >= k(eid)) {
                        // this means this event is already read
                        return false;
                    }
                    auto e = r.messages[eid];
                    return e.sender() != a.myUserId
                        && a.pushRulesDesc.handle(e, r).shouldNotify;
                };

                r.unreadNotificationEventIds = sortedUniqueMerge(std::move(r.unreadNotificationEventIds), newEventIds, [needToAddPredicate](const auto &e) { return !needToAddPredicate(e); }, k);
                return r;
            },
            [&](RemoveReadLocalNotificationsAction a) {
                auto k = [r](const auto &id) {
                    return sortKeyForTimelineEvent(r.messages[id]);
                };
                auto cmp = [k](const auto &a, const auto &b) {
                    return k(a) < k(b);
                };

                auto rr = getMaxInTimeline(
                    r.readReceipts[a.myUserId].eventId,
                    r.localReadMarker,
                    k
                );
                if (rr.empty()) {
                    return r;
                }

                auto it = std::upper_bound(
                    r.unreadNotificationEventIds.begin(),
                    r.unreadNotificationEventIds.end(),
                    rr,
                    cmp
                );
                // *it > rr, *(it - 1) <= rr (if it - 1 is valid)
                if (it == r.unreadNotificationEventIds.end()) {
                    // If it == end(), it means everything is read
                    r.unreadNotificationEventIds = {};
                } else if (it == r.unreadNotificationEventIds.begin()) {
                    // If it == begin(), it means everything is unread, so nothing to do
                } else {
                    // it is somewhere in the middle, pointing to the first element that is unread
                    r.unreadNotificationEventIds = std::move(r.unreadNotificationEventIds).erase(0, it.index());
                }
                return r;
            },
            [&](UpdateLocalReadMarkerAction a) {
                r.localReadMarker = a.localReadMarker;
                auto next = RoomModel::update(std::move(r), RemoveReadLocalNotificationsAction{a.myUserId});
                return next;
            }
            );
    }

    RoomListModel RoomListModel::update(RoomListModel l, Action a)
    {
        return lager::match(std::move(a))(
            [&](UpdateRoomAction a) {
                l.rooms = std::move(l.rooms)
                    .update(a.roomId,
                            [=](RoomModel oldRoom) {
                                oldRoom.roomId = a.roomId; // in case it is a new room
                                return RoomModel::update(std::move(oldRoom), a.roomAction);
                            });
                return l;
            }
            );
    }

    static auto membershipTransducer(const std::string &membership)
    {
        return zug::filter([](auto val) {
            auto [k, v] = val;
            auto [type, stateKey] = k;
            return type == "m.room.member"s;
        })
        | zug::map([](auto val) {
            auto [k, v] = val;
            auto [type, stateKey] = k;
            return std::pair<std::string, Kazv::Event>{stateKey, v};
        })
        | zug::filter([&membership](auto val) {
            auto [stateKey, ev] = val;
            return ev.content().get()
                .at("membership"s) == membership;
        });
    }

    static auto memberIdsByMembership(immer::map<KeyOfState, Event> stateEvents, const std::string &membership)
    {
        return intoImmer(
            immer::flex_vector<std::string>{},
            membershipTransducer(membership)
            | zug::map([](auto val) {
                auto [stateKey, ev] = val;
                return stateKey;
            }),
            stateEvents);
    }

    auto memberEventsByMembership(immer::map<KeyOfState, Event> stateEvents, const std::string &membership)
    {
        return intoImmer(
            EventList{},
            membershipTransducer(membership)
            | zug::map([](auto val) {
                auto [stateKey, ev] = val;
                return ev;
            }),
            stateEvents);
    }

    immer::flex_vector<std::string> RoomModel::joinedMemberIds() const
    {
        return memberIdsByMembership(stateEvents, "join"s);
    }

    immer::flex_vector<std::string> RoomModel::invitedMemberIds() const
    {
        return memberIdsByMembership(stateEvents, "invite"s);
    }

    immer::flex_vector<std::string> RoomModel::knockedMemberIds() const
    {
        return memberIdsByMembership(stateEvents, "knock"s);
    }

    immer::flex_vector<std::string> RoomModel::leftMemberIds() const
    {
        return memberIdsByMembership(stateEvents, "leave"s);
    }

    immer::flex_vector<std::string> RoomModel::bannedMemberIds() const
    {
        return memberIdsByMembership(stateEvents, "ban"s);
    }

    EventList RoomModel::joinedMemberEvents() const
    {
        return memberEventsByMembership(stateEvents, "join"s);
    }

    EventList RoomModel::invitedMemberEvents() const
    {
        return memberEventsByMembership(stateEvents, "invite"s);
    }

    EventList RoomModel::knockedMemberEvents() const
    {
        return memberEventsByMembership(stateEvents, "knock"s);
    }

    EventList RoomModel::leftMemberEvents() const
    {
        return memberEventsByMembership(stateEvents, "leave"s);
    }

    EventList RoomModel::bannedMemberEvents() const
    {
        return memberEventsByMembership(stateEvents, "ban"s);
    }

    EventList RoomModel::heroMemberEvents() const
    {
        return intoImmer(
            EventList{},
            zug::filter([heroIds=heroIds](auto val) {
                auto [k, ev] = val;
                auto [type, stateKey] = k;
                return type == "m.room.member"s &&
                    std::find(heroIds.begin(), heroIds.end(), stateKey) != heroIds.end();
            })
            | zug::map([](auto val) {
                auto [_, ev] = val;
                return ev;
            }),
            stateEvents);
    }

    static Timestamp defaultRotateMs = 604800000;
    static int defaultRotateMsgs = 100;

    MegOlmSessionRotateDesc RoomModel::sessionRotateDesc() const
    {
        auto k = KeyOfState{"m.room.encryption", ""};
        auto content = stateEvents[k].content().get();
        auto ms = content.contains("rotation_period_ms")
            ? content["rotation_period_ms"].get<Timestamp>()
            : defaultRotateMs;
        auto msgs = content.contains("rotation_period_msgs")
            ? content["rotation_period_msgs"].get<int>()
            : defaultRotateMsgs;
        return MegOlmSessionRotateDesc{ ms, msgs };
    }

    bool RoomModel::hasUser(std::string userId) const
    {
        try {
            auto ev = stateEvents.at(KeyOfState{"m.room.member", userId});
            if (ev.content().get().at("membership") == "join") {
                return true;
            }
        } catch (const std::exception &) {
            return false;
        }
        return false;
    }

    std::optional<LocalEchoDesc> RoomModel::getLocalEchoByTxnId(std::string txnId) const
    {
        auto it = std::find_if(localEchoes.begin(), localEchoes.end(), [txnId](const auto &desc) {
            return txnId == desc.txnId;
        });
        if (it != localEchoes.end()) {
            return *it;
        } else {
            return std::nullopt;
        }
    }

    std::optional<PendingRoomKeyEvent> RoomModel::getPendingRoomKeyEventByTxnId(std::string txnId) const
    {
        auto it = std::find_if(pendingRoomKeyEvents.begin(), pendingRoomKeyEvents.end(), [txnId](const auto &desc) {
            return txnId == desc.txnId;
        });
        if (it != pendingRoomKeyEvents.end()) {
            return *it;
        } else {
            return std::nullopt;
        }
    }

    static double getTagOrder(const json &tag)
    {
        // https://spec.matrix.org/v1.7/client-server-api/#events-12
        // If a room has a tag without an order key then it should appear after the rooms with that tag that have an order key.
        return tag.contains("order") && tag["order"].is_number()
            ? tag["order"].template get<double>()
            : ROOM_TAG_DEFAULT_ORDER;
    }

    immer::map<std::string, double> RoomModel::tags() const
    {
        auto content = accountData["m.tag"].content().get();
        if (!content.contains("tags") || !content["tags"].is_object()) {
            return {};
        }

        auto tagsObject = content["tags"];
        auto tagsItems = tagsObject.items();
        return std::accumulate(tagsItems.begin(), tagsItems.end(), immer::map<std::string, double>(),
            [=](auto acc, const auto &cur) {
                auto [id, tag] = cur;
                return std::move(acc).set(id, getTagOrder(tag));
            }
        );
    }

    static auto normalizeTagEventJson(Event e)
    {
        auto content = e.content().get();
        if (!content.contains("tags") || !content["tags"].is_object()) {
            content["tags"] = json::object();
        }
        return json{
            {"content", content},
            {"type", "m.tag"},
        };
    }

    Event RoomModel::makeAddTagEvent(std::string tagId, std::optional<double> order) const
    {
        auto eventJson = normalizeTagEventJson(accountData["m.tag"]);
        auto tag = json::object();
        if (order.has_value()) {
            tag["order"] = order.value();
        }

        eventJson["content"]["tags"][tagId] = tag;
        return Event(eventJson);
    }

    Event RoomModel::makeRemoveTagEvent(std::string tagId) const
    {
        auto eventJson = normalizeTagEventJson(accountData["m.tag"]);
        eventJson["content"]["tags"].erase(tagId);
        return Event(eventJson);
    }

    void RoomModel::generateRelationships(EventList newEvents)
    {
        for (const auto &event: newEvents) {
            auto [relType, eventId] = event.relationship();
            if (!relType.empty()) {
                reverseEventRelationships = updateIn(std::move(reverseEventRelationships), [event](auto &&evs) {
                    return evs.push_back(event.id());
                }, eventId, relType);
            }
        }
    }

    void RoomModel::regenerateRelationships()
    {
        generateRelationships(intoImmer(EventList{}, zug::map([](const auto &kv) {
            return kv.second;
        }), messages));
    }

    void RoomModel::addToUndecryptedEvents(EventList newEvents)
    {
        if (!encrypted) {
            return;
        }

        for (auto event : newEvents) {
            if (event.encrypted() && !event.decrypted()) {
                auto original = event.originalJson();
                const auto &o = original.get();
                if (o.contains("content")
                    && o["content"].contains("session_id")
                    && o["content"]["session_id"].is_string()
                ) {
                    auto sessionId = o["content"]["session_id"].template get<std::string>();
                    undecryptedEvents = std::move(undecryptedEvents)
                        .update(sessionId, [id=event.id()](const auto &v) {
                            return v.push_back(id);
                        });
                }
            }
        }
    }

    void RoomModel::recalculateUndecryptedEvents()
    {
        if (!encrypted) {
            return;
        }

        addToUndecryptedEvents(intoImmer(EventList{}, zug::map([](const auto &kv) {
            return kv.second;
        }), messages));
    }
}
