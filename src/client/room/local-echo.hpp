/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <boost/serialization/split_free.hpp>
#include "event.hpp"

namespace Kazv
{
    /**
     * Describes a local echo.
     */
    struct LocalEchoDesc
    {
        enum Status
        {
            Sending,
            Failed,
        };

        std::string txnId;
        Event event;
        Status status{Sending};
    };

    inline bool operator==(const LocalEchoDesc &a, const LocalEchoDesc &b)
    {
        return a.txnId == b.txnId
            && a.event == b.event
            && a.status == b.status;
    }

    inline bool operator!=(const LocalEchoDesc &a, const LocalEchoDesc &b)
    {
        return !(a == b);
    }
}

namespace boost::serialization
{
    template<class Archive>
    void save(Archive &ar, const Kazv::LocalEchoDesc &d, std::uint32_t const version)
    {
        using namespace Kazv;
        LocalEchoDesc::Status dummyStatus{LocalEchoDesc::Failed};
        ar
            & d.txnId
            & d.event
            & dummyStatus
            ;
    }

    template<class Archive>
    void load(Archive &ar, Kazv::LocalEchoDesc &d, std::uint32_t const version)
    {
        using namespace Kazv;
        LocalEchoDesc::Status dummyStatus{LocalEchoDesc::Failed};
        ar
            & d.txnId
            & d.event
            & dummyStatus
            ;
        d.status = LocalEchoDesc::Failed;
    }

    template<class Archive>
    void serialize(Archive &ar, Kazv::LocalEchoDesc &d, std::uint32_t const version)
    {
        using boost::serialization::split_free;
        split_free(ar, d, version);
    }
}

BOOST_CLASS_VERSION(Kazv::LocalEchoDesc, 0)
