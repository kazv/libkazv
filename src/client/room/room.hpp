/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <lager/reader.hpp>
#include <lager/context.hpp>
#include <lager/with.hpp>
#include <lager/constant.hpp>
#include <lager/lenses/optional.hpp>
#include <zug/transducer/map.hpp>
#include <zug/transducer/filter.hpp>
#include <zug/sequence.hpp>
#include <immer/flex_vector_transient.hpp>

#include "debug.hpp"

#include "sdk-model.hpp"
#include "client-model.hpp"
#include "room-model.hpp"
#include <cursorutil.hpp>
#include "sdk-model-cursor-tag.hpp"
#include "random-generator.hpp"
#include "power-levels-desc.hpp"

namespace Kazv
{
    /**
     * Represent a Matrix room.
     *
     * This class has the same constraints as Client.
     */
    class Room
    {
    public:
        using PromiseT = SingleTypePromise<DefaultRetType>;
        using DepsT = lager::deps<SdkModelCursorKey, RandomInterface &
#ifdef KAZV_USE_THREAD_SAFETY_HELPER
                                  , EventLoopThreadIdKeeper &
#endif
                                  >;
        using ContextT = Context<ClientAction>;

        struct InEventLoopTag {};

        /**
         * Constructor.
         *
         * Construct the room with @c roomId .
         *
         * `sdk` and `roomId` must be cursors in the same thread.
         *
         * The constructed room will be in the same thread as `sdk` and `roomId`.
         *
         * @warning Do not use this directly. Use `Client::room()` and
         * `Client::roomBycursor()` instead.
         */
        Room(lager::reader<SdkModel> sdk,
             lager::reader<std::string> roomId,
             ContextT ctx);

        /**
         * Constructor.
         *
         * Construct the room with @c roomId and with Deps support.
         *
         * `sdk` and `roomId` must be cursors in the same thread.
         *
         * The constructed room will be in the same thread as `sdk` and `roomId`.
         *
         * @warning Do not use this directly. Use `Client::room()` and
         * `Client::roomBycursor()` instead.
         */
        Room(lager::reader<SdkModel> sdk,
             lager::reader<std::string> roomId,
             ContextT ctx, DepsT deps);

        /**
         * Construct a Room in the same thread as the event loop.
         *
         * The constructed Room is not constructed from a cursor,
         * and thus copying-constructing from that is thread-safe as long as each thread
         * calls with different objects.
         *
         * this must have Deps support.
         *
         * @warning Do not use this directly. Use `Client::room()` and
         * `Client::roomBycursor()` instead.
         */
        Room(InEventLoopTag, std::string roomId, ContextT ctx, DepsT deps);

        /**
         * Return a Room that represents the room *currently represented* by this,
         * but suitable for use in the event loop of the context.
         *
         * This function can only be called from the thread where this belongs.
         *
         * Example:
         *
         * ```
         * auto ctx = sdk.context();
         * auto client = sdk.clientFromSecondaryRoot(sr);
         * auto room = client.room("!room-id:domain.name");
         * room.sendTextMessage("test")
         *     .then([r=room.toEventLoop(), ctx](auto &&st) {
         *         if (!st) {
         *             std::cerr << "Cannot send message" << std::endl;
         *             return ctx.createResolvedPromise(st);
         *         }
         *         return r.sendTextMessage("follow-up");
         *     });
         * ```
         *
         * @sa Sdk::clientFromSecondaryRoot , Client::room
         */
        Room toEventLoop() const;

        /* lager::reader<MapT<KeyOfState, Event>> */
        inline auto stateEvents() const {
            return roomCursor()
                [&RoomModel::stateEvents];
        }

        /**
         * Get the invite_state of this room.
         *
         * @return A lager::reader containing a map from KeyOfState to the event.
         */
        auto inviteState() const -> lager::reader<immer::map<KeyOfState, Event>>;

        /* lager::reader<std::optional<Event>> */
        inline auto stateOpt(KeyOfState k) const {
            return stateEvents()
                [std::move(k)];
        }

        /* lager::reader<Event> */
        inline auto state(KeyOfState k) const {
            return stateOpt(k)
                [lager::lenses::or_default];
        }

        /**
         * Get the timeline event ids of this room in ascending timestamp order.
         * It takes constant time for the cursor to be updated.
         *
         * @return A lager::reader of a RangeT of event ids.
         */
        auto timelineEventIds() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get a map from event ids to events. It takes constant time for the
         * cursor to be updated.
         *
         * @return A lager::reader of a map for all timeline events.
         */
        auto messagesMap() const -> lager::reader<immer::map<std::string, Event>>;

        /**
         * Get a list of timeline events in this room. It takes O(timeline.size())
         * time for the cursor to be updated.
         *
         * @return A lager::reader<RangeT<Event>> of timeline events from
         * the oldest to the latest.
         */
        auto timelineEvents() const -> lager::reader<immer::flex_vector<Event>>;

        /**
         * Get one message by the event id.
         *
         * @param A lager::reader of the event id.
         *
         * @return A lager::reader of the event.
         */
        auto message(lager::reader<std::string> eventId) const -> lager::reader<Event>;

        /**
         * Get one local echo by the txnId.
         *
         * @param A lager::reader of the txnId.
         *
         * @return A lager::reader of the local echo.
         */
        auto localEcho(lager::reader<std::string> txnId) const -> lager::reader<LocalEchoDesc>;

        /**
         * Get the member events of heroes this room.
         *
         * @return a lager::reader of a RangeT of Event containing the member events.
         */
        auto heroMemberEvents() const -> lager::reader<immer::flex_vector<Event>>;

        /**
         * Get the member events of heroes this room.
         *
         * @return a lager::reader of a RangeT of std::string containing the member events.
         */
        auto heroDisplayNames() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get the name of this room.
         *
         * If there is a m.room.name state event, the name in it is used.
         * If there is none, the returned cursor will hold std::nullopt.
         *
         * @return a lager::reader of an optional std::string.
         */
        auto nameOpt() const -> lager::reader<std::optional<std::string>>;

        /**
         * Get the name of this room.
         *
         * If there is a m.room.name state event, the name in it is used.
         * If there is none, the returned cursor will hold a placeholder string.
         *
         * @return a lager::reader of an std::string.
         */
        auto name() const -> lager::reader<std::string>;

        /**
         * Get the avatar mxc uri of this room.
         *
         * @return A lager::reader containing the mxc uri of this room.
         */
        auto avatarMxcUri() const -> lager::reader<std::string>;

        /**
         * Get the list of joined member ids.
         *
         * @return A lager::reader containing an RangeT of the joined members' ids.
         */
        auto members() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get the list of invited member ids.
         *
         * @return A lager::reader containing an RangeT of the invited members' ids.
         */
        auto invitedMembers() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get the list of knocked member ids.
         *
         * @return A lager::reader containing an RangeT of the knocked members' ids.
         */
        auto knockedMembers() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get the list of left member ids.
         *
         * @return A lager::reader containing an RangeT of the left members' ids.
         */
        auto leftMembers() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get the list of banned member ids.
         *
         * @return A lager::reader containing an RangeT of the banned members' id.
         */
        auto bannedMembers() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get the list of joined member events.
         *
         * @return A lager::reader containing an EventList of the joined members' events.
         */
        auto joinedMemberEvents() const -> lager::reader<EventList>;

        /**
         * Get the list of invited member events.
         *
         * @return A lager::reader containing an EventList of the invited members' events.
         */
        auto invitedMemberEvents() const -> lager::reader<EventList>;

        /**
         * Get the list of knocked member events.
         *
         * @return A lager::reader containing an EventList of the knocked members' events.
         */
        auto knockedMemberEvents() const -> lager::reader<EventList>;

        /**
         * Get the list of left member events.
         *
         * @return A lager::reader containing an EventList of the left members' events.
         */
        auto leftMemberEvents() const -> lager::reader<EventList>;

        /**
         * Get the list of banned member events.
         *
         * @return A lager::reader containing an EventList of the banned members' events.
         */
        auto bannedMemberEvents() const -> lager::reader<EventList>;

        /**
         * Get the member event for userId.
         *
         * If membership of the current user is Invite, it prefers
         * the event in inviteState to the one in stateEvents.
         *
         * @return A lager::reader containing the state event.
         */
        auto memberEventByCursor(lager::reader<std::string> userId) const -> lager::reader<Event>;

        /**
         * Get the member event for userId.
         *
         * If membership of the current user is Invite, it prefers
         * the event in inviteState to the one in stateEvents.
         *
         * @return A lager::reader containing the state event.
         */
        auto memberEventFor(std::string userId) const -> lager::reader<Event>;

        /**
         * Get whether this room is encrypted.
         *
         * The encryption status is changed to true if the client
         * receives a state event that turns on encryption.
         * If that state event is removed later, the status will
         * not be changed.
         *
         * @return A lager::reader<bool> that contains
         * whether this room is encrypted.
         */
        lager::reader<bool> encrypted() const;

        /*lager::reader<std::string>*/
        KAZV_WRAP_ATTR(RoomModel, roomCursor(), roomId);
        /*lager::reader<RoomMembership>*/
        KAZV_WRAP_ATTR(RoomModel, roomCursor(), membership);
        /*lager::reader<std::string>*/
        KAZV_WRAP_ATTR(RoomModel, roomCursor(), localDraft);
        /* lager::reader<bool> */
        KAZV_WRAP_ATTR(RoomModel, roomCursor(), membersFullyLoaded);

        /**
         * Get the local read marker in this room.
         *
         * @return a lager::reader of a std::string of the local read marker.
         */
        auto localReadMarker() const -> lager::reader<std::string>;

        /**
         * Get the ids of the heroes of the room.
         *
         * @return a lager::reader of a RangeT<std::string> containing
         * the ids of the heroes of the room.
         */
        auto heroIds() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get the joined member count of this room.
         *
         * @return A lager::reader of std::size_t containing the joined
         * member count for this room.
         */
        auto joinedMemberCount() const -> lager::reader<std::size_t>;

        /**
         * Get the invited member count of this room.
         *
         * @return A lager::reader of std::size_t containing the invited
         * member count for this room.
         */
        auto invitedMemberCount() const -> lager::reader<std::size_t>;

        /**
         * Set local draft for this room.
         *
         * After the returned Promise is resolved,
         * @c localDraft() will contain @c localDraft .
         *
         * @param localDraft The local draft to send.
         * @return A Promise that resolves when the local draft
         * has been set, or when there is an error.
         */
        PromiseT setLocalDraft(std::string localDraft) const;

        /**
         * Send an event to this room.
         *
         * @param msg The message to send
         * @return A Promise that resolves when the event has been sent,
         * or when there is an error.
         */
        PromiseT sendMessage(Event msg) const;

        /**
         * Send a text message to this room.
         *
         * @param text The text
         * @return A Promise that resolves when the text message has
         * been sent, or when there is an error.
         */
        PromiseT sendTextMessage(std::string text) const;

        /**
         * Resend an event to this room.
         *
         * @param txnId The transaction id of the unsent message
         * @return A Promise that resolves when the event has been sent,
         * or when there is an error.
         */
        PromiseT resendMessage(std::string txnId) const;

        /**
         * Redact an event.
         *
         * @param eventId The event id of the event you want to redact
         * @param reason The reason of redaction
         * @return A Promise that resolves when the event has been sent,
         * or when there is an error.
         */
        PromiseT redactEvent(std::string eventId, std::optional<std::string> reason) const;

        /**
         * Send one pending key event in this room.
         *
         * @param txnId The transaction id of the pending key event
         * @return A Promise that resolves when the event has been sent,
         * or when there is an error.
         */
        PromiseT sendPendingKeyEvent(std::string txnId) const;

        /**
         * Send all pending key events in this room.
         *
         * @return A Promise that resolves when all events has been sent,
         * or when there is an error.
         */
        PromiseT sendAllPendingKeyEvents() const;

        /**
         * Get the full state of this room.
         *
         * This method will update the Client as needed.
         *
         * After the returned Promise resolves successfully,
         * @c stateEvents() will contain the fetched state.
         *
         * @return A Promise that resolves when the room state
         * has been fetched, or when there is an error.
         */
        PromiseT refreshRoomState() const;

        /**
         * Get one state event with @c type and @c stateKey .
         *
         * This method will update the Client as needed.
         *
         * After the returned Promise resolves successfully,
         * @c state({type,stateKey}) will contain the fetched
         * state event.
         *
         * @return A Promise that resolves when the state
         * event has been fetched, or when there is an error.
         */
        PromiseT getStateEvent(std::string type, std::string stateKey) const;

        /**
         * Send a state event to this room.
         *
         * @param state The state event to send.
         * @return A Promise that resolves when the state event
         * has been sent, or when there is an error.
         */
        PromiseT sendStateEvent(Event state) const;

        /**
         * Set the room name.
         *
         * @param name The new name for this room.
         * @return A Promise that resolves when the state event
         * for the name change has been sent, or when there is an error.
         */
        PromiseT setName(std::string name) const;

        // lager::reader<std::string>
        inline auto topic() const {
            using namespace lager::lenses;
            return stateEvents()
                [KeyOfState{"m.room.topic", ""}]
                [or_default]
                .xform(eventContent
                       | jsonAtOr("topic"s, ""s));
        }

        /**
         * Set the room topic.
         *
         * @param topic The new topic for this room.
         * @return A Promise that resolves when the state event
         * for the topic change has been sent, or when there is an error.
         */
        PromiseT setTopic(std::string topic) const;

        /**
         * Invite a user to this room
         *
         * @param userId The user id for the user to invite.
         * @return A Promise that resolves when the state event
         * for the invite has been sent, or when there is an error.
         */
        PromiseT invite(std::string userId) const;

        /* lager::reader<MapT<std::string, Event>> */
        inline auto ephemeralEvents() const {
            return roomCursor()
                [&RoomModel::ephemeral];
        }

        /* lager::reader<std::optional<Event>> */
        inline auto ephemeralOpt(std::string type) const {
            return roomCursor()
                [&RoomModel::ephemeral]
                [type];
        }

        /* lager::reader<Event> */
        inline auto ephemeral(std::string type) const {
            return roomCursor()
                [&RoomModel::ephemeral]
                [type]
                [lager::lenses::or_default];
        }

        /**
         * Get the ids of all typing users in this room.
         *
         * @return A lager::reader of an RangeT of std::string of ids of all typing users.
         */
        auto typingUsers() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Get the member events of all typing users in this room.
         *
         * @return A lager::reader of an EventList of member events of all typing users.
         */
        auto typingMemberEvents() const -> lager::reader<EventList>;

        /**
         * Set the typing status of the current user in this room.
         *
         * @param typing Whether the user is now typing.
         * @param timeoutMs How long this typing status should last,
         * in milliseconds.
         * @return A Promise that resolves when the typing status
         * has been sent, or when there is an error.
         */
        PromiseT setTyping(bool typing, std::optional<int> timeoutMs) const;

        /* lager::reader<MapT<std::string, Event>> */
        inline auto accountDataEvents() const {
            return roomCursor()
                [&RoomModel::accountData];
        }

        /* lager::reader<std::optional<Event>> */
        inline auto accountDataOpt(std::string type) const {
            return roomCursor()
                [&RoomModel::accountData]
                [type];
        }

        /* lager::reader<Event> */
        inline auto accountData(std::string type) const {
            return roomCursor()
                [&RoomModel::accountData]
                [type]
                [lager::lenses::or_default];
        }

        /* lager::reader<std::string> */
        inline auto readMarker() const {
            using namespace lager::lenses;
            return accountData("m.fully_read")
                .xform(eventContent
                       | jsonAtOr("event_id", std::string{}));
        }

        /**
         * Set the account data for this room.
         *
         * @return A Promise that resolves when the account data
         * has been set, or when there is an error.
         */
        PromiseT setAccountData(Event accountDataEvent) const;

        /**
         * Get the tags of the current room.
         *
         * @return A lager::reader of a map from tag id of this room to
         * the corresponding order.
         */
        auto tags() const -> lager::reader<immer::map<std::string, double>>;

        /**
         * Add or set a tag to this room.
         *
         * @param tagId The tag id to add or set.
         * @param order The order to specify.
         *
         * @return A Promise that resolves when the tag
         * has been set, or when there is an error.
         */
        PromiseT addOrSetTag(std::string tagId, std::optional<double> order = std::nullopt) const;

        /**
         * Remove a tag from this room.
         *
         * @param tagId The tag id to remove.
         *
         * @return A Promise that resolves when the tag
         * has been removed, or when there is an error.
         */
        PromiseT removeTag(std::string tagId) const;

        /**
         * Leave this room.
         *
         * @return A Promise that resolves when the state event
         * for the leaving has been sent, or when there is an error.
         */
        PromiseT leave() const;

        /**
         * Forget this room.
         *
         * One can only forget a room when they have already left it.
         *
         * @return A Promise that resolves when the room has been
         * forgot, or when there is an error.
         */
        PromiseT forget() const;

        /**
         * Kick a user from this room.
         *
         * You must have enough power levels in this room to do so.
         *
         * @param userId The id of the user that will be kicked.
         * @param reason The reason to explain this kick.
         *
         * @return A Promise that resolves when the kick is done,
         * or when there is an error.
         */
        PromiseT kick(std::string userId, std::optional<std::string> reason = std::nullopt) const;

        /**
         * Ban a user from this room.
         *
         * You must have enough power levels in this room to do so.
         *
         * @param userId The id of the user that will be banned.
         * @param reason The reason to explain this ban.
         *
         * @return A Promise that resolves when the ban is done,
         * or when there is an error.
         */
        PromiseT ban(std::string userId, std::optional<std::string> reason = std::nullopt) const;

        // TODO: v1.1 adds reason field
        /**
         * Unban a user from this room.
         *
         * You must have enough power levels in this room to do so.
         *
         * @param userId The id of the user that will be unbanned.
         *
         * @return A Promise that resolves when the unban is done,
         * or when there is an error.
         */
        PromiseT unban(std::string userId/*, std::optional<std::string> reason = std::nullopt*/) const;

        /* lager::reader<JsonWrap> */
        inline auto avatar() const {
            return state(KeyOfState{"m.room.avatar", ""})
                .xform(eventContent);
        }

        /**
         * Get pinned events of this room
         *
         * @return A lager::reader of RangeT of the pinned event ids.
         */
        auto pinnedEvents() const -> lager::reader<immer::flex_vector<std::string>>;

        /**
         * Set pinned events of this room
         *
         * @param eventIds The event ids of the new pinned events
         *
         * @return A Promise that resolves when the state event
         * for the pinned events change has been sent, or when there is an error.
         */
        PromiseT setPinnedEvents(immer::flex_vector<std::string> eventIds) const;

        /**
         * Add eventIds to the pinned events of this room
         *
         * @param eventIds The ids of events you want to add to the pinned events.
         * @return A Promise that resolves when the state event
         * for the pinned events change has been sent, or when there is an error.
         */
        PromiseT pinEvents(immer::flex_vector<std::string> eventIds) const;

        /**
         * Remove eventIds from the pinned events of this room
         *
         * @param eventIds The ids of events you want to remove from the pinned events.
         * @return A Promise that resolves when the state event
         * for the pinned events change has been sent, or when there is an error.
         */
        PromiseT unpinEvents(immer::flex_vector<std::string> eventIds) const;

        /**
         * Get the Gaps in the timeline for this room.
         *
         * Any key of the map in the returned reader can be send as
         * an argument of paginateBackFromEvent() to try to fill the Gap
         * at that event.
         *
         * @return A lager::reader that contains an evnetId-to-prevBatch map.
         */
        lager::reader<immer::map<std::string /* eventId */, std::string /* prevBatch */>> timelineGaps() const;

        /**
         * Try to paginate back from @c eventId.
         *
         * @param eventId An event id that is in the key of `+timelineGaps()`.
         *
         * @return A Promise that resolves when the pagination is
         * successful, or when there is an error. If it is successful,
         * `+timelineGaps()` will no longer contain eventId as key, and
         * `timeline()` will contain the events before eventId in the
         * full event chain on the homeserver.
         * If `eventId` is not in `+timelineGaps()`, it is considered
         * to be failed.
         */
        PromiseT paginateBackFromEvent(std::string eventId) const;

        /**
         * Get the list of local echoes in this room.
         *
         * @return A lager::reader that contains the list of all local echoes in the room.
         * The event type will always be m.room.message and event id will not be meaningful.
         */
        auto localEchoes() const -> lager::reader<immer::flex_vector<LocalEchoDesc>>;

        /**
         * Remove a local echo from this room.
         *
         * @param txnId The transaction id associated with that local echo.
         *
         * @return A Promise that resolves when the local echo is removed.
         */
        PromiseT removeLocalEcho(std::string txnId) const;

        /**
         * Get the list of pending room key events in this room.
         *
         * @return A lager::reader that contains the list of all pending room key events in the room.
         * The event type will always be m.room.encrypted and event id will not be meaningful.
         */
        auto pendingRoomKeyEvents() const -> lager::reader<immer::flex_vector<PendingRoomKeyEvent>>;

        /**
         * Get the power levels of this room.
         *
         * @return A lager::reader that contains a PowerLevelDesc describing the power levels of this room.
         */
        auto powerLevels() const -> lager::reader<PowerLevelsDesc>;

        /**
         * Get a list of child events of a specified event.
         *
         * @param eventId The id of the event.
         * @param relType The type of the relationship.
         * @return A lager::reader of a RangeT of events that are related to the event via `relType`. It is sorted in the same order as the timeline.
         */
        auto relatedEvents(lager::reader<std::string> eventId, std::string relType) const -> lager::reader<EventList>;

        /**
         * Get a list of read receipts of some event in this room.
         *
         * @param eventId The id of the event.
         * @return A lager::reader of a RangeT of matrix id and timestamp of receipts of the event.
         */
        auto eventReaders(lager::reader<std::string> eventId) const -> lager::reader<immer::flex_vector<EventReader>>;

        /**
         * Post a read receipt for this room.
         *
         * @param eventId The event id the user has read up to.
         *
         * @return A Promise that resolves when the read receipt is sent.
         */
        PromiseT postReceipt(std::string eventId) const;

        /**
         * Get a list of event ids of unread notifications.
         *
         * @return A lager::reader of a RangeT of event ids that
         * has an unread notification.
         */
        auto unreadNotificationEventIds() const -> lager::reader<immer::flex_vector<std::string>>;

    private:
        const lager::reader<SdkModel> &sdkCursor() const;
        const lager::reader<RoomModel> &roomCursor() const;
        lager::reader<RoomModel> makeRoomCursor() const;
        std::string currentRoomId() const;
        /// if membership is invite, return inviteState; otherwise return stateEvents
        lager::reader<immer::map<KeyOfState, Event>> inviteStateOrState() const;
        /// if membership is invite and inviteState[key] exists, return inviteState[key], otherwise return stateEvents[key]
        lager::reader<Event> inviteStateOrStateEvent(lager::reader<KeyOfState> key) const;

        std::optional<lager::reader<SdkModel>> m_sdk;
        std::variant<lager::reader<std::string>, std::string> m_roomId;

        ContextT m_ctx;
        std::optional<DepsT> m_deps;
        lager::reader<RoomModel> m_roomCursor;
        KAZV_DECLARE_THREAD_ID();
        KAZV_DECLARE_EVENT_LOOP_THREAD_ID_KEEPER(m_deps.has_value() ? &lager::get<EventLoopThreadIdKeeper &>(m_deps.value()) : 0);
    };
}
