/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <zug/into_vector.hpp>
#include <immer/algorithm.hpp>
#include <debug.hpp>

#include "room.hpp"

namespace Kazv
{
    Room::Room(lager::reader<SdkModel> sdk,
               lager::reader<std::string> roomId,
               Context<ClientAction> ctx,
               DepsT deps)
        : m_sdk(sdk)
        , m_roomId(roomId)
        , m_ctx(ctx)
        , m_deps(deps)
        , m_roomCursor(makeRoomCursor())
    {
    }

    Room::Room(lager::reader<SdkModel> sdk,
               lager::reader<std::string> roomId,
               Context<ClientAction> ctx)
        : m_sdk(sdk)
        , m_roomId(roomId)
        , m_ctx(ctx)
        , m_deps(std::nullopt)
        , m_roomCursor(makeRoomCursor())
    {
    }

    Room::Room(InEventLoopTag, std::string roomId, ContextT ctx, DepsT deps)
        : m_sdk(std::nullopt)
        , m_roomId(roomId)
        , m_ctx(ctx)
        , m_deps(deps)
        , m_roomCursor(makeRoomCursor())
#ifdef KAZV_USE_THREAD_SAFETY_HELPER
        , KAZV_ON_EVENT_LOOP_VAR(true)
#endif
    {
    }

    Room Room::toEventLoop() const
    {
        assert(m_deps.has_value());
        return Room(InEventLoopTag{}, currentRoomId(), m_ctx, m_deps.value());
    }

    const lager::reader<SdkModel> &Room::sdkCursor() const
    {
        if (m_sdk.has_value()) { return m_sdk.value(); }

        assert(m_deps.has_value());

        return *lager::get<SdkModelCursorKey>(m_deps.value());
    }

    const lager::reader<RoomModel> &Room::roomCursor() const
    {
        return m_roomCursor;
    }

    lager::reader<RoomModel> Room::makeRoomCursor() const
    {
        return lager::match(m_roomId)(
            [&](const lager::reader<std::string> &roomId) -> lager::reader<RoomModel> {
                return lager::with(sdkCursor().map(&SdkModel::c)[&ClientModel::roomList], roomId)
                    .map([](auto rooms, auto id) {
                             return rooms[id];
                         }).make();
            },
            [&](const std::string &roomId) -> lager::reader<RoomModel> {
                return sdkCursor().map(&SdkModel::c)[&ClientModel::roomList].map([roomId](auto rooms) { return rooms[roomId]; }).make();
            });
    }

    std::string Room::currentRoomId() const
    {
        return lager::match(m_roomId)(
            [&](const lager::reader<std::string> &roomId) {
                return roomId.get();
            },
            [&](const std::string &roomId) {
                return roomId;
            });
    }

    auto Room::message(lager::reader<std::string> eventId) const -> lager::reader<Event>
    {
        return lager::with(roomCursor()[&RoomModel::messages], eventId)
            .map([](const auto &msgs, const auto &id) {
                return msgs[id];
            });
    }

    auto Room::localEcho(lager::reader<std::string> txnId) const -> lager::reader<LocalEchoDesc>
    {
        return lager::with(roomCursor()[&RoomModel::localEchoes], txnId)
            .map([](const auto localEchoes, const auto &id) {
                auto localEchoIt = std::find_if(
                    localEchoes.begin(), localEchoes.end(),
                    [id](const auto localEcho) {
                        return localEcho.txnId == id;
                    });
                if (localEchoIt == localEchoes.end()) {
                    return LocalEchoDesc {};
                }
                return *localEchoIt;
            });
    }

    auto Room::members() const -> lager::reader<immer::flex_vector<std::string>>
    {
        return roomCursor().map([](auto room) {
            return room.joinedMemberIds();
        });
    }

    auto Room::invitedMembers() const -> lager::reader<immer::flex_vector<std::string>>
    {
        return roomCursor().map([](auto room) {
            return room.invitedMemberIds();
        });
    }

    auto Room::knockedMembers() const -> lager::reader<immer::flex_vector<std::string>>
    {
        return roomCursor().map([](auto room) {
            return room.knockedMemberIds();
        });
    }

    auto Room::leftMembers() const -> lager::reader<immer::flex_vector<std::string>>
    {
        return roomCursor().map([](auto room) {
            return room.leftMemberIds();
        });
    }

    auto Room::bannedMembers() const -> lager::reader<immer::flex_vector<std::string>>
    {
        return roomCursor().map([](auto room) {
            return room.bannedMemberIds();
        });
    }

    auto Room::joinedMemberEvents() const -> lager::reader<EventList>
    {
        return roomCursor().map([](auto room) {
            return room.joinedMemberEvents();
        });
    }

    auto Room::invitedMemberEvents() const -> lager::reader<EventList>
    {
        return roomCursor().map([](auto room) {
            return room.invitedMemberEvents();
        });
    }

    auto Room::knockedMemberEvents() const -> lager::reader<EventList>
    {
        return roomCursor().map([](auto room) {
            return room.knockedMemberEvents();
        });
    }

    auto Room::leftMemberEvents() const -> lager::reader<EventList>
    {
        return roomCursor().map([](auto room) {
            return room.leftMemberEvents();
        });
    }

    auto Room::bannedMemberEvents() const -> lager::reader<EventList>
    {
        return roomCursor().map([](auto room) {
            return room.bannedMemberEvents();
        });
    }

    auto Room::memberEventByCursor(lager::reader<std::string> userId) const -> lager::reader<Event>
    {
        return inviteStateOrStateEvent(userId.map([](auto id) {
            return KeyOfState{"m.room.member", id};
        }));
    }

    auto Room::memberEventFor(std::string userId) const -> lager::reader<Event>
    {
        return memberEventByCursor(lager::make_constant(userId));
    }

    lager::reader<immer::map<KeyOfState, Event>> Room::inviteStateOrState() const
    {
        return lager::with(stateEvents(), inviteState(), membership())
            .map([](const auto &stateEv, const auto &inviteSt, const auto &mem) {
                if (mem == RoomMembership::Invite) {
                    return inviteSt;
                } else {
                    return stateEv;
                }
            });
    }

    lager::reader<Event> Room::inviteStateOrStateEvent(lager::reader<KeyOfState> key) const
    {
        return lager::with(stateEvents(), inviteState(), membership(), key)
            .map([](const auto &stateEv, const auto &inviteSt, const auto &mem, const auto &k) {
                if (mem == RoomMembership::Invite) {
                    auto maybePtr = inviteSt.find(k);
                    return maybePtr ? *maybePtr : stateEv[k];
                } else {
                    return stateEv[k];
                }
            });
    }

    auto Room::inviteState() const -> lager::reader<immer::map<KeyOfState, Event>>
    {
        return roomCursor()[&RoomModel::inviteState];
    }

    auto Room::heroMemberEvents() const -> lager::reader<immer::flex_vector<Event>>
    {
        using namespace lager::lenses;
        return roomCursor().map(&RoomModel::heroMemberEvents);
    }

    auto Room::heroDisplayNames() const
        -> lager::reader<immer::flex_vector<std::string>>
    {
        return heroMemberEvents()
            .xform(zug::map([](const auto &events) {
                return intoImmer(
                    immer::flex_vector<std::string>{},
                    zug::map([](const auto &event) {
                        auto content = event.content();
                        return content.get().contains("displayname")
                            ? content.get()["displayname"].template get<std::string>()
                            : std::string();
                    }),
                    events);
            }));
    }

    auto Room::nameOpt() const -> lager::reader<std::optional<std::string>>
    {
        using namespace lager::lenses;
        return inviteStateOrState()
            [KeyOfState{"m.room.name", ""}]
            [or_default]
            .xform(eventContent)
            .xform(zug::map([](const JsonWrap &content) {
                return content.get().contains("name")
                    ? std::optional<std::string>(content.get()["name"])
                    : std::nullopt;
            }));
    }

    auto Room::name() const -> lager::reader<std::string>
    {
        using namespace lager::lenses;
        return nameOpt()[value_or("<no name>")];
    }

    lager::reader<bool> Room::encrypted() const
    {
        return roomCursor()[&RoomModel::encrypted];
    }

    auto Room::localReadMarker() const
        -> lager::reader<std::string>
    {
        return roomCursor()[&RoomModel::localReadMarker];
    }

    auto Room::heroIds() const
        -> lager::reader<immer::flex_vector<std::string>>
    {
        return roomCursor().map([](const auto &room) {
            return room.heroIds;
        });
    }

    auto Room::joinedMemberCount() const -> lager::reader<std::size_t>
    {
        return roomCursor()[&RoomModel::joinedMemberCount];
    }

    auto Room::invitedMemberCount() const -> lager::reader<std::size_t>
    {
        return roomCursor()[&RoomModel::invitedMemberCount];
    }

    auto Room::avatarMxcUri() const -> lager::reader<std::string>
    {
        using namespace lager::lenses;
        return inviteStateOrState()
            [KeyOfState{"m.room.avatar", ""}]
            [or_default]
            .map([](Event ev) {
                auto content = ev.content().get();
                return content.contains("url")
                    ? std::string(content["url"])
                    : "";
            });
    }

    auto Room::setLocalDraft(std::string localDraft) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(UpdateRoomAction{+roomId(), SetLocalDraftAction{localDraft}});
    }

    auto Room::sendMessage(Event msg) const
        -> PromiseT
    {
        using namespace CursorOp;
        auto hasCrypto = ~sdkCursor().map([](const auto &sdk) -> bool {
                                        return sdk.c().crypto.has_value();
                                    });
        auto roomEncrypted = ~roomCursor()[&RoomModel::encrypted];
        auto noFullMembers = ~roomCursor()[&RoomModel::membersFullyLoaded]
            .map([](auto b) { return !b; });

        auto rid = +roomId();

        // Don't use m_ctx directly in the callbacks
        // as `this` may have been destroyed when
        // the callbacks are called.
        auto ctx = m_ctx;

        auto promise = ctx.createResolvedPromise(true);

        // If we do not need encryption just send it as-is
        if (! +allCursors(hasCrypto, roomEncrypted)) {
            return promise
                .then([ctx, rid, msg](auto succ) {
                          if (! succ) {
                              return ctx.createResolvedPromise(false);
                          }
                          return ctx.dispatch(SendMessageAction{rid, msg});
                      });
        }

        if (! m_deps) {
            return ctx.createResolvedPromise({false, json{{"error", "missing-deps"}}});
        }

        auto deps = m_deps.value();

        promise = promise.then([ctx, rid, msg](auto) {
            return ctx.dispatch(SaveLocalEchoAction{rid, msg});
        });

        auto saveLocalEchoPromise = promise;

        // If the room member list is not complete, load it fully first.
        if (+allCursors(hasCrypto, roomEncrypted, noFullMembers)) {
            kzo.client.dbg() << "The members of " << rid
                             << " are not fully loaded." << std::endl;

            promise = promise
                .then([ctx, rid](auto) { // SaveLocalEchoAction can't fail
                          return ctx.dispatch(GetRoomStatesAction{rid});
                      })
                .then([ctx, rid](auto succ) {
                          if (! succ) {
                              kzo.client.warn() << "Loading members of " << rid
                                                << " failed." << std::endl;
                              return ctx.createResolvedPromise(false);
                          } else {
                              // XXX remove the hard-coded initialSync parameter
                              return ctx.dispatch(QueryKeysAction{true})
                                  .then([](auto succ) {
                                            if (! succ) {
                                                kzo.client.warn() << "Query keys failed" << std::endl;
                                            }
                                            return succ;
                                        });
                          }
                      });
        }

        auto encryptEvent = [ctx, rid, msg, deps](auto &&status) {
            if (! status) { return ctx.createResolvedPromise(status); }

            kzo.client.dbg() << "encrypting megolm" << std::endl;

            auto &rg = lager::get<RandomInterface &>(deps);

            return ctx.dispatch(EncryptMegOlmEventAction{
                rid,
                msg,
                currentTimeMs(),
                rg.generateRange<RandomData>(EncryptMegOlmEventAction::maxRandomSize())
            });
        };

        auto saveEncryptedLocalEcho = [saveLocalEchoPromise, msg, rid, ctx](auto &&status) mutable {
            if (! status) { return ctx.createResolvedPromise(status); }

            return saveLocalEchoPromise
                .then([status, msg, rid, ctx](auto &&st) {
                    auto txnId = st.dataStr("txnId");
                    kzo.client.dbg() << "saving encrypted local echo with txn id " << txnId << std::endl;

                    auto encrypted = status.dataJson("encrypted");
                    auto decrypted = msg.originalJson();
                    auto event = Event(encrypted).setDecryptedJson(decrypted, Event::Decrypted);

                    return ctx.dispatch(SaveLocalEchoAction{rid, event, txnId});
                })
                .then([status](auto &&) {
                    return status;
                });
        };

        auto maybeSendKeys = [ctx, rid, r=toEventLoop(), deps](auto status) {
            if (! status) { return ctx.createResolvedPromise(status); }

            auto encryptedEvent = status.dataJson("encrypted");
            auto content = encryptedEvent.at("content");

            auto ret = ctx.createResolvedPromise({});
            if (status.data().get().contains("key")) {
                kzo.client.dbg() << "megolm session rotated, sending session key" << std::endl;

                auto key = status.dataStr("key");
                ret = ret
                    .then([rid, r, key, ctx, deps, sessionId=content.at("session_id")](auto &&) {
                        auto members = (+r.roomCursor()).joinedMemberIds();
                        auto client = (+r.sdkCursor()).c();
                        using DeviceMapT = immer::map<std::string, immer::flex_vector<std::string>>;

                        auto devicesToSend = accumulate(
                            members, DeviceMapT{}, [client](auto map, auto uid) {
                                return std::move(map)
                                    .set(uid, client.devicesToSendKeys(uid));
                            });
                        auto &rg = lager::get<RandomInterface &>(deps);

                        return ctx.dispatch(ClaimKeysAction{
                            rid, sessionId, key, devicesToSend,
                            rg.generateRange<RandomData>(ClaimKeysAction::randomSize(devicesToSend))
                        })
                            .then([rid, ctx, deps, devicesToSend](auto status) {
                                if (! status) { return ctx.createResolvedPromise({}); }

                                kzo.client.dbg() << "olm-encrypting key event" << std::endl;

                                auto keyEv = status.dataJson("keyEvent");
                                auto &rg = lager::get<RandomInterface &>(deps);

                                return ctx.dispatch(PrepareForSharingRoomKeyAction{
                                    rid,
                                    devicesToSend, keyEv,
                                    rg.generateRange<RandomData>(PrepareForSharingRoomKeyAction::randomSize(devicesToSend))
                                });
                            })
                            .then([ctx, r, devicesToSend, rid](auto status) {
                                if (! status) { return ctx.createResolvedPromise({}); }
                                auto txnId = status.dataStr("txnId");
                                return r.sendPendingKeyEvent(txnId);
                            });
                    });
            }
            return ret
                .then([ctx, prevStatus=status](auto status) {
                    if (! status) { return status; }
                    return prevStatus;
                });
        };

        auto sendEncryptedEvent = [ctx, rid, saveLocalEchoPromise, msg](auto status) mutable {
            if (! status) { return ctx.createResolvedPromise(status); }
            return saveLocalEchoPromise.then([ctx, rid, status, msg](auto st) {
                auto txnId = st.dataStr("txnId");
                kzo.client.dbg() << "sending encrypted message with txn id " << txnId << std::endl;
                auto encrypted = status.dataJson("encrypted");
                auto decrypted = msg.originalJson();
                auto event = Event(encrypted).setDecryptedJson(decrypted, Event::Decrypted);

                return ctx.dispatch(SendMessageAction{rid, event, txnId});
            });
        };

        auto maybeSetStatusToFailed = [ctx, saveLocalEchoPromise, rid](const auto &st) mutable {
            if (st.success()) {
                return ctx.createResolvedPromise(st);
            }

            return saveLocalEchoPromise
                .then([rid, ctx](const auto &saveLocalEchoStatus) {
                    auto txnId = saveLocalEchoStatus.dataStr("txnId");
                    return ctx.dispatch(UpdateLocalEchoStatusAction{rid, txnId, LocalEchoDesc::Failed});
                })
                .then([st](const auto &) { // can't fail
                    return st;
                });
        };

        return promise
            .then(encryptEvent)
            .then(saveEncryptedLocalEcho)
            .then(maybeSendKeys)
            .then(sendEncryptedEvent)
            .then(maybeSetStatusToFailed);
    }

    auto Room::sendTextMessage(std::string text) const
        -> PromiseT
    {
        json j{
            {"type", "m.room.message"},
            {"content", {
                    {"msgtype", "m.text"},
                    {"body", text}
                }
            }
        };
        Event e{j};
        return sendMessage(e);
    }

    auto Room::resendMessage(std::string txnId) const
        -> PromiseT
    {
        return m_ctx.createResolvedPromise({})
            .then([that=toEventLoop()](const auto &) {
                kzo.client.dbg() << "resending all pending key events" << std::endl;
                return that.sendAllPendingKeyEvents();
            })
            .then([that=toEventLoop(), txnId](const auto &sendKeyStatus) {
                if (!sendKeyStatus.success()) {
                    kzo.client.warn() << "resending all pending key events failed" << std::endl;
                    return that.m_ctx.createResolvedPromise(sendKeyStatus);
                }
                auto maybeLocalEcho = that.roomCursor().map([txnId](const auto &room) {
                    return room.getLocalEchoByTxnId(txnId);
                }).make();
                auto roomEncrypted = that.roomCursor()[&RoomModel::encrypted].make();

                if (!maybeLocalEcho.get()) {
                    return that.m_ctx.createResolvedPromise({false, json{
                        {"errorCode", "MOE_KAZV_MXC_NO_SUCH_TXNID"},
                        {"error", "No such txn id"}
                    }});
                }
                auto localEcho = maybeLocalEcho.get().value();
                auto event = localEcho.event;
                auto rid = that.roomId().make().get();
                if ((roomEncrypted.get() && event.encrypted()) || !roomEncrypted.get()) {
                    kzo.client.info() << "resending message with txn id " << localEcho.txnId << std::endl;
                    return that.m_ctx.dispatch(SendMessageAction{rid, event, localEcho.txnId});
                } else {
                    kzo.client.info() << "room encrypted but event isn't, just resend everything" << std::endl;
                    return that.m_ctx.dispatch(RoomListAction{UpdateRoomAction{rid, RemoveLocalEchoAction{localEcho.txnId}}})
                        .then([localEcho, that](auto) { // Can't fail
                            return that.sendMessage(localEcho.event);
                        });
                }
            });
    }

    auto Room::redactEvent(std::string eventId, std::optional<std::string> reason) const -> PromiseT
    {
        return m_ctx.dispatch(RedactEventAction{
                roomId().make().get(),
                eventId,
                reason
            });
    }

    auto Room::sendPendingKeyEvent(std::string txnId) const -> PromiseT
    {
        return m_ctx.createResolvedPromise({})
            .then([txnId, r=toEventLoop()](auto &&) {
                auto ctx = r.m_ctx;
                auto rid = r.roomId().make().get();
                kzo.client.dbg() << "sending key event as to-device message" << std::endl;

                kzo.client.dbg() << "txnId of key event: " << txnId << std::endl;
                auto maybePending = r.roomCursor().make().get().getPendingRoomKeyEventByTxnId(txnId);
                if (!maybePending.has_value()) {
                    kzo.client.warn() << "No such pending room key event";
                    return ctx.createResolvedPromise(EffectStatus(/* succ = */ false, json::object(
                        {{"errorCode", "MOE_KAZV_MXC_NO_SUCH_PENDING_ROOM_KEY_EVENT"},
                         {"error", "No such pending room key event"}}
                    )));
                }
                auto pending = maybePending.value();
                return ctx.dispatch(SendMultipleToDeviceMessagesAction{pending.messages})
                    .then([ctx, rid, txnId](const auto &sendToDeviceStatus) {
                        if (!sendToDeviceStatus.success()) {
                            return ctx.createResolvedPromise(sendToDeviceStatus);
                        } else {
                            return ctx.dispatch(UpdateRoomAction{rid, RemovePendingRoomKeyAction{txnId}});
                        }
                    });
            });
    }

    auto Room::sendAllPendingKeyEvents() const -> PromiseT
    {
        return m_ctx.createResolvedPromise({})
            .then([r=toEventLoop()](auto &&) {
                auto pendingEvents = r.pendingRoomKeyEvents().make().get();
                if (pendingEvents.empty()) {
                    return r.m_ctx.createResolvedPromise(EffectStatus(/* succ = */ true));
                } else {
                    auto txnId = pendingEvents[0].txnId;
                    return r.sendPendingKeyEvent(txnId)
                        .then([r, txnId](const auto &stat) {
                            if (!stat.success()) {
                                kzo.client.warn() << "Can't send pending key event of txnId " << txnId << std::endl;
                                return r.m_ctx.createResolvedPromise(stat);
                            }
                            return r.sendAllPendingKeyEvents();
                        });
                }
            });
    }

    auto Room::refreshRoomState() const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(GetRoomStatesAction{+roomId()});
    }

    auto Room::getStateEvent(std::string type, std::string stateKey) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(GetStateEventAction{+roomId(), type, stateKey});
    }

    auto Room::sendStateEvent(Event state) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(SendStateEventAction{+roomId(), state});
    }

    auto Room::setName(std::string name) const
        -> PromiseT
    {
        json j{
            {"type", "m.room.name"},
            {"content", {
                    {"name", name}
                }
            }
        };
        Event e{j};
        return sendStateEvent(e);
    }

    auto Room::setTopic(std::string topic) const
        -> PromiseT
    {
        json j{
            {"type", "m.room.topic"},
            {"content", {
                    {"topic", topic}
                }
            }
        };
        Event e{j};
        return sendStateEvent(e);
    }

    auto Room::invite(std::string userId) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(InviteToRoomAction{+roomId(), userId});
    }

    auto Room::typingUsers() const -> lager::reader<immer::flex_vector<std::string>>
    {
        using namespace lager::lenses;
        return ephemeral("m.typing")
            .xform(eventContent
                | jsonAtOr("user_ids", immer::flex_vector<std::string>{}));
    }

    auto Room::typingMemberEvents() const -> lager::reader<EventList>
    {
        return lager::with(typingUsers(), roomCursor()[&RoomModel::stateEvents])
            .map([](const auto &userIds, const auto &events) {
                return intoImmer(EventList{}, zug::map([events](const auto &id) {
                    return events[KeyOfState{"m.room.member", id}];
                }), userIds);
            });
    }

    auto Room::setTyping(bool typing, std::optional<int> timeoutMs) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(SetTypingAction{+roomId(), typing, timeoutMs});
    }

    auto Room::leave() const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(LeaveRoomAction{+roomId()});
    }

    auto Room::forget() const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(ForgetRoomAction{+roomId()});
    }

    auto Room::kick(std::string userId, std::optional<std::string> reason) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(KickAction{+roomId(), userId, reason});
    }

    auto Room::ban(std::string userId, std::optional<std::string> reason) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(BanAction{+roomId(), userId, reason});
    }

    auto Room::unban(std::string userId/*, std::optional<std::string> reason*/) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(UnbanAction{+roomId(), userId});
    }

    auto Room::setAccountData(Event accountDataEvent) const -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(SetAccountDataPerRoomAction{+roomId(), accountDataEvent});
    }

    auto Room::tags() const -> lager::reader<immer::map<std::string, double>>
    {
        return roomCursor().map(&RoomModel::tags);
    }

    auto Room::addOrSetTag(std::string tagId, std::optional<double> order) const -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.createResolvedPromise({})
            .then([that=toEventLoop(), tagId, order](auto) {
                return that.setAccountData((+that.roomCursor()).makeAddTagEvent(tagId, order));
            });
    }

    auto Room::removeTag(std::string tagId) const -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.createResolvedPromise({})
            .then([that=toEventLoop(), tagId](auto) {
                return that.setAccountData((+that.roomCursor()).makeRemoveTagEvent(tagId));
            });
    }

    auto Room::pinnedEvents() const -> lager::reader<immer::flex_vector<std::string>>
    {
        return state(KeyOfState{"m.room.pinned_events", ""})
            .xform(eventContent | jsonAtOr("pinned", immer::flex_vector<std::string>{}));
    }

    auto Room::setPinnedEvents(immer::flex_vector<std::string> eventIds) const
        -> PromiseT
    {
        json j{
            {"type", "m.room.pinned_events"},
            {"content", {
                    {"pinned", eventIds}
                }
            }
        };
        Event e{j};
        return sendStateEvent(e);
    }

    auto Room::pinEvents(immer::flex_vector<std::string> eventIds) const -> PromiseT
    {
        return m_ctx.createResolvedPromise({})
            .then([room=toEventLoop(), eventIds](auto) {
                auto curPinnedEvents = room.pinnedEvents().get();
                auto newPinnedEvents = intoImmer(
                    curPinnedEvents,
                    zug::filter([curPinnedEvents](const auto &eventId) {
                        return std::find(curPinnedEvents.begin(), curPinnedEvents.end(), eventId) == curPinnedEvents.end();
                    }),
                    eventIds
                );
                return room.setPinnedEvents(newPinnedEvents);
            });
    }

    auto Room::unpinEvents(immer::flex_vector<std::string> eventIds) const -> PromiseT
    {
        return m_ctx.createResolvedPromise({})
            .then([room=toEventLoop(), eventIds](auto) {
                auto curPinnedEvents = room.pinnedEvents().get();
                auto newPinnedEvents = intoImmer(
                    immer::flex_vector<std::string>(),
                    zug::filter([eventIds](const auto &eventId) {
                        return std::find(eventIds.begin(), eventIds.end(), eventId) == eventIds.end();
                    }),
                    curPinnedEvents
                );
                return room.setPinnedEvents(newPinnedEvents);
            });
    }

    auto Room::timelineEventIds() const -> lager::reader<immer::flex_vector<std::string>>
    {
        return roomCursor()[&RoomModel::timeline];
    }

    auto Room::messagesMap() const -> lager::reader<immer::map<std::string, Event>>
    {
        return roomCursor()[&RoomModel::messages];
    }

    auto Room::timelineEvents() const -> lager::reader<immer::flex_vector<Event>>
    {
        return roomCursor()
            .xform(zug::map([](auto r) {
                auto messages = r.messages;
                auto timeline = r.timeline;
                return intoImmer(
                    immer::flex_vector<Event>{},
                    zug::map([=](auto eventId) {
                        return messages[eventId];
                    }),
                    timeline);
            }));
    }

    auto Room::timelineGaps() const
        -> lager::reader<immer::map<std::string /* eventId */,
                                    std::string /* prevBatch */>>
    {
        return roomCursor()[&RoomModel::timelineGaps];
    }

    auto Room::paginateBackFromEvent(std::string eventId) const
        -> PromiseT
    {
        using namespace CursorOp;
        return m_ctx.dispatch(PaginateTimelineAction{
                +roomId(), eventId, std::nullopt});
    }

    auto Room::localEchoes() const -> lager::reader<immer::flex_vector<LocalEchoDesc>>
    {
        return roomCursor()[&RoomModel::localEchoes];
    }

    auto Room::removeLocalEcho(std::string txnId) const -> PromiseT
    {
        return m_ctx.dispatch(RoomListAction{UpdateRoomAction{
            currentRoomId(),
            RemoveLocalEchoAction{txnId},
        }});
    }

    auto Room::pendingRoomKeyEvents() const -> lager::reader<immer::flex_vector<PendingRoomKeyEvent>>
    {
        return roomCursor()[&RoomModel::pendingRoomKeyEvents];
    }

    auto Room::powerLevels() const -> lager::reader<PowerLevelsDesc>
    {
        return state({"m.room.power_levels", ""})
            .map([](const Event &event) {
                return PowerLevelsDesc(event);
            });
    }

    auto Room::relatedEvents(lager::reader<std::string> eventId, std::string relType) const -> lager::reader<EventList>
    {
        return lager::with(
            eventId,
            roomCursor()[&RoomModel::reverseEventRelationships],
            roomCursor()[&RoomModel::messages]
        ).map([relType](const auto &eid, const auto &rels, const auto &msgs) {
            auto relatedEvents = zug::into_vector(
                zug::map([msgs](const auto &id) {
                    return msgs[id];
                }),
                rels[eid][relType]
            );
            std::sort(relatedEvents.begin(), relatedEvents.end(), [](const Event &a, const Event &b) {
                return std::make_tuple(a.originServerTs(), a.id()) < std::make_tuple(b.originServerTs(), b.id());
            });

            return EventList(relatedEvents.begin(), relatedEvents.end());
        }).make();
    }

    auto Room::eventReaders(lager::reader<std::string> eventId) const -> lager::reader<immer::flex_vector<EventReader>>
    {
        return lager::with(
            roomCursor()[&RoomModel::readReceipts],
            roomCursor()[&RoomModel::eventReadUsers],
            eventId
        ).map([](const auto &receipts, const auto &readUsers, const auto &eid) {
            auto userIds = readUsers[eid];
            return intoImmer(
                immer::flex_vector<EventReader>{},
                zug::map([receipts](const auto &userId) {
                    return EventReader{userId, receipts[userId].timestamp};
                }),
                userIds
            );
        });
    }

    auto Room::postReceipt(std::string eventId) const -> PromiseT
    {
        return m_ctx.dispatch(PostReceiptAction{roomId().make().get(), eventId});
    }

    auto Room::unreadNotificationEventIds() const -> lager::reader<immer::flex_vector<std::string>>
    {
        return m_roomCursor[&RoomModel::unreadNotificationEventIds];
    }
}
