/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include "status-utils.hpp"

namespace Kazv
{
    detail::ReturnEffectStatusT failWithResponse(const BaseJob::Response &r)
    {
        auto code = r.errorCode();
        auto msg = r.errorMessage();
        auto d = json{
            {"error", msg},
            {"errorCode", code},
        };
        return detail::ReturnEffectStatusT{
            EffectStatus(/* succ = */ false, JsonWrap(d))
        };
    }

    detail::ReturnEffectStatusT failEffect(std::string errorCode, std::string errorMsg)
    {
        auto d = json{
            {"error", errorMsg},
            {"errorCode", errorCode},
        };

        return detail::ReturnEffectStatusT{
            EffectStatus(/* succ = */ false, JsonWrap(d))
        };
    }
}
