/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <lager/lenses/at.hpp>
#include <lager/lenses/attr.hpp>
#include "notification-handler.hpp"

namespace Kazv
{
    struct NotificationHandler::Private
    {
        Private(lager::reader<ClientModel> clientArg)
            : client(clientArg)
            , pushRulesDesc(
                clientArg
                [&ClientModel::accountData]
                ["m.push_rules"]
                [lager::lenses::or_default]
                .map([](const Event &event) {
                    return PushRulesDesc(event);
                })
            )
        {}

        lager::reader<ClientModel> client;
        lager::reader<PushRulesDesc> pushRulesDesc;
    };

    NotificationHandler::NotificationHandler(lager::reader<ClientModel> client)
        : m_d(new Private(client))
    {
    }

    NotificationHandler::~NotificationHandler() = default;

    KAZV_DEFINE_COPYABLE_UNIQUE_PTR(NotificationHandler, m_d)

    PushAction NotificationHandler::handleNotification(const Event &e) const
    {
        auto roomId = e.raw().get().contains("room_id")
            ? e.raw().get()["room_id"].template get<std::string>()
            : std::string();
        auto room = m_d->client[&ClientModel::roomList][&RoomListModel::rooms][roomId][lager::lenses::or_default].make().get();
        return m_d->pushRulesDesc.get().handle(e, room);
    }
}
