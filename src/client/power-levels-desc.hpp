/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <memory>
#include <cstdint>
#include <optional>

#include <copy-helper.hpp>

namespace Kazv
{
    class Event;

    using PowerLevel = std::int_fast64_t;

    /**
     * Represent a m.power_levels event.
     */
    class PowerLevelsDesc
    {
    public:
        /**
         * Construct a PowerLevelsDesc from an event.
         */
        PowerLevelsDesc(const Event &e);
        ~PowerLevelsDesc();

        KAZV_DECLARE_COPYABLE(PowerLevelsDesc);

        /**
         * Get the power level of a user in the room.
         *
         * @param userId The id of the user.
         * @return The power level of the user.
         */
        PowerLevel powerLevelOfUser(std::string userId) const;

        /**
         * Determine whether a user can send a non-state event.
         *
         * @param userId The id of the user.
         * @param eventType The type of the event to send.
         * @return Whether the user can send such an event.
         */
        bool canSendMessage(std::string userId, std::string eventType) const;

        /**
         * Determine whether a user can send a state event.
         *
         * @param userId The id of the user.
         * @param eventType The type of the event to send.
         * @return Whether the user can send such an event.
         */
        bool canSendState(std::string userId, std::string eventType) const;

        /**
         * Determine whether a user can invite someone else.
         *
         * @param userId The id of the user.
         * @return Whether the user can send such an event.
         */
        bool canInvite(std::string userId) const;

        /**
         * Determine whether a user can kick someone else.
         *
         * @param userId The id of the user.
         * @return Whether the user can send such an event.
         */
        bool canKick(std::string userId) const;

        /**
         * Determine whether a user can ban someone else.
         *
         * @param userId The id of the user.
         * @return Whether the user can send such an event.
         */
        bool canBan(std::string userId) const;

        /**
         * Determine whether a user can redact someone else's events.
         *
         * @param userId The id of the user.
         * @return Whether the user can send such an event.
         */
        bool canRedact(std::string userId) const;

        /**
         * Get the normalized event of this.
         *
         * @return The normalized event for this power levels desc.
         */
        Event normalizedEvent() const;

        /**
         * Get the original event provided when constructing this.
         *
         * @return The original event provided when constructing this.
         */
        Event originalEvent() const;

        /**
         * Amend the power levels event to set the power level needed
         * to ban someone.
         *
         * @param powerLevel The new power level needed to ban someone. Use std::nullopt to remove.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setBan(std::optional<PowerLevel> powerLevel) const;

        /**
         * Amend the power levels event to set the power level needed
         * to invite someone.
         *
         * @param powerLevel The new power level needed to invite someone. Use std::nullopt to remove.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setInvite(std::optional<PowerLevel> powerLevel) const;

        /**
         * Amend the power levels event to set the power level needed
         * to kick someone.
         *
         * @param powerLevel The new power level needed to kick someone. Use std::nullopt to remove.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setKick(std::optional<PowerLevel> powerLevel) const;

        /**
         * Amend the power levels event to set the power level needed
         * to redact someone else's messages.
         *
         * @param powerLevel The new power level needed to redact someone else's
         * messages. Use std::nullopt to remove.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setRedact(std::optional<PowerLevel> powerLevel) const;

        /**
         * Amend the power levels event to set the power level needed
         * to send an event of a particular type.
         *
         * @param eventType The type of the event.
         * @param powerLevel The new power level needed to send an event of this type. Use std::nullopt to remove.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setEvent(std::string eventType, std::optional<PowerLevel> powerLevel) const;

        /**
         * Amend the power levels event to set the power level needed
         * to send a non-state event whose type is not in `events`.
         *
         * @param powerLevel The new power level needed to send a non-state event of a type not in `events`. Use std::nullopt to remove.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setEventsDefault(std::optional<PowerLevel> powerLevel) const;

        /**
         * Amend the power levels event to set the power level needed
         * to send a state event whose type is not in `events`.
         *
         * @param powerLevel The new power level needed to send a state event of a type not in `events`. Use std::nullopt to remove.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setStateDefault(std::optional<PowerLevel> powerLevel) const;

        /**
         * Amend the power levels event to set the power level of some user.
         *
         * @param userId The id of the user.
         * @param powerLevel The new power level of this user.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setUser(std::string userId, std::optional<PowerLevel> powerLevel) const;

        /**
         * Amend the power levels event to set the default power level for users.
         *
         * @param powerLevel The new default power level for users in the room. Use std::nullopt to remove.
         * @return A new PowerLevelsDesc that reflects the amended event.
         */
        PowerLevelsDesc setUsersDefault(std::optional<PowerLevel> powerLevel) const;

    private:
        struct Private;
        std::unique_ptr<Private> m_d;
    };
}
