/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "basejob.hpp"
#include "event.hpp"

namespace Kazv::Api {

/*! \brief Retrieve a portion of a space tree.
 *
 * Paginates over the space tree in a depth-first manner to locate child rooms of a given space.
 * 
 * Where a child room is unknown to the local server, federation is used to fill in the details.
 * The servers listed in the `via` array should be contacted to attempt to fill in missing rooms.
 * 
 * Only [`m.space.child`](#mspacechild) state events of the room are considered. Invalid child
 * rooms and parent events are not covered by this endpoint.
 */
class GetSpaceHierarchyJob : public BaseJob {
public:
  // Inner data structures

/// Paginates over the space tree in a depth-first manner to locate child rooms of a given space.
/// 
/// Where a child room is unknown to the local server, federation is used to fill in the details.
/// The servers listed in the `via` array should be contacted to attempt to fill in missing rooms.
/// 
/// Only [`m.space.child`](#mspacechild) state events of the room are considered. Invalid child
/// rooms and parent events are not covered by this endpoint.
    struct StrippedChildStateEvent :
      Event
        {
/// The `origin_server_ts` for the event.
          std::int_fast64_t originServerTs;
        
        };

/// Paginates over the space tree in a depth-first manner to locate child rooms of a given space.
/// 
/// Where a child room is unknown to the local server, federation is used to fill in the details.
/// The servers listed in the `via` array should be contacted to attempt to fill in missing rooms.
/// 
/// Only [`m.space.child`](#mspacechild) state events of the room are considered. Invalid child
/// rooms and parent events are not covered by this endpoint.
    struct ChildRoomsChunk
        {
/// The number of members joined to the room.
          int numJoinedMembers;
/// The ID of the room.
          std::string roomId;
/// Whether the room may be viewed by guest users without joining.
          bool worldReadable;
/// Whether guest users may join the room and participate in it.
/// If they can, they will be subject to ordinary power level
/// rules like any other user.
          bool guestCanJoin;
/// The [`m.space.child`](#mspacechild) events of the space-room, represented
/// as [Stripped State Events](#stripped-state) with an added `origin_server_ts` key.
/// 
/// If the room is not a space-room, this should be empty.
          immer::array<StrippedChildStateEvent> childrenState;
/// The canonical alias of the room, if any.
          std::optional<std::string> canonicalAlias;
/// The name of the room, if any.
          std::optional<std::string> name;
/// The topic of the room, if any.
          std::optional<std::string> topic;
/// The URL for the room's avatar, if one is set.
          std::optional<std::string> avatarUrl;
/// The room's join rule. When not present, the room is assumed to
/// be `public`.
          std::optional<std::string> joinRule;
/// The `type` of room (from [`m.room.create`](/client-server-api/#mroomcreate)), if any.
          std::optional<std::string> roomType;
        
        };



class JobResponse : public Response
{

public:
  JobResponse(Response r);
  bool success() const;

    // Result properties
        
        

    
/// The rooms for the current page, with the current filters.
immer::array<ChildRoomsChunk> rooms() const;

    
/// A token to supply to `from` to keep paginating the responses. Not present when there are
/// no further results.
std::optional<std::string> nextBatch() const;

};
          static constexpr auto needsAuth() {
          return true
            ;
              }


// Construction/destruction

  /*! \brief Retrieve a portion of a space tree.
 *
    * \param roomId
    *   The room ID of the space to get a hierarchy for.
    * 
    * \param suggestedOnly
    *   Optional (default `false`) flag to indicate whether or not the server should only consider
    *   suggested rooms. Suggested rooms are annotated in their [`m.space.child`](#mspacechild) event
    *   contents.
    * 
    * \param limit
    *   Optional limit for the maximum number of rooms to include per response. Must be an integer
    *   greater than zero.
    *   
    *   Servers should apply a default value, and impose a maximum value to avoid resource exhaustion.
    * 
    * \param maxDepth
    *   Optional limit for how far to go into the space. Must be a non-negative integer.
    *   
    *   When reached, no further child rooms will be returned.
    *   
    *   Servers should apply a default value, and impose a maximum value to avoid resource exhaustion.
    * 
    * \param from
    *   A pagination token from a previous result. If specified, `max_depth` and `suggested_only` cannot
    *   be changed from the first request.
    */
    explicit GetSpaceHierarchyJob(std::string serverUrl
    , std::string _accessToken
      ,
        std::string roomId , std::optional<bool> suggestedOnly  = std::nullopt, std::optional<int> limit  = std::nullopt, std::optional<int> maxDepth  = std::nullopt, std::optional<std::string> from  = std::nullopt
        );


    static BaseJob::Query buildQuery(
    std::optional<bool> suggestedOnly, std::optional<int> limit, std::optional<int> maxDepth, std::optional<std::string> from);

      static BaseJob::Body buildBody(std::string roomId, std::optional<bool> suggestedOnly, std::optional<int> limit, std::optional<int> maxDepth, std::optional<std::string> from);

        

        

      GetSpaceHierarchyJob withData(JsonWrap j) &&;
      GetSpaceHierarchyJob withData(JsonWrap j) const &;
      };
      using GetSpaceHierarchyResponse = GetSpaceHierarchyJob::JobResponse;
      } 
      namespace nlohmann
      {
      using namespace Kazv;
      using namespace Kazv::Api;
      template<>
      struct adl_serializer<GetSpaceHierarchyJob::StrippedChildStateEvent> {

  static void to_json(json& jo, const GetSpaceHierarchyJob::StrippedChildStateEvent &pod)
  {
  if (! jo.is_object()) { jo = json::object(); }
    jo = static_cast<const Event &>(pod);
    //nlohmann::to_json(jo, static_cast<const Event &>(pod));
  
    jo["origin_server_ts"s] = pod.originServerTs;
    
  }

  static void from_json(const json &jo, GetSpaceHierarchyJob::StrippedChildStateEvent& result)
  {
    static_cast<Event &>(result) = jo;
    //nlohmann::from_json(jo, static_cast<const Event &>(result));
    if (jo.contains("origin_server_ts"s)) {
      result.originServerTs = jo.at("origin_server_ts"s);
    }
  
  }

};
      template<>
      struct adl_serializer<GetSpaceHierarchyJob::ChildRoomsChunk> {

  static void to_json(json& jo, const GetSpaceHierarchyJob::ChildRoomsChunk &pod)
  {
  if (! jo.is_object()) { jo = json::object(); }
  
  
    jo["num_joined_members"s] = pod.numJoinedMembers;
    
    jo["room_id"s] = pod.roomId;
    
    jo["world_readable"s] = pod.worldReadable;
    
    jo["guest_can_join"s] = pod.guestCanJoin;
    
    jo["children_state"s] = pod.childrenState;
    
    
    addToJsonIfNeeded(jo, "canonical_alias"s, pod.canonicalAlias);
    
    addToJsonIfNeeded(jo, "name"s, pod.name);
    
    addToJsonIfNeeded(jo, "topic"s, pod.topic);
    
    addToJsonIfNeeded(jo, "avatar_url"s, pod.avatarUrl);
    
    addToJsonIfNeeded(jo, "join_rule"s, pod.joinRule);
    
    addToJsonIfNeeded(jo, "room_type"s, pod.roomType);
  }

  static void from_json(const json &jo, GetSpaceHierarchyJob::ChildRoomsChunk& result)
  {
  
    if (jo.contains("num_joined_members"s)) {
      result.numJoinedMembers = jo.at("num_joined_members"s);
    }
    if (jo.contains("room_id"s)) {
      result.roomId = jo.at("room_id"s);
    }
    if (jo.contains("world_readable"s)) {
      result.worldReadable = jo.at("world_readable"s);
    }
    if (jo.contains("guest_can_join"s)) {
      result.guestCanJoin = jo.at("guest_can_join"s);
    }
    if (jo.contains("children_state"s)) {
      result.childrenState = jo.at("children_state"s);
    }
    if (jo.contains("canonical_alias"s)) {
      result.canonicalAlias = jo.at("canonical_alias"s);
    }
    if (jo.contains("name"s)) {
      result.name = jo.at("name"s);
    }
    if (jo.contains("topic"s)) {
      result.topic = jo.at("topic"s);
    }
    if (jo.contains("avatar_url"s)) {
      result.avatarUrl = jo.at("avatar_url"s);
    }
    if (jo.contains("join_rule"s)) {
      result.joinRule = jo.at("join_rule"s);
    }
    if (jo.contains("room_type"s)) {
      result.roomType = jo.at("room_type"s);
    }
  
  }

};
    }

    namespace Kazv::Api
    {

} // namespace Kazv::Api
