/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "relations.hpp"

namespace Kazv::Api
{

  
BaseJob::Query GetRelatingEventsJob::buildQuery(
std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit)
{
BaseJob::Query _q;
  
    addToQueryIfNeeded(_q, "from"s, from);
  
    addToQueryIfNeeded(_q, "to"s, to);
  
    addToQueryIfNeeded(_q, "limit"s, limit);
return _q;
}

    BaseJob::Body GetRelatingEventsJob::buildBody(std::string roomId, std::string eventId, std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit)
      {
      // ignore unused param
      (void)(roomId);(void)(eventId);(void)(from);(void)(to);(void)(limit);
      
      
              return BaseJob::EmptyBody{};

      };

      

GetRelatingEventsJob::GetRelatingEventsJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        std::string roomId, std::string eventId, std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v1") + "/rooms/" + roomId + "/relations/" + eventId,
          GET,
          std::string("GetRelatingEvents"),
          _accessToken,
          ReturnType::Json,
            buildBody(roomId, eventId, from, to, limit)
              , buildQuery(from, to, limit)
                , {}

)
        {
        }

        GetRelatingEventsJob GetRelatingEventsJob::withData(JsonWrap j) &&
        {
          auto ret = GetRelatingEventsJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        GetRelatingEventsJob GetRelatingEventsJob::withData(JsonWrap j) const &
        {
          auto ret = GetRelatingEventsJob(*this);
          ret.attachData(j);
          return ret;
        }

        GetRelatingEventsJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool GetRelatingEventsResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
            && jsonBody().get().contains("chunk"s)
          ;
          }


    
    EventList GetRelatingEventsResponse::chunk() const
    {
    if (jsonBody().get()
    .contains("chunk"s)) {
    return
    jsonBody().get()["chunk"s]
    .template get<EventList>();}
    else { return EventList(  );}
    }

    
    std::optional<std::string> GetRelatingEventsResponse::nextBatch() const
    {
    if (jsonBody().get()
    .contains("next_batch"s)) {
    return
    jsonBody().get()["next_batch"s]
    .template get<std::string>();}
    else { return std::optional<std::string>(  );}
    }

    
    std::optional<std::string> GetRelatingEventsResponse::prevBatch() const
    {
    if (jsonBody().get()
    .contains("prev_batch"s)) {
    return
    jsonBody().get()["prev_batch"s]
    .template get<std::string>();}
    else { return std::optional<std::string>(  );}
    }


  
BaseJob::Query GetRelatingEventsWithRelTypeJob::buildQuery(
std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit)
{
BaseJob::Query _q;
  
    addToQueryIfNeeded(_q, "from"s, from);
  
    addToQueryIfNeeded(_q, "to"s, to);
  
    addToQueryIfNeeded(_q, "limit"s, limit);
return _q;
}

    BaseJob::Body GetRelatingEventsWithRelTypeJob::buildBody(std::string roomId, std::string eventId, std::string relType, std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit)
      {
      // ignore unused param
      (void)(roomId);(void)(eventId);(void)(relType);(void)(from);(void)(to);(void)(limit);
      
      
              return BaseJob::EmptyBody{};

      };

      

GetRelatingEventsWithRelTypeJob::GetRelatingEventsWithRelTypeJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        std::string roomId, std::string eventId, std::string relType, std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v1") + "/rooms/" + roomId + "/relations/" + eventId + "/" + relType,
          GET,
          std::string("GetRelatingEventsWithRelType"),
          _accessToken,
          ReturnType::Json,
            buildBody(roomId, eventId, relType, from, to, limit)
              , buildQuery(from, to, limit)
                , {}

)
        {
        }

        GetRelatingEventsWithRelTypeJob GetRelatingEventsWithRelTypeJob::withData(JsonWrap j) &&
        {
          auto ret = GetRelatingEventsWithRelTypeJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        GetRelatingEventsWithRelTypeJob GetRelatingEventsWithRelTypeJob::withData(JsonWrap j) const &
        {
          auto ret = GetRelatingEventsWithRelTypeJob(*this);
          ret.attachData(j);
          return ret;
        }

        GetRelatingEventsWithRelTypeJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool GetRelatingEventsWithRelTypeResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
            && jsonBody().get().contains("chunk"s)
          ;
          }


    
    EventList GetRelatingEventsWithRelTypeResponse::chunk() const
    {
    if (jsonBody().get()
    .contains("chunk"s)) {
    return
    jsonBody().get()["chunk"s]
    .template get<EventList>();}
    else { return EventList(  );}
    }

    
    std::optional<std::string> GetRelatingEventsWithRelTypeResponse::nextBatch() const
    {
    if (jsonBody().get()
    .contains("next_batch"s)) {
    return
    jsonBody().get()["next_batch"s]
    .template get<std::string>();}
    else { return std::optional<std::string>(  );}
    }

    
    std::optional<std::string> GetRelatingEventsWithRelTypeResponse::prevBatch() const
    {
    if (jsonBody().get()
    .contains("prev_batch"s)) {
    return
    jsonBody().get()["prev_batch"s]
    .template get<std::string>();}
    else { return std::optional<std::string>(  );}
    }


  
BaseJob::Query GetRelatingEventsWithRelTypeAndEventTypeJob::buildQuery(
std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit)
{
BaseJob::Query _q;
  
    addToQueryIfNeeded(_q, "from"s, from);
  
    addToQueryIfNeeded(_q, "to"s, to);
  
    addToQueryIfNeeded(_q, "limit"s, limit);
return _q;
}

    BaseJob::Body GetRelatingEventsWithRelTypeAndEventTypeJob::buildBody(std::string roomId, std::string eventId, std::string relType, std::string eventType, std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit)
      {
      // ignore unused param
      (void)(roomId);(void)(eventId);(void)(relType);(void)(eventType);(void)(from);(void)(to);(void)(limit);
      
      
              return BaseJob::EmptyBody{};

      };

      

GetRelatingEventsWithRelTypeAndEventTypeJob::GetRelatingEventsWithRelTypeAndEventTypeJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        std::string roomId, std::string eventId, std::string relType, std::string eventType, std::optional<std::string> from, std::optional<std::string> to, std::optional<int> limit
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v1") + "/rooms/" + roomId + "/relations/" + eventId + "/" + relType + "/" + eventType,
          GET,
          std::string("GetRelatingEventsWithRelTypeAndEventType"),
          _accessToken,
          ReturnType::Json,
            buildBody(roomId, eventId, relType, eventType, from, to, limit)
              , buildQuery(from, to, limit)
                , {}

)
        {
        }

        GetRelatingEventsWithRelTypeAndEventTypeJob GetRelatingEventsWithRelTypeAndEventTypeJob::withData(JsonWrap j) &&
        {
          auto ret = GetRelatingEventsWithRelTypeAndEventTypeJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        GetRelatingEventsWithRelTypeAndEventTypeJob GetRelatingEventsWithRelTypeAndEventTypeJob::withData(JsonWrap j) const &
        {
          auto ret = GetRelatingEventsWithRelTypeAndEventTypeJob(*this);
          ret.attachData(j);
          return ret;
        }

        GetRelatingEventsWithRelTypeAndEventTypeJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool GetRelatingEventsWithRelTypeAndEventTypeResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
            && jsonBody().get().contains("chunk"s)
          ;
          }


    
    EventList GetRelatingEventsWithRelTypeAndEventTypeResponse::chunk() const
    {
    if (jsonBody().get()
    .contains("chunk"s)) {
    return
    jsonBody().get()["chunk"s]
    .template get<EventList>();}
    else { return EventList(  );}
    }

    
    std::optional<std::string> GetRelatingEventsWithRelTypeAndEventTypeResponse::nextBatch() const
    {
    if (jsonBody().get()
    .contains("next_batch"s)) {
    return
    jsonBody().get()["next_batch"s]
    .template get<std::string>();}
    else { return std::optional<std::string>(  );}
    }

    
    std::optional<std::string> GetRelatingEventsWithRelTypeAndEventTypeResponse::prevBatch() const
    {
    if (jsonBody().get()
    .contains("prev_batch"s)) {
    return
    jsonBody().get()["prev_batch"s]
    .template get<std::string>();}
    else { return std::optional<std::string>(  );}
    }

}
