/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "refresh.hpp"

namespace Kazv::Api
{

  
BaseJob::Query RefreshJob::buildQuery(
)
{
BaseJob::Query _q;

return _q;
}

    BaseJob::Body RefreshJob::buildBody(std::optional<std::string> refreshToken)
      {
      // ignore unused param
      (void)(refreshToken);
      
        json _data
        
         = json::object();
        
          
            addToJsonIfNeeded(_data, "refresh_token"s, refreshToken);
        return BaseJob::JsonBody(_data);
        

      };

      

RefreshJob::RefreshJob(
        std::string serverUrl
        
            ,
        std::optional<std::string> refreshToken
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v3") + "/refresh",
          POST,
          std::string("Refresh"),
           {} ,
          ReturnType::Json,
            buildBody(refreshToken)
              , buildQuery()
                , {}

)
        {
        }

        RefreshJob RefreshJob::withData(JsonWrap j) &&
        {
          auto ret = RefreshJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        RefreshJob RefreshJob::withData(JsonWrap j) const &
        {
          auto ret = RefreshJob(*this);
          ret.attachData(j);
          return ret;
        }

        RefreshJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool RefreshResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
            && jsonBody().get().contains("access_token"s)
          ;
          }


    
    std::string RefreshResponse::accessToken() const
    {
    if (jsonBody().get()
    .contains("access_token"s)) {
    return
    jsonBody().get()["access_token"s]
    .template get<std::string>();}
    else { return std::string(  );}
    }

    
    std::optional<std::string> RefreshResponse::refreshToken() const
    {
    if (jsonBody().get()
    .contains("refresh_token"s)) {
    return
    jsonBody().get()["refresh_token"s]
    .template get<std::string>();}
    else { return std::optional<std::string>(  );}
    }

    
    std::optional<int> RefreshResponse::expiresInMs() const
    {
    if (jsonBody().get()
    .contains("expires_in_ms"s)) {
    return
    jsonBody().get()["expires_in_ms"s]
    .template get<int>();}
    else { return std::optional<int>(  );}
    }

}
