/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "basejob.hpp"


namespace Kazv::Api {

/*! \brief Refresh an access token
 *
 * Refresh an access token. Clients should use the returned access token
 * when making subsequent API calls, and store the returned refresh token
 * (if given) in order to refresh the new access token when necessary.
 * 
 * After an access token has been refreshed, a server can choose to
 * invalidate the old access token immediately, or can choose not to, for
 * example if the access token would expire soon anyways. Clients should
 * not make any assumptions about the old access token still being valid,
 * and should use the newly provided access token instead.
 * 
 * The old refresh token remains valid until the new access token or refresh token
 * is used, at which point the old refresh token is revoked.
 * 
 * Note that this endpoint does not require authentication via an
 * access token. Authentication is provided via the refresh token.
 * 
 * Application Service identity assertion is disabled for this endpoint.
 */
class RefreshJob : public BaseJob {
public:



class JobResponse : public Response
{

public:
  JobResponse(Response r);
  bool success() const;

    // Result properties
        
        

    
/// The new access token to use.
std::string accessToken() const;

    
/// The new refresh token to use when the access token needs to
/// be refreshed again. If not given, the old refresh token can
/// be re-used.
std::optional<std::string> refreshToken() const;

    
/// The lifetime of the access token, in milliseconds. If not
/// given, the client can assume that the access token will not
/// expire.
std::optional<int> expiresInMs() const;

};
          static constexpr auto needsAuth() {
          return 
            false;
              }


// Construction/destruction

  /*! \brief Refresh an access token
 *
    * \param refreshToken
    *   The refresh token
    */
    explicit RefreshJob(std::string serverUrl
    
      ,
        std::optional<std::string> refreshToken  = std::nullopt
        );
    

    static BaseJob::Query buildQuery(
    );

      static BaseJob::Body buildBody(std::optional<std::string> refreshToken);

        

        

      RefreshJob withData(JsonWrap j) &&;
      RefreshJob withData(JsonWrap j) const &;
      };
      using RefreshResponse = RefreshJob::JobResponse;
      } 
      namespace nlohmann
      {
      using namespace Kazv;
      using namespace Kazv::Api;
    
    }

    namespace Kazv::Api
    {

} // namespace Kazv::Api
