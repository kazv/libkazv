/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "appservice_room_directory.hpp"

namespace Kazv::Api
{

  
BaseJob::Query UpdateAppserviceRoomDirectoryVisibilityJob::buildQuery(
)
{
BaseJob::Query _q;

return _q;
}

    BaseJob::Body UpdateAppserviceRoomDirectoryVisibilityJob::buildBody(std::string networkId, std::string roomId, std::string visibility)
      {
      // ignore unused param
      (void)(networkId);(void)(roomId);(void)(visibility);
      
        json _data
        
         = json::object();
        
            _data["visibility"s] = visibility;
          
        return BaseJob::JsonBody(_data);
        

      };

      

UpdateAppserviceRoomDirectoryVisibilityJob::UpdateAppserviceRoomDirectoryVisibilityJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        std::string networkId, std::string roomId, std::string visibility
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v3") + "/directory/list/appservice/" + networkId + "/" + roomId,
          PUT,
          std::string("UpdateAppserviceRoomDirectoryVisibility"),
          _accessToken,
          ReturnType::Json,
            buildBody(networkId, roomId, visibility)
              , buildQuery()
                , {}

)
        {
        }

        UpdateAppserviceRoomDirectoryVisibilityJob UpdateAppserviceRoomDirectoryVisibilityJob::withData(JsonWrap j) &&
        {
          auto ret = UpdateAppserviceRoomDirectoryVisibilityJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        UpdateAppserviceRoomDirectoryVisibilityJob UpdateAppserviceRoomDirectoryVisibilityJob::withData(JsonWrap j) const &
        {
          auto ret = UpdateAppserviceRoomDirectoryVisibilityJob(*this);
          ret.attachData(j);
          return ret;
        }

        UpdateAppserviceRoomDirectoryVisibilityJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool UpdateAppserviceRoomDirectoryVisibilityResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
          ;
          }


}
