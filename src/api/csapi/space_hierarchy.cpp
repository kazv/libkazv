/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "space_hierarchy.hpp"

namespace Kazv::Api
{

  
BaseJob::Query GetSpaceHierarchyJob::buildQuery(
std::optional<bool> suggestedOnly, std::optional<int> limit, std::optional<int> maxDepth, std::optional<std::string> from)
{
BaseJob::Query _q;
  
    addToQueryIfNeeded(_q, "suggested_only"s, suggestedOnly);
  
    addToQueryIfNeeded(_q, "limit"s, limit);
  
    addToQueryIfNeeded(_q, "max_depth"s, maxDepth);
  
    addToQueryIfNeeded(_q, "from"s, from);
return _q;
}

    BaseJob::Body GetSpaceHierarchyJob::buildBody(std::string roomId, std::optional<bool> suggestedOnly, std::optional<int> limit, std::optional<int> maxDepth, std::optional<std::string> from)
      {
      // ignore unused param
      (void)(roomId);(void)(suggestedOnly);(void)(limit);(void)(maxDepth);(void)(from);
      
      
              return BaseJob::EmptyBody{};

      };

      

GetSpaceHierarchyJob::GetSpaceHierarchyJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        std::string roomId, std::optional<bool> suggestedOnly, std::optional<int> limit, std::optional<int> maxDepth, std::optional<std::string> from
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v1") + "/rooms/" + roomId + "/hierarchy",
          GET,
          std::string("GetSpaceHierarchy"),
          _accessToken,
          ReturnType::Json,
            buildBody(roomId, suggestedOnly, limit, maxDepth, from)
              , buildQuery(suggestedOnly, limit, maxDepth, from)
                , {}

)
        {
        }

        GetSpaceHierarchyJob GetSpaceHierarchyJob::withData(JsonWrap j) &&
        {
          auto ret = GetSpaceHierarchyJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        GetSpaceHierarchyJob GetSpaceHierarchyJob::withData(JsonWrap j) const &
        {
          auto ret = GetSpaceHierarchyJob(*this);
          ret.attachData(j);
          return ret;
        }

        GetSpaceHierarchyJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool GetSpaceHierarchyResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
            && jsonBody().get().contains("rooms"s)
          ;
          }


    
    immer::array<GetSpaceHierarchyJob::ChildRoomsChunk> GetSpaceHierarchyResponse::rooms() const
    {
    if (jsonBody().get()
    .contains("rooms"s)) {
    return
    jsonBody().get()["rooms"s]
    .template get<immer::array<ChildRoomsChunk>>();}
    else { return immer::array<ChildRoomsChunk>(  );}
    }

    
    std::optional<std::string> GetSpaceHierarchyResponse::nextBatch() const
    {
    if (jsonBody().get()
    .contains("next_batch"s)) {
    return
    jsonBody().get()["next_batch"s]
    .template get<std::string>();}
    else { return std::optional<std::string>(  );}
    }

}
