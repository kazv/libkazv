/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "inviting.hpp"

namespace Kazv::Api
{

  
BaseJob::Query InviteUserJob::buildQuery(
)
{
BaseJob::Query _q;

return _q;
}

    BaseJob::Body InviteUserJob::buildBody(std::string roomId, std::string userId, std::optional<std::string> reason)
      {
      // ignore unused param
      (void)(roomId);(void)(userId);(void)(reason);
      
        json _data
        
         = json::object();
        
            _data["user_id"s] = userId;
          
          
            addToJsonIfNeeded(_data, "reason"s, reason);
        return BaseJob::JsonBody(_data);
        

      };

      

InviteUserJob::InviteUserJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        std::string roomId, std::string userId, std::optional<std::string> reason
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v3") + "/rooms/" + roomId + "/invite",
          POST,
          std::string("InviteUser"),
          _accessToken,
          ReturnType::Json,
            buildBody(roomId, userId, reason)
              , buildQuery()
                , {}

)
        {
        }

        InviteUserJob InviteUserJob::withData(JsonWrap j) &&
        {
          auto ret = InviteUserJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        InviteUserJob InviteUserJob::withData(JsonWrap j) const &
        {
          auto ret = InviteUserJob(*this);
          ret.attachData(j);
          return ret;
        }

        InviteUserJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool InviteUserResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
          ;
          }


}
