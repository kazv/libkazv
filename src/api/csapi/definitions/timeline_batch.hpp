/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "types.hpp"
#include "event.hpp"

namespace Kazv::Api {

struct Timeline
{       

/// List of events.
    EventList events;

/// True if the number of events returned was limited by the `limit` on the filter.
    std::optional<bool> limited;

/// A token that can be supplied to the `from` parameter of the [`/rooms/<room_id>/messages`](#get_matrixclientv3roomsroomidmessages) endpoint in order to retrieve earlier events.
/// If no earlier events are available, this property may be omitted from the response.
    std::optional<std::string> prevBatch;
};

}
namespace nlohmann
{
using namespace Kazv;
using namespace Kazv::Api;
template<>
struct adl_serializer<Timeline> {
  static void to_json(json& jo, const Timeline &pod)
  {
  if (! jo.is_object()) { jo = json::object(); }
  
  
    jo["events"s] = pod.events;
    
    
    addToJsonIfNeeded(jo, "limited"s, pod.limited);
    
    addToJsonIfNeeded(jo, "prev_batch"s, pod.prevBatch);
  }
  static void from_json(const json &jo, Timeline& result)
  {
  
    if (jo.contains("events"s)) {
      result.events = jo.at("events"s);
    }
    if (jo.contains("limited"s)) {
      result.limited = jo.at("limited"s);
    }
    if (jo.contains("prev_batch"s)) {
      result.prevBatch = jo.at("prev_batch"s);
    }
  
  }
};
    }

    namespace Kazv::Api
    {
} // namespace Kazv::Api
