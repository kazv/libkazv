/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "types.hpp"


namespace Kazv::Api {

struct PublicRoomsChunk
{       

/// The number of members joined to the room.
    int numJoinedMembers;

/// The ID of the room.
    std::string roomId;

/// Whether the room may be viewed by guest users without joining.
    bool worldReadable;

/// Whether guest users may join the room and participate in it.
/// If they can, they will be subject to ordinary power level
/// rules like any other user.
    bool guestCanJoin;

/// The canonical alias of the room, if any.
    std::optional<std::string> canonicalAlias;

/// The name of the room, if any.
    std::optional<std::string> name;

/// The topic of the room, if any.
    std::optional<std::string> topic;

/// The URL for the room's avatar, if one is set.
    std::optional<std::string> avatarUrl;

/// The room's join rule. When not present, the room is assumed to
/// be `public`. Note that rooms with `invite` join rules are not
/// expected here, but rooms with `knock` rules are given their
/// near-public nature.
    std::optional<std::string> joinRule;
};

}
namespace nlohmann
{
using namespace Kazv;
using namespace Kazv::Api;
template<>
struct adl_serializer<PublicRoomsChunk> {
  static void to_json(json& jo, const PublicRoomsChunk &pod)
  {
  if (! jo.is_object()) { jo = json::object(); }
  
  
    jo["num_joined_members"s] = pod.numJoinedMembers;
    
    jo["room_id"s] = pod.roomId;
    
    jo["world_readable"s] = pod.worldReadable;
    
    jo["guest_can_join"s] = pod.guestCanJoin;
    
    
    addToJsonIfNeeded(jo, "canonical_alias"s, pod.canonicalAlias);
    
    addToJsonIfNeeded(jo, "name"s, pod.name);
    
    addToJsonIfNeeded(jo, "topic"s, pod.topic);
    
    addToJsonIfNeeded(jo, "avatar_url"s, pod.avatarUrl);
    
    addToJsonIfNeeded(jo, "join_rule"s, pod.joinRule);
  }
  static void from_json(const json &jo, PublicRoomsChunk& result)
  {
  
    if (jo.contains("num_joined_members"s)) {
      result.numJoinedMembers = jo.at("num_joined_members"s);
    }
    if (jo.contains("room_id"s)) {
      result.roomId = jo.at("room_id"s);
    }
    if (jo.contains("world_readable"s)) {
      result.worldReadable = jo.at("world_readable"s);
    }
    if (jo.contains("guest_can_join"s)) {
      result.guestCanJoin = jo.at("guest_can_join"s);
    }
    if (jo.contains("canonical_alias"s)) {
      result.canonicalAlias = jo.at("canonical_alias"s);
    }
    if (jo.contains("name"s)) {
      result.name = jo.at("name"s);
    }
    if (jo.contains("topic"s)) {
      result.topic = jo.at("topic"s);
    }
    if (jo.contains("avatar_url"s)) {
      result.avatarUrl = jo.at("avatar_url"s);
    }
    if (jo.contains("join_rule"s)) {
      result.joinRule = jo.at("join_rule"s);
    }
  
  }
};
    }

    namespace Kazv::Api
    {
} // namespace Kazv::Api
