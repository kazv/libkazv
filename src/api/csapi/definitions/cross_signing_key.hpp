/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "types.hpp"


namespace Kazv::Api {
/// Cross signing key
struct CrossSigningKey
{       

/// The ID of the user the key belongs to.
    std::string userId;

/// What the key is used for.
    immer::array<std::string> usage;

/// The public key.  The object must have exactly one property, whose name is
/// in the form `<algorithm>:<unpadded_base64_public_key>`, and whose value
/// is the unpadded base64 public key.
    immer::map<std::string, std::string> keys;

/// Signatures of the key, calculated using the process described at [Signing JSON](/appendices/#signing-json).
/// Optional for the master key. Other keys must be signed by the
/// user\'s master key.
    JsonWrap signatures;
};

}
namespace nlohmann
{
using namespace Kazv;
using namespace Kazv::Api;
template<>
struct adl_serializer<CrossSigningKey> {
  static void to_json(json& jo, const CrossSigningKey &pod)
  {
  if (! jo.is_object()) { jo = json::object(); }
  
  
    jo["user_id"s] = pod.userId;
    
    jo["usage"s] = pod.usage;
    
    jo["keys"s] = pod.keys;
    
    
    addToJsonIfNeeded(jo, "signatures"s, pod.signatures);
  }
  static void from_json(const json &jo, CrossSigningKey& result)
  {
  
    if (jo.contains("user_id"s)) {
      result.userId = jo.at("user_id"s);
    }
    if (jo.contains("usage"s)) {
      result.usage = jo.at("usage"s);
    }
    if (jo.contains("keys"s)) {
      result.keys = jo.at("keys"s);
    }
    if (jo.contains("signatures"s)) {
      result.signatures = jo.at("signatures"s);
    }
  
  }
};
    }

    namespace Kazv::Api
    {
} // namespace Kazv::Api
