/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "basejob.hpp"


namespace Kazv::Api {

/*! \brief Knock on a room, requesting permission to join.
 *
 * *Note that this API takes either a room ID or alias, unlike other membership APIs.*
 * 
 * This API "knocks" on the room to ask for permission to join, if the user
 * is allowed to knock on the room. Acceptance of the knock happens out of
 * band from this API, meaning that the client will have to watch for updates
 * regarding the acceptance/rejection of the knock.
 * 
 * If the room history settings allow, the user will still be able to see
 * history of the room while being in the "knock" state. The user will have
 * to accept the invitation to join the room (acceptance of knock) to see
 * messages reliably. See the `/join` endpoints for more information about
 * history visibility to the user.
 * 
 * The knock will appear as an entry in the response of the
 * [`/sync`](/client-server-api/#get_matrixclientv3sync) API.
 */
class KnockRoomJob : public BaseJob {
public:



class JobResponse : public Response
{

public:
  JobResponse(Response r);
  bool success() const;

    // Result properties
        
        

    
/// The knocked room ID.
std::string roomId() const;

};
          static constexpr auto needsAuth() {
          return true
            ;
              }


// Construction/destruction

  /*! \brief Knock on a room, requesting permission to join.
 *
    * \param roomIdOrAlias
    *   The room identifier or alias to knock upon.
    * 
    * \param serverName
    *   The servers to attempt to knock on the room through. One of the servers
    *   must be participating in the room.
    * 
    * \param reason
    *   Optional reason to be included as the `reason` on the subsequent
    *   membership event.
    */
    explicit KnockRoomJob(std::string serverUrl
    , std::string _accessToken
      ,
        std::string roomIdOrAlias , immer::array<std::string> serverName  = {}, std::optional<std::string> reason  = std::nullopt
        );
    

    static BaseJob::Query buildQuery(
    immer::array<std::string> serverName);

      static BaseJob::Body buildBody(std::string roomIdOrAlias, immer::array<std::string> serverName, std::optional<std::string> reason);

        

        

      KnockRoomJob withData(JsonWrap j) &&;
      KnockRoomJob withData(JsonWrap j) const &;
      };
      using KnockRoomResponse = KnockRoomJob::JobResponse;
      } 
      namespace nlohmann
      {
      using namespace Kazv;
      using namespace Kazv::Api;
    
    }

    namespace Kazv::Api
    {

} // namespace Kazv::Api
