/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "sso_login_redirect.hpp"

namespace Kazv::Api
{

  
BaseJob::Query RedirectToSSOJob::buildQuery(
std::string redirectUrl)
{
BaseJob::Query _q;
    addToQuery(_q, "redirectUrl"s, redirectUrl);
  
return _q;
}

    BaseJob::Body RedirectToSSOJob::buildBody(std::string redirectUrl)
      {
      // ignore unused param
      (void)(redirectUrl);
      
      
              return BaseJob::EmptyBody{};

      };

      

RedirectToSSOJob::RedirectToSSOJob(
        std::string serverUrl
        
            ,
        std::string redirectUrl
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v3") + "/login/sso/redirect",
          GET,
          std::string("RedirectToSSO"),
           {} ,
          ReturnType::Json,
            buildBody(redirectUrl)
              , buildQuery(redirectUrl)
                , {}

)
        {
        }

        RedirectToSSOJob RedirectToSSOJob::withData(JsonWrap j) &&
        {
          auto ret = RedirectToSSOJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        RedirectToSSOJob RedirectToSSOJob::withData(JsonWrap j) const &
        {
          auto ret = RedirectToSSOJob(*this);
          ret.attachData(j);
          return ret;
        }

        


  
BaseJob::Query RedirectToIdPJob::buildQuery(
std::string redirectUrl)
{
BaseJob::Query _q;
    addToQuery(_q, "redirectUrl"s, redirectUrl);
  
return _q;
}

    BaseJob::Body RedirectToIdPJob::buildBody(std::string idpId, std::string redirectUrl)
      {
      // ignore unused param
      (void)(idpId);(void)(redirectUrl);
      
      
              return BaseJob::EmptyBody{};

      };

      

RedirectToIdPJob::RedirectToIdPJob(
        std::string serverUrl
        
            ,
        std::string idpId, std::string redirectUrl
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v3") + "/login/sso/redirect/" + idpId,
          GET,
          std::string("RedirectToIdP"),
           {} ,
          ReturnType::Json,
            buildBody(idpId, redirectUrl)
              , buildQuery(redirectUrl)
                , {}

)
        {
        }

        RedirectToIdPJob RedirectToIdPJob::withData(JsonWrap j) &&
        {
          auto ret = RedirectToIdPJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        RedirectToIdPJob RedirectToIdPJob::withData(JsonWrap j) const &
        {
          auto ret = RedirectToIdPJob(*this);
          ret.attachData(j);
          return ret;
        }

        

}
