/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "basejob.hpp"
#include "csapi/definitions/auth_data.hpp"
#include "csapi/definitions/cross_signing_key.hpp"

namespace Kazv::Api {

/*! \brief Upload cross-signing keys.
 *
 * Publishes cross-signing keys for the user.
 * 
 * This API endpoint uses the [User-Interactive Authentication API](/client-server-api/#user-interactive-authentication-api).
 */
class UploadCrossSigningKeysJob : public BaseJob {
public:



class JobResponse : public Response
{

public:
  JobResponse(Response r);
  bool success() const;

};
          static constexpr auto needsAuth() {
          return true
            ;
              }


// Construction/destruction

  /*! \brief Upload cross-signing keys.
 *
    * \param masterKey
    *   Optional. The user\'s master key.
    * 
    * \param selfSigningKey
    *   Optional. The user\'s self-signing key. Must be signed by
    *   the accompanying master key, or by the user\'s most recently
    *   uploaded master key if no master key is included in the
    *   request.
    * 
    * \param userSigningKey
    *   Optional. The user\'s user-signing key. Must be signed by
    *   the accompanying master key, or by the user\'s most recently
    *   uploaded master key if no master key is included in the
    *   request.
    * 
    * \param auth
    *   Additional authentication information for the
    *   user-interactive authentication API.
    */
    explicit UploadCrossSigningKeysJob(std::string serverUrl
    , std::string _accessToken
      ,
        std::optional<CrossSigningKey> masterKey  = std::nullopt, std::optional<CrossSigningKey> selfSigningKey  = std::nullopt, std::optional<CrossSigningKey> userSigningKey  = std::nullopt, std::optional<AuthenticationData> auth  = std::nullopt
        );
    

    static BaseJob::Query buildQuery(
    );

      static BaseJob::Body buildBody(std::optional<CrossSigningKey> masterKey, std::optional<CrossSigningKey> selfSigningKey, std::optional<CrossSigningKey> userSigningKey, std::optional<AuthenticationData> auth);

        

        

      UploadCrossSigningKeysJob withData(JsonWrap j) &&;
      UploadCrossSigningKeysJob withData(JsonWrap j) const &;
      };
      using UploadCrossSigningKeysResponse = UploadCrossSigningKeysJob::JobResponse;
      } 
      namespace nlohmann
      {
      using namespace Kazv;
      using namespace Kazv::Api;
    
    }

    namespace Kazv::Api
    {

/*! \brief Upload cross-signing signatures.
 *
 * Publishes cross-signing signatures for the user.  The request body is a
 * map from user ID to key ID to signed JSON object.
 */
class UploadCrossSigningSignaturesJob : public BaseJob {
public:



class JobResponse : public Response
{

public:
  JobResponse(Response r);
  bool success() const;

    // Result properties
        
        

    
/// A map from user ID to key ID to an error for any signatures
/// that failed.  If a signature was invalid, the `errcode` will
/// be set to `M_INVALID_SIGNATURE`.
immer::map<std::string, immer::map<std::string, JsonWrap>> failures() const;

};
          static constexpr auto needsAuth() {
          return true
            ;
              }


// Construction/destruction

  /*! \brief Upload cross-signing signatures.
 *
    * \param signatures
    *   The signatures to be published.
    */
    explicit UploadCrossSigningSignaturesJob(std::string serverUrl
    , std::string _accessToken
      ,
        immer::map<std::string, immer::map<std::string, JsonWrap>> signatures 
        );
    

    static BaseJob::Query buildQuery(
    );

      static BaseJob::Body buildBody(immer::map<std::string, immer::map<std::string, JsonWrap>> signatures);

        

        

      UploadCrossSigningSignaturesJob withData(JsonWrap j) &&;
      UploadCrossSigningSignaturesJob withData(JsonWrap j) const &;
      };
      using UploadCrossSigningSignaturesResponse = UploadCrossSigningSignaturesJob::JobResponse;
      } 
      namespace nlohmann
      {
      using namespace Kazv;
      using namespace Kazv::Api;
    
    }

    namespace Kazv::Api
    {

} // namespace Kazv::Api
