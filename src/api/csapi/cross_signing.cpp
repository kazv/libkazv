/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "cross_signing.hpp"

namespace Kazv::Api
{

  
BaseJob::Query UploadCrossSigningKeysJob::buildQuery(
)
{
BaseJob::Query _q;

return _q;
}

    BaseJob::Body UploadCrossSigningKeysJob::buildBody(std::optional<CrossSigningKey> masterKey, std::optional<CrossSigningKey> selfSigningKey, std::optional<CrossSigningKey> userSigningKey, std::optional<AuthenticationData> auth)
      {
      // ignore unused param
      (void)(masterKey);(void)(selfSigningKey);(void)(userSigningKey);(void)(auth);
      
        json _data
        
         = json::object();
        
          
            addToJsonIfNeeded(_data, "master_key"s, masterKey);
          
            addToJsonIfNeeded(_data, "self_signing_key"s, selfSigningKey);
          
            addToJsonIfNeeded(_data, "user_signing_key"s, userSigningKey);
          
            addToJsonIfNeeded(_data, "auth"s, auth);
        return BaseJob::JsonBody(_data);
        

      };

      

UploadCrossSigningKeysJob::UploadCrossSigningKeysJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        std::optional<CrossSigningKey> masterKey, std::optional<CrossSigningKey> selfSigningKey, std::optional<CrossSigningKey> userSigningKey, std::optional<AuthenticationData> auth
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v3") + "/keys/device_signing/upload",
          POST,
          std::string("UploadCrossSigningKeys"),
          _accessToken,
          ReturnType::Json,
            buildBody(masterKey, selfSigningKey, userSigningKey, auth)
              , buildQuery()
                , {}

)
        {
        }

        UploadCrossSigningKeysJob UploadCrossSigningKeysJob::withData(JsonWrap j) &&
        {
          auto ret = UploadCrossSigningKeysJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        UploadCrossSigningKeysJob UploadCrossSigningKeysJob::withData(JsonWrap j) const &
        {
          auto ret = UploadCrossSigningKeysJob(*this);
          ret.attachData(j);
          return ret;
        }

        UploadCrossSigningKeysJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool UploadCrossSigningKeysResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
          ;
          }



  
BaseJob::Query UploadCrossSigningSignaturesJob::buildQuery(
)
{
BaseJob::Query _q;

return _q;
}

    BaseJob::Body UploadCrossSigningSignaturesJob::buildBody(immer::map<std::string, immer::map<std::string, JsonWrap>> signatures)
      {
      // ignore unused param
      (void)(signatures);
        return 
          JsonBody(signatures);
      
          

      };

      

UploadCrossSigningSignaturesJob::UploadCrossSigningSignaturesJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        immer::map<std::string, immer::map<std::string, JsonWrap>> signatures
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v3") + "/keys/signatures/upload",
          POST,
          std::string("UploadCrossSigningSignatures"),
          _accessToken,
          ReturnType::Json,
            buildBody(signatures)
              , buildQuery()
                , {}

)
        {
        }

        UploadCrossSigningSignaturesJob UploadCrossSigningSignaturesJob::withData(JsonWrap j) &&
        {
          auto ret = UploadCrossSigningSignaturesJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        UploadCrossSigningSignaturesJob UploadCrossSigningSignaturesJob::withData(JsonWrap j) const &
        {
          auto ret = UploadCrossSigningSignaturesJob(*this);
          ret.attachData(j);
          return ret;
        }

        UploadCrossSigningSignaturesJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool UploadCrossSigningSignaturesResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
          ;
          }


    
    immer::map<std::string, immer::map<std::string, JsonWrap>> UploadCrossSigningSignaturesResponse::failures() const
    {
    if (jsonBody().get()
    .contains("failures"s)) {
    return
    jsonBody().get()["failures"s]
    .template get<immer::map<std::string, immer::map<std::string, JsonWrap>>>();}
    else { return immer::map<std::string, immer::map<std::string, JsonWrap>>(  );}
    }

}
