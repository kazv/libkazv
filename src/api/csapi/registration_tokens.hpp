/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "basejob.hpp"


namespace Kazv::Api {

/*! \brief Query if a given registration token is still valid.
 *
 * Queries the server to determine if a given registration token is still
 * valid at the time of request. This is a point-in-time check where the
 * token might still expire by the time it is used.
 * 
 * Servers should be sure to rate limit this endpoint to avoid brute force
 * attacks.
 */
class RegistrationTokenValidityJob : public BaseJob {
public:



class JobResponse : public Response
{

public:
  JobResponse(Response r);
  bool success() const;

    // Result properties
        
        

    
/// True if the token is still valid, false otherwise. This should
/// additionally be false if the token is not a recognised token by
/// the server.
bool valid() const;

};
          static constexpr auto needsAuth() {
          return 
            false;
              }


// Construction/destruction

  /*! \brief Query if a given registration token is still valid.
 *
    * \param token
    *   The token to check validity of.
    */
    explicit RegistrationTokenValidityJob(std::string serverUrl
    
      ,
        std::string token 
        );


    static BaseJob::Query buildQuery(
    std::string token);

      static BaseJob::Body buildBody(std::string token);

        

        

      RegistrationTokenValidityJob withData(JsonWrap j) &&;
      RegistrationTokenValidityJob withData(JsonWrap j) const &;
      };
      using RegistrationTokenValidityResponse = RegistrationTokenValidityJob::JobResponse;
      } 
      namespace nlohmann
      {
      using namespace Kazv;
      using namespace Kazv::Api;
    
    }

    namespace Kazv::Api
    {

} // namespace Kazv::Api
