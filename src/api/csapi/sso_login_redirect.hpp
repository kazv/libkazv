/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#pragma once

#include "basejob.hpp"


namespace Kazv::Api {

/*! \brief Redirect the user's browser to the SSO interface.
 *
 * A web-based Matrix client should instruct the user's browser to
 * navigate to this endpoint in order to log in via SSO.
 * 
 * The server MUST respond with an HTTP redirect to the SSO interface,
 * or present a page which lets the user select an IdP to continue
 * with in the event multiple are supported by the server.
 */
class RedirectToSSOJob : public BaseJob {
public:



          static constexpr auto needsAuth() {
          return 
            false;
              }


// Construction/destruction

  /*! \brief Redirect the user's browser to the SSO interface.
 *
    * \param redirectUrl
    *   URI to which the user will be redirected after the homeserver has
    *   authenticated the user with SSO.
    */
    explicit RedirectToSSOJob(std::string serverUrl
    
      ,
        std::string redirectUrl 
        );


    static BaseJob::Query buildQuery(
    std::string redirectUrl);

      static BaseJob::Body buildBody(std::string redirectUrl);

        

        

      RedirectToSSOJob withData(JsonWrap j) &&;
      RedirectToSSOJob withData(JsonWrap j) const &;
      };
      
      } 
      namespace nlohmann
      {
      using namespace Kazv;
      using namespace Kazv::Api;
    
    }

    namespace Kazv::Api
    {

/*! \brief Redirect the user's browser to the SSO interface for an IdP.
 *
 * This endpoint is the same as `/login/sso/redirect`, though with an
 * IdP ID from the original `identity_providers` array to inform the
 * server of which IdP the client/user would like to continue with.
 * 
 * The server MUST respond with an HTTP redirect to the SSO interface
 * for that IdP.
 */
class RedirectToIdPJob : public BaseJob {
public:



          static constexpr auto needsAuth() {
          return 
            false;
              }


// Construction/destruction

  /*! \brief Redirect the user's browser to the SSO interface for an IdP.
 *
    * \param idpId
    *   The `id` of the IdP from the `m.login.sso` `identity_providers`
    *   array denoting the user's selection.
    * 
    * \param redirectUrl
    *   URI to which the user will be redirected after the homeserver has
    *   authenticated the user with SSO.
    */
    explicit RedirectToIdPJob(std::string serverUrl
    
      ,
        std::string idpId , std::string redirectUrl 
        );


    static BaseJob::Query buildQuery(
    std::string redirectUrl);

      static BaseJob::Body buildBody(std::string idpId, std::string redirectUrl);

        

        

      RedirectToIdPJob withData(JsonWrap j) &&;
      RedirectToIdPJob withData(JsonWrap j) const &;
      };
      
      } 
      namespace nlohmann
      {
      using namespace Kazv;
      using namespace Kazv::Api;
    
    }

    namespace Kazv::Api
    {

} // namespace Kazv::Api
