/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "knocking.hpp"

namespace Kazv::Api
{

  
BaseJob::Query KnockRoomJob::buildQuery(
immer::array<std::string> serverName)
{
BaseJob::Query _q;
  
    addToQueryIfNeeded(_q, "server_name"s, serverName);
return _q;
}

    BaseJob::Body KnockRoomJob::buildBody(std::string roomIdOrAlias, immer::array<std::string> serverName, std::optional<std::string> reason)
      {
      // ignore unused param
      (void)(roomIdOrAlias);(void)(serverName);(void)(reason);
      
        json _data
        
         = json::object();
        
          
            addToJsonIfNeeded(_data, "reason"s, reason);
        return BaseJob::JsonBody(_data);
        

      };

      

KnockRoomJob::KnockRoomJob(
        std::string serverUrl
        , std::string _accessToken
            ,
        std::string roomIdOrAlias, immer::array<std::string> serverName, std::optional<std::string> reason
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v3") + "/knock/" + roomIdOrAlias,
          POST,
          std::string("KnockRoom"),
          _accessToken,
          ReturnType::Json,
            buildBody(roomIdOrAlias, serverName, reason)
              , buildQuery(serverName)
                , {}

)
        {
        }

        KnockRoomJob KnockRoomJob::withData(JsonWrap j) &&
        {
          auto ret = KnockRoomJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        KnockRoomJob KnockRoomJob::withData(JsonWrap j) const &
        {
          auto ret = KnockRoomJob(*this);
          ret.attachData(j);
          return ret;
        }

        KnockRoomJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool KnockRoomResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
            && jsonBody().get().contains("room_id"s)
          ;
          }


    
    std::string KnockRoomResponse::roomId() const
    {
    if (jsonBody().get()
    .contains("room_id"s)) {
    return
    jsonBody().get()["room_id"s]
    .template get<std::string>();}
    else { return std::string(  );}
    }

}
