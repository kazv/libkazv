/******************************************************************************
 * THIS FILE IS GENERATED - ANY EDITS WILL BE OVERWRITTEN
 */

#include <algorithm>

#include "registration_tokens.hpp"

namespace Kazv::Api
{

  
BaseJob::Query RegistrationTokenValidityJob::buildQuery(
std::string token)
{
BaseJob::Query _q;
    addToQuery(_q, "token"s, token);
  
return _q;
}

    BaseJob::Body RegistrationTokenValidityJob::buildBody(std::string token)
      {
      // ignore unused param
      (void)(token);
      
      
              return BaseJob::EmptyBody{};

      };

      

RegistrationTokenValidityJob::RegistrationTokenValidityJob(
        std::string serverUrl
        
            ,
        std::string token
        )
      : BaseJob(std::move(serverUrl),
          std::string("/_matrix/client/v1") + "/register/m.login.registration_token/validity",
          GET,
          std::string("RegistrationTokenValidity"),
           {} ,
          ReturnType::Json,
            buildBody(token)
              , buildQuery(token)
                , {}

)
        {
        }

        RegistrationTokenValidityJob RegistrationTokenValidityJob::withData(JsonWrap j) &&
        {
          auto ret = RegistrationTokenValidityJob(std::move(*this));
          ret.attachData(j);
          return ret;
        }

        RegistrationTokenValidityJob RegistrationTokenValidityJob::withData(JsonWrap j) const &
        {
          auto ret = RegistrationTokenValidityJob(*this);
          ret.attachData(j);
          return ret;
        }

        RegistrationTokenValidityJob::JobResponse::JobResponse(Response r)
        : Response(std::move(r)) {}

          bool RegistrationTokenValidityResponse::success() const
          {
            return Response::success()
            
              && isBodyJson(body)
            && jsonBody().get().contains("valid"s)
          ;
          }


    
    bool RegistrationTokenValidityResponse::valid() const
    {
    if (jsonBody().get()
    .contains("valid"s)) {
    return
    jsonBody().get()["valid"s]
    .template get<bool>();}
    else { return bool(  );}
    }

}
