/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa  <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_all.hpp>

#include <event.hpp>

using namespace Kazv;


TEST_CASE("Should distinguish between state events and other events", "[base][event]")
{
    Event state = R"({
    "state_key": "",
    "sender": "@example:example.org",
    "type": "moe.kazv.mxc.custom.state.type",
    "event_id": "!dummy",
    "origin_server_ts": 1234,
    "content": {}
})"_json;

    REQUIRE(state.isState());

    Event nonState = R"({
    "sender": "@example:example.org",
    "type": "moe.kazv.mxc.custom.non.state.type",
    "event_id": "!dummy",
    "origin_server_ts": 1234,
    "content": {}
})"_json;

    REQUIRE(!nonState.isState());
}

TEST_CASE("redacted()", "[base][event]")
{
    auto eventJson = R"({
    "state_key": "",
    "sender": "@example:example.org",
    "type": "moe.kazv.mxc.custom.state.type",
    "event_id": "!dummy",
    "origin_server_ts": 1234,
    "content": {}
})"_json;

    REQUIRE(!Event(eventJson).redacted());

    eventJson["unsigned"] = json::object({{"redacted_because", json::object()}});
    REQUIRE(Event(eventJson).redacted());
}

TEST_CASE("relationship()", "[base][event]")
{
    auto eventJson = json{
        {"sender", "@example:example.org"},
        {"type", "moe.kazv.mxc.custom.state.type"},
        {"event_id", "$dummy"},
        {"origin_server_ts", 1234},
        {"content", {
            {"m.relates_to", {
                {"rel_type", "moe.kazv.mxc.some-type"},
                {"event_id", "$another"},
            },
        }}},
    };

    WHEN("m.relates_to is a normal relation") {
        REQUIRE(Event(eventJson).relationship() == std::pair<std::string, std::string>{"moe.kazv.mxc.some-type", "$another"});
        REQUIRE(Event(eventJson).replyingTo() == "");
        REQUIRE(Event(eventJson).mRelatesTo().get() == eventJson["content"]["m.relates_to"]);
    }

    WHEN("m.relates_to is a reply") {
        eventJson["content"]["m.relates_to"] = {
            {"m.in_reply_to", {
                {"event_id", "$another"}
            }},
        };
        REQUIRE(Event(eventJson).relationship() == std::pair<std::string, std::string>{"m.in_reply_to", "$another"});
        REQUIRE(Event(eventJson).replyingTo() == "$another");
        REQUIRE(Event(eventJson).mRelatesTo().get() == eventJson["content"]["m.relates_to"]);
    }

    WHEN("m.relates_to is missing rel_type") {
        eventJson["content"]["m.relates_to"].erase("rel_type");
        REQUIRE(Event(eventJson).relationship() == std::pair<std::string, std::string>{"", ""});
        REQUIRE(Event(eventJson).mRelatesTo().get() == eventJson["content"]["m.relates_to"]);
    }

    WHEN("m.relates_to is missing event_id") {
        eventJson["content"]["m.relates_to"].erase("event_id");
        REQUIRE(Event(eventJson).relationship() == std::pair<std::string, std::string>{"", ""});
        REQUIRE(Event(eventJson).mRelatesTo().get() == eventJson["content"]["m.relates_to"]);
    }

    WHEN("m.relates_to is not an object") {
        eventJson["content"]["m.relates_to"] = json::array();
        REQUIRE(Event(eventJson).relationship() == std::pair<std::string, std::string>{"", ""});
        REQUIRE(Event(eventJson).mRelatesTo().get() == json::object());
    }

    WHEN("m.relates_to does not exist") {
        eventJson["content"].erase("m.relates_to");
        REQUIRE(Event(eventJson).relationship() == std::pair<std::string, std::string>{"", ""});
        REQUIRE(Event(eventJson).mRelatesTo().get() == json::object());
    }
}

TEST_CASE("relationship(), with encrypted event", "[base][event]")
{
    auto eventJsonWithRel = json{
        {"sender", "@example:example.org"},
        {"type", "moe.kazv.mxc.custom.state.type"},
        {"event_id", "$dummy"},
        {"origin_server_ts", 1234},
        {"content", {
            {"m.relates_to", {
                {"rel_type", "moe.kazv.mxc.some-type"},
                {"event_id", "$another"},
            },
        }}},
    };

    auto eventJson = eventJsonWithRel;
    eventJson["content"].erase("m.relates_to");
    auto plainJsonWithRel = eventJsonWithRel;
    plainJsonWithRel["type"] = "m.room.encrypted";
    plainJsonWithRel["content"]["m.relates_to"] = {
        {"rel_type", "moe.kazv.mxc.some-other-type"},
        {"event_id", "$another-2"},
    };

    auto plainJson = plainJsonWithRel;
    plainJson["content"].erase("m.relates_to");

    WHEN("plaintext with relationship / ciphertext with relationship") {
        auto event = Event(plainJsonWithRel).setDecryptedJson(eventJsonWithRel, Event::Decrypted);
        THEN("should use the relationship within plaintext") {
            REQUIRE(event.relationship() == std::pair<std::string, std::string>{"moe.kazv.mxc.some-other-type", "$another-2"});
            REQUIRE(event.mRelatesTo().get() == plainJsonWithRel["content"]["m.relates_to"]);
        }
    }

    WHEN("plaintext without relationship / ciphertext with relationship") {
        auto event = Event(plainJson).setDecryptedJson(eventJsonWithRel, Event::Decrypted);
        THEN("should use the relationship within ciphertext") {
            REQUIRE(event.relationship() == std::pair<std::string, std::string>{"moe.kazv.mxc.some-type", "$another"});
            REQUIRE(event.mRelatesTo().get() == eventJsonWithRel["content"]["m.relates_to"]);
        }
    }

    WHEN("plaintext with relationship / ciphertext without relationship") {
        auto event = Event(plainJsonWithRel).setDecryptedJson(eventJson, Event::Decrypted);
        THEN("should use the relationship within plaintext") {
            REQUIRE(event.relationship() == std::pair<std::string, std::string>{"moe.kazv.mxc.some-other-type", "$another-2"});
            REQUIRE(event.mRelatesTo().get() == plainJsonWithRel["content"]["m.relates_to"]);
        }
    }

    WHEN("plaintext without relationship / ciphertext without relationship") {
        auto event = Event(plainJson).setDecryptedJson(eventJson, Event::Decrypted);
        THEN("should return empty pair") {
            REQUIRE(event.relationship() == std::pair<std::string, std::string>{"", ""});
            REQUIRE(event.mRelatesTo().get() == json::object());
        }
    }
}
