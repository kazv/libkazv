/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>

#include <json-utils.hpp>

using namespace Kazv;

TEST_CASE("isNonCompoundCanonicalJsonValue()", "[base][json-utils]")
{
    REQUIRE(isNonCompoundCanonicalJsonValue(nlohmann::json::parse("\"string\"")));
    REQUIRE(isNonCompoundCanonicalJsonValue(nlohmann::json::parse("1")));
    REQUIRE(isNonCompoundCanonicalJsonValue(nlohmann::json::parse("9007199254740991")));
    REQUIRE(isNonCompoundCanonicalJsonValue(nlohmann::json::parse("-9007199254740991")));
    REQUIRE(isNonCompoundCanonicalJsonValue(nlohmann::json::parse("true")));
    REQUIRE(isNonCompoundCanonicalJsonValue(nlohmann::json::parse("false")));
    REQUIRE(isNonCompoundCanonicalJsonValue(nlohmann::json::parse("null")));

    REQUIRE(!isNonCompoundCanonicalJsonValue(nlohmann::json::parse("[]")));
    REQUIRE(!isNonCompoundCanonicalJsonValue(nlohmann::json::parse("{}")));
    REQUIRE(!isNonCompoundCanonicalJsonValue(nlohmann::json::parse("1.2")));
    REQUIRE(!isNonCompoundCanonicalJsonValue(nlohmann::json::parse("9007199254740992")));
    REQUIRE(!isNonCompoundCanonicalJsonValue(nlohmann::json::parse("-9007199254740992")));
}

TEST_CASE("getInJson()", "[base][json-utils]")
{
    auto j = nlohmann::json::parse(R"({
  "1": "2",
  "a": {
    "b": 123,
    "c": null,
    "d": [1, 2, 3]
  }
})");

    REQUIRE(getInJson(j, std::vector<std::string>{"b"}) == std::nullopt);
    REQUIRE(getInJson(j, std::vector<std::string>{"1"}).value() == "2");
    REQUIRE(getInJson(j, std::vector<std::string>{"a"}).value() == j["a"]);
    REQUIRE(getInJson(j, std::vector<std::string>{"a", "b"}).value() == j["a"]["b"]);
    REQUIRE(getInJson(j, std::vector<std::string>{"a", "b", "d", "0"}) == std::nullopt);
}
