/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_predicate.hpp>
#include <catch2/matchers/catch_matchers_contains.hpp>

#include <string>

#include <immer/map.hpp>
#include <immer/flex_vector.hpp>

#include <immer-utils.hpp>

using namespace Kazv;

using ComplexMapT = immer::map<std::string, immer::map<std::string, immer::flex_vector<std::string>>>;

TEST_CASE("getIn()", "[base][immer-utils]")
{
    ComplexMapT complexMap{
        {"a", {{"a/b", {"a/b/0", "a/b/1"}}}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    };

    REQUIRE(getIn(complexMap) == complexMap);
    REQUIRE(getIn(complexMap, "a") == complexMap["a"]);
    REQUIRE(getIn(complexMap, "a", "a/b") == complexMap["a"]["a/b"]);
    REQUIRE(getIn(complexMap, "a", "a/b", 0) == complexMap["a"]["a/b"][0]);
}

TEST_CASE("setIn()", "[base][immer-utils]")
{
    ComplexMapT complexMap{
        {"a", {{"a/b", {"a/b/0", "a/b/1"}}}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    };

    REQUIRE(setIn(complexMap, {}) == ComplexMapT{});
    REQUIRE(setIn(complexMap, {}, "a") == ComplexMapT{
        {"a", {}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    });
    REQUIRE(setIn(complexMap, {}, "a", "a/b") == ComplexMapT{
        {"a", {{"a/b", {}}}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    });
    REQUIRE(setIn(complexMap, "test", "a", "a/b", 0) == ComplexMapT{
        {"a", {{"a/b", {"test", "a/b/1"}}}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    });
}

TEST_CASE("updateIn()", "[base][immer-utils]")
{
    ComplexMapT complexMap{
        {"a", {{"a/b", {"a/b/0", "a/b/1"}}}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    };

    REQUIRE(updateIn(complexMap, [](auto) { return ComplexMapT{}; }) == ComplexMapT{});
    REQUIRE(updateIn(complexMap, [](auto a) { return a.set("a/b2", {"a/b2/0", "a/b2/1"}); }, "a") == ComplexMapT{
        {"a", {{"a/b", {"a/b/0", "a/b/1"}}, {"a/b2", {"a/b2/0", "a/b2/1"}}}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    });
    REQUIRE(updateIn(complexMap, [](auto a) { return a.set(0, "new"); }, "a", "a/b") == ComplexMapT{
        {"a", {{"a/b", {"new", "a/b/1"}}}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    });
    REQUIRE(updateIn(complexMap, [](auto a) { return a + "test"; }, "a", "a/b", 0) == ComplexMapT{
        {"a", {{"a/b", {"a/b/0test", "a/b/1"}}}},
        {"c", {{"c/d", {"c/d/0", "c/d/1"}}}},
    });
}
