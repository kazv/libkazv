/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>
#include <client-model.hpp>
#include <factory.hpp>

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("DeviceListTracker::findByCurve25519Key")
{
    auto info = makeDeviceKeyInfo(withDeviceId("device1"));
    auto client = makeClient(withDevice("@example:example.com", info));

    REQUIRE(client.deviceLists.findByCurve25519Key("@example:example.com", info.curve25519Key) == info);
    REQUIRE(client.deviceLists.findByCurve25519Key("@someone:example.com", info.curve25519Key) == std::nullopt);
}
