/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_test_macros.hpp>

#include <types.hpp>
#include <factory.hpp>

#include <power-levels-desc.hpp>

using namespace Kazv;
using namespace Kazv::Factory;

static auto examplePowerLevelsJson = R"({
  "ban": 10,
  "events": {
    "m.room.message": 20,
    "moe.kazv.mxc.some-type": 50
  },
  "events_default": 1,
  "invite": 1,
  "kick": 1,
  "notifications": {"room": 99},
  "redact": 49,
  "state_default": 100,
  "users": {
    "@mew:example.com": 100
  },
  "users_default": 1
})"_json;

static auto examplePowerLevels = makeEvent(
    withEventType("m.room.power_levels")
    | withStateKey("")
    | withEventContent(examplePowerLevelsJson)
);

TEST_CASE("Parse Power Levels", "[client][power-levels]")
{
    auto powerLevels = PowerLevelsDesc(examplePowerLevels);
    REQUIRE(powerLevels.normalizedEvent().content().get()
        == examplePowerLevels.content().get());

    SECTION("additional keys are discarded")
    {
        auto jsonWithAdditionalKeys = examplePowerLevelsJson;
        jsonWithAdditionalKeys["moe.kazv.mxc.something-else"];
        auto eventWithAdditionalKeys = makeEvent(
            withEventType("m.room.power_levels")
            | withStateKey("")
            | withEventContent(jsonWithAdditionalKeys)
        );
        auto powerLevels = PowerLevelsDesc(eventWithAdditionalKeys);
        REQUIRE(powerLevels.normalizedEvent().content().get()
            == examplePowerLevels.content().get());
    }

    SECTION("additional keys in notifications are discarded")
    {
        auto jsonWithAdditionalKeys = examplePowerLevelsJson;
        jsonWithAdditionalKeys["notifications"]["moe.kazv.mxc.something-else"];
        auto eventWithAdditionalKeys = makeEvent(
            withEventType("m.room.power_levels")
            | withStateKey("")
            | withEventContent(jsonWithAdditionalKeys)
        );
        auto powerLevels = PowerLevelsDesc(eventWithAdditionalKeys);
        REQUIRE(powerLevels.normalizedEvent().content().get()
            == examplePowerLevels.content().get());
    }

    SECTION("string values are converted to numbers")
    {
        auto jsonWithStringValues = examplePowerLevelsJson;
        jsonWithStringValues["ban"] = "10";
        auto eventWithStringValues = makeEvent(
            withEventType("m.room.power_levels")
            | withStateKey("")
            | withEventContent(jsonWithStringValues)
        );
        auto powerLevels = PowerLevelsDesc(eventWithStringValues);
        REQUIRE(powerLevels.normalizedEvent().content().get()
            == examplePowerLevels.content().get());
    }

    SECTION("string values that cannot be converted are replaced by default values")
    {
        auto jsonWithStringValues = examplePowerLevelsJson;
        jsonWithStringValues["ban"] = "10s";
        auto expected = examplePowerLevelsJson;
        expected["ban"] = 50;
        auto eventWithStringValues = makeEvent(
            withEventType("m.room.power_levels")
            | withStateKey("")
            | withEventContent(jsonWithStringValues)
        );
        auto powerLevels = PowerLevelsDesc(eventWithStringValues);
        REQUIRE(powerLevels.normalizedEvent().content().get()
            == expected);
    }

    SECTION("string values in events that cannot be converted are replaced by default values")
    {
        auto jsonWithStringValues = examplePowerLevelsJson;
        jsonWithStringValues["events"]["ttt"] = "10s";
        auto expected = examplePowerLevelsJson;
        auto eventWithStringValues = makeEvent(
            withEventType("m.room.power_levels")
            | withStateKey("")
            | withEventContent(jsonWithStringValues)
        );
        auto powerLevels = PowerLevelsDesc(eventWithStringValues);
        REQUIRE(powerLevels.normalizedEvent().content().get()
            == expected);
    }

    SECTION("string values in users that cannot be converted are replaced by default values")
    {
        auto jsonWithStringValues = examplePowerLevelsJson;
        jsonWithStringValues["users"]["@ttt:example.com"] = "10s";
        auto expected = examplePowerLevelsJson;
        auto eventWithStringValues = makeEvent(
            withEventType("m.room.power_levels")
            | withStateKey("")
            | withEventContent(jsonWithStringValues)
        );
        auto powerLevels = PowerLevelsDesc(eventWithStringValues);
        REQUIRE(powerLevels.normalizedEvent().content().get()
            == expected);
    }
}

TEST_CASE("powerLevelOfUser()", "[client][power-levels]")
{
    auto powerLevels = PowerLevelsDesc(examplePowerLevels);
    REQUIRE(powerLevels.powerLevelOfUser("@mew:example.com") == 100);
    REQUIRE(powerLevels.powerLevelOfUser("@mewmew:example.com") == 1);

    SECTION("default power levels object")
    {
        powerLevels = PowerLevelsDesc(json::object({}));
        REQUIRE(powerLevels.powerLevelOfUser("@mew:example.com") == 0);
        REQUIRE(powerLevels.powerLevelOfUser("@mewmew:example.com") == 0);
    }
}

TEST_CASE("canSendMessage()", "[client][power-levels]")
{
    auto powerLevels = PowerLevelsDesc(examplePowerLevels);
    REQUIRE(powerLevels.canSendMessage("@mew:example.com", "m.room.message"));
    REQUIRE(!powerLevels.canSendMessage("@mewmew:example.com", "m.room.message"));
    REQUIRE(powerLevels.canSendMessage("@mew:example.com", "m.room.encrypted"));
    REQUIRE(powerLevels.canSendMessage("@mewmew:example.com", "m.room.encrypted"));
}

TEST_CASE("canSendState()", "[client][power-levels]")
{
    auto powerLevels = PowerLevelsDesc(examplePowerLevels);
    REQUIRE(powerLevels.canSendState("@mew:example.com", "moe.kazv.mxc.some-type"));
    REQUIRE(!powerLevels.canSendState("@mewmew:example.com", "moe.kazv.mxc.some-type"));
    REQUIRE(powerLevels.canSendState("@mew:example.com", "moe.kazv.mxc.some-other-type"));
    REQUIRE(!powerLevels.canSendState("@mewmew:example.com", "moe.kazv.mxc.some-other-type"));
}

TEST_CASE("canInvite()", "[client][power-levels]")
{
    auto powerLevels = PowerLevelsDesc(examplePowerLevels);
    REQUIRE(powerLevels.canInvite("@mew:example.com"));
    REQUIRE(powerLevels.canInvite("@mewmew:example.com"));
}

TEST_CASE("canKick()", "[client][power-levels]")
{
    auto powerLevels = PowerLevelsDesc(examplePowerLevels);
    REQUIRE(powerLevels.canKick("@mew:example.com"));
    REQUIRE(powerLevels.canKick("@mewmew:example.com"));
}

TEST_CASE("canBan()", "[client][power-levels]")
{
    auto powerLevels = PowerLevelsDesc(examplePowerLevels);
    REQUIRE(powerLevels.canBan("@mew:example.com"));
    REQUIRE(!powerLevels.canBan("@mewmew:example.com"));
}

TEST_CASE("canRedact()", "[client][power-levels]")
{
    auto powerLevels = PowerLevelsDesc(examplePowerLevels);
    REQUIRE(powerLevels.canRedact("@mew:example.com"));
    REQUIRE(!powerLevels.canRedact("@mewmew:example.com"));
}

TEST_CASE("setBan()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setBan(100);
    REQUIRE(next.originalEvent().content().get() == json{{"ban", 100}});
    next = next.setBan(std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json::object());
}

TEST_CASE("setKick()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setKick(100);
    REQUIRE(next.originalEvent().content().get() == json{{"kick", 100}});
    next = next.setKick(std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json::object());
}

TEST_CASE("setInvite()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setInvite(100);
    REQUIRE(next.originalEvent().content().get() == json{{"invite", 100}});
    next = next.setInvite(std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json::object());
}

TEST_CASE("setRedact()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setRedact(100);
    REQUIRE(next.originalEvent().content().get() == json{{"redact", 100}});
    next = next.setRedact(std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json::object());
}

TEST_CASE("setEventsDefault()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setEventsDefault(100);
    REQUIRE(next.originalEvent().content().get() == json{{"events_default", 100}});
    next = next.setEventsDefault(std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json::object());
}

TEST_CASE("setStateDefault()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setStateDefault(100);
    REQUIRE(next.originalEvent().content().get() == json{{"state_default", 100}});
    next = next.setStateDefault(std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json::object());
}

TEST_CASE("setUsersDefault()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setUsersDefault(100);
    REQUIRE(next.originalEvent().content().get() == json{{"users_default", 100}});
    next = next.setUsersDefault(std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json::object());
}

TEST_CASE("setEvent()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setEvent("moe.kazv.mxc.some-type", 100);
    REQUIRE(next.originalEvent().content().get() == json{{"events", {{"moe.kazv.mxc.some-type", 100}}}});
    next = next.setEvent("moe.kazv.mxc.some-type", std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json{{"events", json::object()}});
}

TEST_CASE("setUser()", "[client][power-levels]")
{
    const auto powerLevels = PowerLevelsDesc(Event());
    auto next = powerLevels.setUser("@foo:example.com", 100);
    REQUIRE(next.originalEvent().content().get() == json{{"users", {{"@foo:example.com", 100}}}});
    next = next.setUser("@foo:example.com", std::nullopt);
    REQUIRE(next.originalEvent().content().get() == json{{"users", json::object()}});
}
