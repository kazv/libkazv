/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

#include <client-model.hpp>

#include "factory.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

static ClientModel data(int userCount, int devicePerUser)
{
    auto client = makeClient();

    for (int u = 0; u < userCount; ++u) {
        for (int d = 0; d < devicePerUser; ++d) {
            withDevice(
                "@user" + std::to_string(u) + ":example.com",
                makeDeviceKeyInfo(withDeviceId("device" + std::to_string(d)))
            )(client);
        }
    }

    return client;
}

static void modify(ClientModel &c, int u, int d)
{
    // keep the newly-generated key, but set the device id to what we want.
    auto info = makeDeviceKeyInfo();
    withDeviceId("device" + std::to_string(d))(info);

    withDevice(
        "@user" + std::to_string(u) + ":example.com",
        info
    )(c);
}

static void modifyMany(ClientModel &c, immer::flex_vector<std::pair<int, int>> modifications)
{
    for (auto [u, d] : modifications) {
        modify(c, u, d);
    }
}

TEST_CASE("DeviceListTracker::diff() benchmark: small", "[device-list-tracker][!benchmark]")
{
    auto client = data(100, 5);

    auto anotherClient = client;
    modifyMany(
        anotherClient,
        {{4, 1}, {20, 3}, {60, 6}, {99, 0}, {102, 5}}
    );

    BENCHMARK("diff against itself") {
        return client.deviceLists.diff(client.deviceLists);
    };

    BENCHMARK("diff against some modifications") {
        return client.deviceLists.diff(anotherClient.deviceLists);
    };
}

TEST_CASE("DeviceListTracker::diff() benchmark: medium", "[device-list-tracker][!benchmark]")
{
    auto client = data(1000, 10);

    auto anotherClient = client;
    modifyMany(
        anotherClient,
        {{4, 1}, {201, 9}, {608, 6}, {995, 0}, {1012, 11}}
    );

    BENCHMARK("diff against itself") {
        return client.deviceLists.diff(client.deviceLists);
    };

    BENCHMARK("diff against some modifications") {
        return client.deviceLists.diff(anotherClient.deviceLists);
    };
}

TEST_CASE("DeviceListTracker::diff() benchmark: large", "[device-list-tracker][!benchmark]")
{
    auto client = data(10000, 20);
    auto anotherClient = client;
    modifyMany(
        anotherClient,
        {{4, 1}, {202, 3}, {6078, 6}, {9956, 0}, {10021, 7},
         {7954, 13}, {4013, 9}, {1234, 34}, {2879, 8}}
    );

    BENCHMARK("diff against itself") {
        return client.deviceLists.diff(client.deviceLists);
    };

    BENCHMARK("diff against some modifications") {
        return client.deviceLists.diff(anotherClient.deviceLists);
    };
}
