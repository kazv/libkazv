/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_test_macros.hpp>

#include <boost/asio.hpp>
#include <asio-promise-handler.hpp>

#include <sdk.hpp>

#include "client-test-util.hpp"
#include "factory.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

static json mDirectEvent = R"({
  "content": {
    "@foo:example.com": ["!foo:tusooa.xyz"],
    "@bar:example.com": ["!bar:tusooa.xyz", "!bar2:tusooa.xyz"],
    "@somethingelse": "xxx"
  },
  "type": "m.direct"
})"_json;

static json mTag = R"({
  "content": {
    "tags": {
      "u.work": {
        "order": 0.9
      },
      "m.favourite": {
        "order": 0.5
      }
    }
  },
  "type": "m.tag"
})"_json;

static json mTag2 = R"({
  "content": {
    "tags": {
      "m.favourite": {
        "order": 0.6
      },
      "u.something": {}
    }
  },
  "type": "m.tag"
})"_json;

TEST_CASE("ClientModel::directRoomMap", "[client][tagging]")
{
    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    ClientModel m = makeClient({});
    REQUIRE(m.directRoomMap() == immer::map<std::string, std::string>());
    withAccountData({mDirectEvent})(m);

    auto store = createTestClientStoreFrom(m, ph);

    auto expected = immer::map<std::string, std::string>{
        {"!foo:tusooa.xyz", "@foo:example.com"},
        {"!bar:tusooa.xyz", "@bar:example.com"},
        {"!bar2:tusooa.xyz", "@bar:example.com"},
    };

    REQUIRE(m.directRoomMap() == expected);

    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }), store,
        std::nullopt);

    REQUIRE(client.directRoomMap().make().get() == expected);
}

TEST_CASE("RoomModel::tags", "[client][tagging]")
{
    RoomModel r = makeRoom({});
    REQUIRE(r.tags() == immer::map<std::string, double>{});

    withRoomAccountData({mTag})(r);

    immer::map<std::string, double> expected{
        {"u.work", 0.9},
        {"m.favourite", 0.5}
    };
    REQUIRE(r.tags() == expected);
}

TEST_CASE("RoomModel::tags: without order", "[client][tagging]")
{
    RoomModel r = makeRoom(
        withRoomAccountData({mTag2})
    );

    immer::map<std::string, double> expected{
        {"u.something", 2},
        {"m.favourite", 0.6}
    };
    REQUIRE(r.tags() == expected);
}

TEST_CASE("Room::tags", "[client][tagging]")
{
    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    RoomModel r1 = makeRoom(
        withRoomId("!r1:tusooa.xyz")
        | withRoomAccountData({mTag})
    );
    RoomModel r2 = makeRoom(
        withRoomId("!r2:tusooa.xyz")
        | withRoomAccountData({mTag2})
    );
    ClientModel m = makeClient(
        withRoom(r1)
        | withRoom(r2)
    );

    auto store = createTestClientStoreFrom(m, ph);

    auto expectedR1 = immer::map<std::string, double>{
        {"u.work", 0.9},
        {"m.favourite", 0.5},
    };

    auto expectedR2 = immer::map<std::string, double>{
        {"m.favourite", 0.6},
        {"u.something", ROOM_TAG_DEFAULT_ORDER},
    };

    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }), store,
        std::nullopt);

    auto room1 = client.room(r1.roomId);
    REQUIRE(room1.tags().make().get() == expectedR1);

    auto room2 = client.room(r2.roomId);
    REQUIRE(room2.tags().make().get() == expectedR2);
}

TEST_CASE("ClientModel::roomIdsUnderTag", "[client][tagging]")
{
    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    RoomModel r1 = makeRoom(
        withRoomId("!r1:tusooa.xyz")
        | withRoomAccountData({mTag})
    );
    RoomModel r2 = makeRoom(
        withRoomId("!r2:tusooa.xyz")
        | withRoomAccountData({mTag2})
    );
    ClientModel m = makeClient(
        withRoom(r1)
        | withRoom(r2)
    );

    auto store = createTestClientStoreFrom(m, ph);

    auto expectedFav = immer::map<std::string, double>{
        {"!r1:tusooa.xyz", 0.5},
        {"!r2:tusooa.xyz", 0.6},
    };

    auto expectedSomething = immer::map<std::string, double>{
        {"!r2:tusooa.xyz", ROOM_TAG_DEFAULT_ORDER},
    };

    REQUIRE(m.roomIdsUnderTag("m.favourite") == expectedFav);
    REQUIRE(m.roomIdsUnderTag("u.something") == expectedSomething);
    REQUIRE(m.roomIdsUnderTag("non-existent") == immer::map<std::string, double>{});

    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }), store,
        std::nullopt);

    REQUIRE(client.roomIdsUnderTag("m.favourite").make().get() == expectedFav);
    REQUIRE(client.roomIdsUnderTag("u.something").make().get() == expectedSomething);
    REQUIRE(client.roomIdsUnderTag("non-existent").make().get() == immer::map<std::string, double>{});
}

TEST_CASE("ClientModel::roomIdsByTagName", "[client][tagging]")
{
    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    RoomModel r1 = makeRoom(
        withRoomId("!r1:tusooa.xyz")
        | withRoomAccountData({mTag})
    );
    RoomModel r2 = makeRoom(
        withRoomId("!r2:tusooa.xyz")
        | withRoomAccountData({mTag2})
    );
    ClientModel m = makeClient(
        withRoom(r1)
        | withRoom(r2)
        | withRoom(makeRoom(withRoomId("!r3:tusooa.xyz")))
    );

    auto store = createTestClientStoreFrom(m, ph);

    auto expectedFav = immer::map<std::string, double>{
        {"!r1:tusooa.xyz", 0.5},
        {"!r2:tusooa.xyz", 0.6},
    };

    auto expectedSomething = immer::map<std::string, double>{
        {"!r2:tusooa.xyz", ROOM_TAG_DEFAULT_ORDER},
    };

    auto expectedWork = immer::map<std::string, double>{
        {"!r1:tusooa.xyz", 0.9},
    };

    auto expectedNothing = immer::map<std::string, double>{
        {"!r3:tusooa.xyz", ROOM_TAG_DEFAULT_ORDER},
    };

    auto expected = immer::map<std::string, immer::map<std::string, double>>{
        {"m.favourite", expectedFav},
        {"u.something", expectedSomething},
        {"u.work", expectedWork},
        {"", expectedNothing},
    };

    REQUIRE(m.roomIdsByTagId() == expected);

    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }), store,
        std::nullopt);

    REQUIRE(client.roomIdsByTagId().make().get() == expected);
}
