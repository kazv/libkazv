/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>
#include <actions/encryption.hpp>
#include <client-model.hpp>
#include "factory.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

// The assumption here is that undecrypted events take only a small
// part in the room timeline, and most undecrypted events are so because
// we do not have the corresponding session key.
// This should hold when the most of the
// messages in the timeline is received after the session begins
// (until we implement key sharing).
static ClientModel data(
    std::size_t roomCount,
    std::size_t messagesPerRoom,
    std::size_t undecryptableMessagesPerRoom,
    std::size_t decryptableMessagesPerRoom
)
{
    std::cerr << "Generating data" << std::endl;
    auto client = makeClient(withCrypto(makeCrypto()));

    std::size_t generated = 0;
    auto makeNewEvent = [&](const std::string &roomId, std::size_t m) {
        if (m < undecryptableMessagesPerRoom) {
            return makeEvent(withEventType("m.room.encrypted")
                | withEventContent(json{
                    {"algorithm", CryptoConstants::megOlmAlgo},
                    {"ciphertext", "does not matter"},
                    {"device_id", "somedevice"},
                    {"sender_key", "somekey"},
                    // rotating the session once per 100 messages
                    {"session_id", "some-session-id" + std::to_string(m / 100)},
                })
                | withEventKV("/room_id"_json_pointer, roomId));
        } else if (m < undecryptableMessagesPerRoom + decryptableMessagesPerRoom) {
            auto event = makeEvent();
            auto [encrypted, sessionId] = client.megOlmEncrypt(
                event, roomId, 1719196953000,
                genRandomData(EncryptMegOlmEventAction::maxRandomSize()));
            return encrypted;
        } else {
            return makeEvent();
        }
    };

    for (std::size_t r = 0; r < roomCount; ++r) {
        auto room = makeRoom(withRoomEncrypted(true));
        withRoom(room)(client);
        auto events = EventList{};
        for (std::size_t m = 0; m < messagesPerRoom; ++m) {
            auto event = makeNewEvent(room.roomId, m);
            events = std::move(events).push_back(event);
            ++generated;
            if (generated % 10000 == 0) {
                std::cerr << "Generated " << generated << " events" << std::endl;
            }
        }
        withRoomTimeline(events)(room);
        withRoom(room)(client);
    }
    return client;
}

// correspond to the case where the session is pretty new
TEST_CASE("tryDecryptEvents() benchmark: small", "[client][encryption][!benchmark]")
{
    auto client = data(10, 200, 100, 5);
    BENCHMARK("tryDecryptEvents()") {
        return tryDecryptEvents(client);
    };
}

TEST_CASE("tryDecryptEvents() benchmark: medium", "[client][encryption][!benchmark]")
{
    auto client = data(50, 1000, 200, 5);
    BENCHMARK("tryDecryptEvents()") {
        return tryDecryptEvents(client);
    };
}

// A somewhat unrealistic case
TEST_CASE("tryDecryptEvents() benchmark: large", "[client][encryption][!benchmark]")
{
    auto client = data(100, 4000, 400, 5);
    BENCHMARK("tryDecryptEvents()") {
        return tryDecryptEvents(client);
    };
}
