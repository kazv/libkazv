/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>

#include <notification-handler.hpp>
#include <asio-promise-handler.hpp>
#include <factory.hpp>

#include "push-rules-test-util.hpp"
#include "client-test-util.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("NotificationHandler::handleNotification()", "[client][push-rules]")
{
    auto pushRulesEvent = makeEvent(
        withEventType("m.push_rules")
        | withEventContent(json{
            {"global", {
                {"override", {oneToOneRule}},
            }},
        })
    );
    auto room = makeRoom();
    room.joinedMemberCount = 2;
    auto clientModel = makeClient(withRoom(room) | withAccountData({pushRulesEvent}));
    auto io = boost::asio::io_context();
    auto ph = AsioPromiseHandler(io.get_executor());
    auto store = createTestClientStoreFrom(clientModel, ph);

    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }), store,
        std::nullopt);

    auto nh = client.notificationHandler();

    auto e = makeEvent(withEventKV("/room_id"_json_pointer, room.roomId));
    auto res = nh.handleNotification(e);

    REQUIRE(res.shouldNotify);
}
