/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_test_macros.hpp>

#include <validator.hpp>

using namespace Kazv;

static auto exampleJson = json{
    {"a", "b"},
    {"c", json::object({})},
    {"d", json::array({1, 2, 3})},
    {"e", {
        {"f", "g"},
        {"h", 1},
    }},
};

TEST_CASE("cast()", "[client][validator]")
{
    auto result = json::object();

    SECTION("when it passes the validator")
    {
        auto r = cast(result, exampleJson, "a",
            [](const auto &) { return std::make_pair(true, "mew"); }
        );
        REQUIRE(r);
        REQUIRE(result.at("a") == "mew");
    }

    SECTION("when it does not pass the validator")
    {
        auto r = cast(result, exampleJson, "a",
            [](const auto &j) { return std::make_pair(false, j); }
        );
        REQUIRE(!r);
        REQUIRE(!result.contains("a"));
    }

    SECTION("when the thing does not exist in input")
    {
        auto r = cast(result, exampleJson, "mewmewmew",
            [](const auto &j) { return std::make_pair(true, j); }
        );
        REQUIRE(!r);
        REQUIRE(!result.contains("mewmewmew"));
    }
}

TEST_CASE("identValidate()", "[client][validator]")
{
    auto validator = identValidate([](const auto &j) {
        return j.is_string();
    });

    REQUIRE(validator(exampleJson.at("a")) == std::make_pair(true, exampleJson.at("a")));
    REQUIRE(std::get<0>(validator(exampleJson.at("c"))) == false);
}

TEST_CASE("castArray()", "[client][validator]")
{
    auto result = json::object();

    SECTION("when all inputs pass the validator")
    {
        auto r = castArray(result, exampleJson, "d",
            identValidate(&json::is_number), CastArrayStrategy::FailAll);
        REQUIRE(r);
        REQUIRE(result.at("d") == exampleJson.at("d"));

        r = castArray(result, exampleJson, "d",
            identValidate(&json::is_number), CastArrayStrategy::IgnoreInvalid);
        REQUIRE(r);
        REQUIRE(result.at("d") == exampleJson.at("d"));
    }

    SECTION("when some input does not pass the validator")
    {
        auto v = [](const json &j) -> std::pair<bool, json> {
            return {
                j.is_number() && j.template get<int>() % 2 == 0,
                j.template get<int>() * 2,
            };
        };

        auto r = castArray(result, exampleJson, "d",
            v, CastArrayStrategy::FailAll);
        REQUIRE(!r);
        REQUIRE(!result.contains("d"));

        r = castArray(result, exampleJson, "d",
            v, CastArrayStrategy::IgnoreInvalid);
        REQUIRE(r);
        REQUIRE(result.at("d") == json::array({4}));
    }

    SECTION("when no input passes the validator")
    {
        auto pred = [](const json &) {
            return false;
        };

        auto r = castArray(result, exampleJson, "d",
            identValidate(pred), CastArrayStrategy::FailAll);
        REQUIRE(!r);
        REQUIRE(!result.contains("d"));

        r = castArray(result, exampleJson, "d",
            identValidate(pred), CastArrayStrategy::IgnoreInvalid);
        REQUIRE(r);
        REQUIRE(result.at("d") == json::array());
    }

    SECTION("when input is not an array")
    {
        auto pred = [](const json &) {
            return true;
        };

        auto r = castArray(result, exampleJson, "c",
            identValidate(pred), CastArrayStrategy::FailAll);
        REQUIRE(!r);
        REQUIRE(!result.contains("c"));

        r = castArray(result, exampleJson, "c",
            identValidate(pred), CastArrayStrategy::IgnoreInvalid);
        REQUIRE(!r);
        REQUIRE(!result.contains("c"));
    }

    SECTION("when input does not exist")
    {
        auto pred = [](const json &) {
            return true;
        };

        auto r = castArray(result, exampleJson, "mewmewmew",
            identValidate(pred), CastArrayStrategy::FailAll);
        REQUIRE(!r);
        REQUIRE(!result.contains("mewmewmew"));

        r = castArray(result, exampleJson, "mewmewmew",
            identValidate(pred), CastArrayStrategy::IgnoreInvalid);
        REQUIRE(!r);
        REQUIRE(!result.contains("mewmewmew"));
    }
}

TEST_CASE("castObject()", "[client][validator]")
{
    auto result = json::object();

    SECTION("when all inputs pass the validator")
    {
        auto validator = identValidate([](const json &) {
            return true;
        });
        auto r = castObject(result, exampleJson, "e",
            validator, CastObjectStrategy::FailAll);
        REQUIRE(r);
        REQUIRE(result.at("e") == exampleJson.at("e"));

        r = castObject(result, exampleJson, "e",
            validator, CastObjectStrategy::IgnoreInvalid);
        REQUIRE(r);
        REQUIRE(result.at("e") == exampleJson.at("e"));
    }

    SECTION("when some input does not pass the validator")
    {
        auto v = [](const json &kv) -> std::pair<bool, json> {
            if (kv[1].is_number()) {
                return {
                    true,
                    json::array({kv[0], kv[1].template get<int>() * 2}),
                };
            } else {
                return { false, json() };
            }
        };

        auto r = castObject(result, exampleJson, "e",
            v, CastObjectStrategy::FailAll);
        REQUIRE(!r);
        REQUIRE(!result.contains("e"));

        r = castObject(result, exampleJson, "e",
            v, CastObjectStrategy::IgnoreInvalid);
        REQUIRE(r);
        REQUIRE(result.at("e") == json::object({{"h", 2}}));
    }

    SECTION("when no input passes the validator")
    {
        auto pred = [](const json &) {
            return false;
        };

        auto r = castObject(result, exampleJson, "e",
            identValidate(pred), CastObjectStrategy::FailAll);
        REQUIRE(!r);
        REQUIRE(!result.contains("e"));

        r = castObject(result, exampleJson, "e",
            identValidate(pred), CastObjectStrategy::IgnoreInvalid);
        REQUIRE(r);
        REQUIRE(result.at("e") == json::object());
    }

    SECTION("when input is not an object")
    {
        auto pred = [](const json &) {
            return true;
        };

        auto r = castObject(result, exampleJson, "d",
            identValidate(pred), CastObjectStrategy::FailAll);
        REQUIRE(!r);
        REQUIRE(!result.contains("d"));

        r = castObject(result, exampleJson, "d",
            identValidate(pred), CastObjectStrategy::IgnoreInvalid);
        REQUIRE(!r);
        REQUIRE(!result.contains("d"));
    }

    SECTION("when input does not exist")
    {
        auto pred = [](const json &) {
            return true;
        };

        auto r = castObject(result, exampleJson, "mewmewmew",
            identValidate(pred), CastObjectStrategy::FailAll);
        REQUIRE(!r);
        REQUIRE(!result.contains("mewmewmew"));

        r = castObject(result, exampleJson, "mewmewmew",
            identValidate(pred), CastObjectStrategy::IgnoreInvalid);
        REQUIRE(!r);
        REQUIRE(!result.contains("mewmewmew"));
    }
}

TEST_CASE("makeDefaultValue()", "[client][validator]")
{
    SECTION("if input contains k")
    {
        REQUIRE(makeDefaultValue(exampleJson, "c", "foo") == exampleJson);
    }

    SECTION("if input does not contain k")
    {
        auto expected = exampleJson;
        expected["vvv"] = "foo";
        REQUIRE(makeDefaultValue(exampleJson, "vvv", "foo") == expected);
    }

    SECTION("it works with a const value")
    {
        const auto constJson = exampleJson;
        REQUIRE(makeDefaultValue(constJson, "c", "foo") == constJson);
    }
}
