/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <lager/event_loop/boost_asio.hpp>

#include <catch2/catch_all.hpp>
#include <boost/asio.hpp>
#include <asio-promise-handler.hpp>
#include <cursorutil.hpp>
#include <sdk.hpp>
#include <cprjobhandler.hpp>
#include <lagerstoreeventemitter.hpp>
#include "action-mock-utils.hpp"
#include "client-test-util.hpp"
#include "factory.hpp"

using namespace Kazv::Factory;

TEST_CASE("Redact a message", "[client][redact]")
{
    ClientModel loggedInModel = makeClient({});

    auto [resModel, dontCareEffect] = ClientModel::update(
        loggedInModel, RedactEventAction{"!foo:tusooa.xyz", "$event-id", "some reason"});

    assert1Job(resModel);
    for1stJob(resModel, [] (const auto &job) {
        REQUIRE(job.jobId() == "RedactEvent");
    });
}

TEST_CASE("Redact a message without a reason", "[client][redact]")
{
    ClientModel loggedInModel = makeClient({});

    auto [resModel, dontCareEffect] = ClientModel::update(
        loggedInModel, RedactEventAction{"!foo:tusooa.xyz", "$event-id", std::nullopt});

    assert1Job(resModel);
    for1stJob(resModel, [] (const auto &job) {
        REQUIRE(job.jobId() == "RedactEvent");
        REQUIRE(json::parse(std::get<BytesBody>(job.requestBody())) == json::object());
    });
}

TEST_CASE("Room::redact()", "[client][redact]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m = makeClient(
        withRoom(
            makeRoom(withRoomId("!exampleroomid:example.com"))
        )
    );

    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();

    auto dispatcher = getMockDispatcher(
        sgph,
        ctx,
        returnEmpty<RedactEventAction>()
    );

    auto mockContext = getMockContext(sgph, dispatcher);

    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());
    auto r = client.room("!exampleroomid:example.com");

    r.redactEvent("$some-event", "some reason")
        .then([&io](auto st) {
            REQUIRE(st.success());
            io.stop();
        });

    io.run();

    REQUIRE(dispatcher.template calledTimes<RedactEventAction>() == 1);

    auto redactAction = dispatcher.template of<RedactEventAction>()[0];

    REQUIRE(redactAction.roomId == "!exampleroomid:example.com");
    REQUIRE(redactAction.eventId == "$some-event");
    REQUIRE(redactAction.reason == std::optional<std::string>("some reason"));
}
