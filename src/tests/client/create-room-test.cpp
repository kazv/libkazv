/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <lager/event_loop/boost_asio.hpp>

#include <catch2/catch_test_macros.hpp>
#include <boost/asio.hpp>
#include <asio-promise-handler.hpp>
#include <cprjobhandler.hpp>
#include <lagerstoreeventemitter.hpp>
#include <cursorutil.hpp>
#include <sdk-model.hpp>
#include <client/client.hpp>
#include <client/actions/membership.hpp>
#include "client-test-util.hpp"
#include "action-mock-utils.hpp"
#include "factory.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("Client::createRoom()", "[client][create-room]")
{
    ClientModel m = makeClient();

    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> ph{AsioPromiseHandler{io.get_executor()}};
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();

    auto dispatcher = getMockDispatcher(
        ph,
        ctx,
        returnEmpty<CreateRoomAction>()
    );

    auto mockContext = getMockContext(ph, dispatcher);

    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto initialState = immer::array<Event>{
        makeEvent(withEventType("moe.kazv.mxc.some-type") | withStateKey(""))
    };

    client.createRoom(
        /* visibility = */ RoomVisibility::Private,
        /* name = */ "some name",
        /* alias = */ "alias",
        /* invite = */ immer::array<std::string>{"@invited:example.com"},
        /* isDirect = */ true,
        /* allowFederate = */ true,
        /* topic = */ "some topic",
        /* powerLevelContentOverride = */ json::object({{"messages", 1}}),
        /* preset = */ CreateRoomPreset::TrustedPrivateChat,
        /* initialState = */ initialState
    ).then([&io](auto) {
        io.stop();
    });

    io.run();

    REQUIRE(dispatcher.template calledTimes<CreateRoomAction>() == 1);
    auto action = dispatcher.template of<CreateRoomAction>()[0];
    REQUIRE(action.visibility == RoomVisibility::Private);
    REQUIRE(action.name == "some name");
    REQUIRE(action.roomAliasName == "alias");
    REQUIRE(action.invite == immer::array<std::string>{"@invited:example.com"});
    REQUIRE(action.isDirect == true);
    REQUIRE(action.creationContent.get() == json::object({
        {"m.federate", true},
    }));
    REQUIRE(action.topic == "some topic");
    REQUIRE(action.preset == CreateRoomPreset::TrustedPrivateChat);
    REQUIRE(action.initialState == initialState);
}

TEST_CASE("CreateRoomAction", "[client][create-room]")
{
    ClientModel m = makeClient();
    auto action = CreateRoomAction{};
    action.visibility = RoomVisibility::Private;
    action.name = "some name";
    action.roomAliasName = "alias";
    action.invite = immer::array<std::string>{"@invited:example.com"};
    action.isDirect = true;
    action.powerLevelContentOverride = json{{"messages", 1}};
    action.creationContent = json::object({
        {"m.federate", true},
    });
    action.topic = "some topic";
    action.preset = CreateRoomPreset::TrustedPrivateChat;
    action.initialState = immer::array<Event>{
        json{
            {"type", "moe.kazv.mxc.some-type"},
            {"state_key", "@mew:example.com"},
            {"content", {
                {"foo", "bar"},
            }},
        },
    };

    SECTION("base") {
        auto [next, _] = updateClient(m, action);
        assert1Job(next);
        for1stJob(next, [action](const BaseJob &job) {
            auto body = json::parse(std::get<BytesBody>(job.requestBody()));
            REQUIRE(body["creation_content"] == json{
                {"m.federate", true},
            });
            REQUIRE(body["initial_state"] == json::array({action.initialState[0].originalJson().get()}));
            REQUIRE(body["invite"] == json{"@invited:example.com"});
            REQUIRE(!body.contains("invite_3pid"));
            REQUIRE(body["is_direct"] == true);
            REQUIRE(body["name"] == "some name");
            REQUIRE(body["power_level_content_override"] == json{
                {"messages", 1},
            });
            REQUIRE(body["preset"] == "trusted_private_chat");
            REQUIRE(body["room_alias_name"] == "alias");
            REQUIRE(!body.contains("room_version"));
            REQUIRE(body["topic"] == "some topic");
            REQUIRE(body["visibility"] == "private");
        });
    }

    WHEN("visibility: public") {
        action.visibility = RoomVisibility::Public;
        auto [next, _] = updateClient(m, action);
        for1stJob(next, [action](const BaseJob &job) {
            auto body = json::parse(std::get<BytesBody>(job.requestBody()));
            REQUIRE(body["visibility"] == "public");
        });
    }

    WHEN("preset: public_chat") {
        action.preset = CreateRoomPreset::PublicChat;
        auto [next, _] = updateClient(m, action);
        for1stJob(next, [action](const BaseJob &job) {
            auto body = json::parse(std::get<BytesBody>(job.requestBody()));
            REQUIRE(body["preset"] == "public_chat");
        });
    }

    WHEN("preset: private_chat") {
        action.preset = CreateRoomPreset::PrivateChat;
        auto [next, _] = updateClient(m, action);
        for1stJob(next, [action](const BaseJob &job) {
            auto body = json::parse(std::get<BytesBody>(job.requestBody()));
            REQUIRE(body["preset"] == "private_chat");
        });
    }
}

TEST_CASE("Process create room response", "[client][create-room]")
{
    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    auto store = createTestClientStore(ph);

    WHEN("Success response")
    {
        auto succResponse = makeResponse("CreateRoom", withResponseJsonBody(json{{"room_id", "!some-room:example.com"}}));
        store.dispatch(ProcessResponseAction{succResponse})
            .then([&](auto stat) {
                REQUIRE(stat.success());
            });
    }
    WHEN("Failed response")
    {
        auto failResponse = makeResponse("CreateRoom", withResponseStatusCode(400) | withResponseJsonBody(json{
            {"error", "An unknown error occurred"},
            {"errcode", "M_UNKNOWN"},
        }));
        store.dispatch(ProcessResponseAction{failResponse})
            .then([](auto stat) {
                REQUIRE(!stat.success());
                REQUIRE(stat.dataStr("error") == "An unknown error occurred");
                REQUIRE(stat.dataStr("errorCode") == "400");
            });
    }
    io.run();
}
