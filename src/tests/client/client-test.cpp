/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_test_macros.hpp>

#include <client/client-model.hpp>
#include <client/actions/encryption.hpp>

#include "factory.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("ClientModel copying should not invoke Crypto copy-constructor", "[client][copy]")
{
    ClientModel m = makeClient(withCrypto(makeCrypto()));

    auto m2 = m;

    REQUIRE(m.crypto.value().get() == m2.crypto.value().get());
}

TEST_CASE("Modifying Crypto in Client when it is not unique should invoke Crypto's copy constructor", "[client][copy]")
{
    auto roomId = "!someroom:example.com";
    auto room = makeRoom(
        withRoomEncrypted(true)
        | withRoomId(roomId)
    );
    auto client = makeClient(
        withCrypto(makeCrypto())
        | withRoom(room)
    );

    auto plainText = makeEvent();
    auto [encrypted, sessionId] = client.megOlmEncrypt(plainText, roomId, 1719196953000,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize()));

    withRoomTimeline({
        encrypted
    })(room);
    withRoom(room)(client);

    auto m2 = client;

    auto m3 = tryDecryptEvents(std::move(client));
    REQUIRE(!(m3.crypto.value().get() == m2.crypto.value().get()));
}
