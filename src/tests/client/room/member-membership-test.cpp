/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_test_macros.hpp>

#include <lager/event_loop/boost_asio.hpp>

#include <asio-promise-handler.hpp>
#include <cprjobhandler.hpp>
#include <lagerstoreeventemitter.hpp>
#include <sdk.hpp>
#include <client/client.hpp>
#include <room/room-model.hpp>

#include "factory.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

static immer::flex_vector<Event> makeEvents(immer::flex_vector<std::string> userIds, const std::string &membership)
{
    auto events = immer::flex_vector<Event>{};
    for (const auto &userId : userIds) {
        events = std::move(events).push_back(makeMemberEvent(
            withStateKey(userId)
            | withMembership(membership)
        ));
    }
    return events;
}

const static auto joinedIds = immer::flex_vector<std::string>{
    std::string{"@mew_joined:example.com"}
};
const static auto invitedIds = immer::flex_vector<std::string>{
    std::string{"@mew_invited:example.com"}
};
const static auto knockedIds = immer::flex_vector<std::string>{
    std::string{"@mew_knocked:example.com"}
};
const static auto leftIds = immer::flex_vector<std::string>{
    std::string{"@mew_left:example.com"}
};
const static auto bannedIds = immer::flex_vector<std::string>{
    std::string{"@mew_banned:example.com"}
};

const static auto joinedEvents = makeEvents(joinedIds, "join");
const static auto invitedEvents = makeEvents(invitedIds, "invite");
const static auto knockedEvents =makeEvents(knockedIds, "knock");
const static auto leftEvents = makeEvents(leftIds, "leave");
const static auto bannedEvents = makeEvents(bannedIds, "ban");

const static auto testRoomId = std::string{"testRoom"};

static RoomModel makeTestRoomModel()
{
    return makeRoom(
        withRoomState(
            joinedEvents
            + invitedEvents
            + knockedEvents
            + leftEvents
            + bannedEvents)
        | withRoomId(testRoomId)
    );
}

TEST_CASE("Get members by membership", "[client][room]")
{
    const auto r = makeTestRoomModel();

    REQUIRE(r.joinedMemberIds() == joinedIds);
    REQUIRE(r.invitedMemberIds() == invitedIds);
    REQUIRE(r.knockedMemberIds() == knockedIds);
    REQUIRE(r.leftMemberIds() == leftIds);
    REQUIRE(r.bannedMemberIds() == bannedIds);

    REQUIRE(r.joinedMemberEvents() == joinedEvents);
    REQUIRE(r.invitedMemberEvents() == invitedEvents);
    REQUIRE(r.knockedMemberEvents() == knockedEvents);
    REQUIRE(r.leftMemberEvents() == leftEvents);
    REQUIRE(r.bannedMemberEvents() == bannedEvents);
}

TEST_CASE("Get members by membership with Kazv::Room", "[client][room]")
{
    boost::asio::io_context io;
    auto clientModel = makeClient(withRoom(makeTestRoomModel()));
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});
    auto sdk = makeSdk(
        SdkModel{clientModel},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );
    auto room = sdk.client().room(testRoomId);

    REQUIRE(room.members().get() == joinedIds);
    REQUIRE(room.invitedMembers().get() == invitedIds);
    REQUIRE(room.knockedMembers().get() == knockedIds);
    REQUIRE(room.leftMembers().get() == leftIds);
    REQUIRE(room.bannedMembers().get() == bannedIds);

    REQUIRE(room.joinedMemberEvents().get() == joinedEvents);
    REQUIRE(room.invitedMemberEvents().get() == invitedEvents);
    REQUIRE(room.knockedMemberEvents().get() == knockedEvents);
    REQUIRE(room.leftMemberEvents().get() == leftEvents);
    REQUIRE(room.bannedMemberEvents().get() == bannedEvents);
}
