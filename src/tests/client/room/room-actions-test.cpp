/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_all.hpp>

#include <room/room-model.hpp>
#include <testfixtures/factory.hpp>

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("SetHeroIdsAction", "[client][room]")
{
    RoomModel r;
    r.heroIds = immer::flex_vector<std::string>{"@u:test.org"};

    auto newHeroes = immer::flex_vector<std::string>{"@a:test.org", "@b:test.org"};

    WHEN("We update the model") {
        auto res = RoomModel::update(r, SetHeroIdsAction{newHeroes});

        THEN("it should update hero ids") {
            REQUIRE(res.heroIds == newHeroes);
        }

        THEN("it should compare different than the original") {
            REQUIRE(!(res == r));
        }
    }
}

TEST_CASE("AddToTimelineAction: redactions do not affect stateEvents", "[client][room]")
{
    RoomModel r;
    r.roomId = "!some-room-id:tusooa.xyz"s;
    auto eventId = "$some-event-to-be-redacted"s;
    auto userId = "@some:tusooa.xyz"s;
    auto event = Event{json{
        {"event_id", eventId},
        {"room_id", r.roomId},
        {"sender", userId},
        {"type", "m.room.member"},
        {"state_key", userId},
        {"content", {
            {"displayname", "some"},
            {"membership", "join"},
        }},
    }};
    r.messages = r.messages.set(eventId, event);
    r.stateEvents = r.stateEvents.set({"m.room.member", userId}, event);
    r.timeline = immer::flex_vector<std::string>{eventId};

    auto redactEvent = Event{json{
        {"event_id", "$some-other-event-id"},
        {"room_id", r.roomId},
        {"sender", userId},
        {"type", "m.room.redaction"},
        {"redacts", eventId},
        {"content", {
            {"reason", "lol"},
        }},
    }};

    auto res = RoomModel::update(r, AddToTimelineAction{
        {redactEvent},
        /* prevBatch = */ std::nullopt,
        /* limited = */ false,
        /* gapEventId = */ std::nullopt
    });

    REQUIRE(res.timeline.size() == 2);
    REQUIRE(res.messages[eventId].content().get() == json::object());
    REQUIRE(res.messages[eventId].redacted());
    REQUIRE(res.stateEvents[{"m.room.member", userId}] == event);
}

TEST_CASE("AddToTimelineAction: should handle redactions", "[client][room]")
{
    RoomModel r;
    r.roomId = "!some-room-id:tusooa.xyz"s;
    auto eventId = "$some-event-to-be-redacted"s;
    auto userId = "@some:tusooa.xyz"s;
    auto event = Event{json{
        {"event_id", eventId},
        {"room_id", r.roomId},
        {"sender", userId},
        {"type", "m.room.message"},
        {"content", {
            {"msgtype", "m.text"},
            {"body", "mew"},
        }},
    }};
    r.messages = r.messages.set(eventId, event);
    r.timeline = immer::flex_vector<std::string>{eventId};

    auto redactEvent = Event{json{
        {"event_id", "$some-other-event-id"},
        {"room_id", r.roomId},
        {"sender", userId},
        {"type", "m.room.redaction"},
        {"redacts", eventId},
        {"content", {
            {"reason", "lol"},
        }},
    }};

    auto res = RoomModel::update(r, AddToTimelineAction{
        {redactEvent},
        /* prevBatch = */ std::nullopt,
        /* limited = */ false,
        /* gapEventId = */ std::nullopt
    });

    REQUIRE(res.timeline.size() == 2);
    REQUIRE(res.messages[eventId].content().get() == json::object());
    REQUIRE(res.messages[eventId].redacted());
}

TEST_CASE("MaybeAddStateEventsAction", "[client][room]")
{
    auto r = makeRoom(withRoomState({
        makeEvent(withEventType("m.room.member") | withStateKey("@foo:example.com") | withEventContent(json{{"membership", "join"}})),
    }));

    auto next = RoomModel::update(r, MaybeAddStateEventsAction{{
        // already exists, should not add
        makeEvent(withEventType("m.room.member") | withStateKey("@foo:example.com") | withEventContent(json{{"membership", "leave"}})),
        // does not exist yet but the one below should be used
        makeEvent(withEventType("m.room.member") | withStateKey("@bar:example.com") | withEventContent(json{{"membership", "leave"}})),
        // does not exist, should add
        makeEvent(withEventType("m.room.member") | withStateKey("@bar:example.com") | withEventContent(json{{"membership", "join"}})),
    }});

    REQUIRE(next.stateEvents.count(KeyOfState{"m.room.member", "@foo:example.com"}));
    REQUIRE(next.stateEvents[KeyOfState{"m.room.member", "@foo:example.com"}].content().get()["membership"] == "join");
    REQUIRE(next.stateEvents.count(KeyOfState{"m.room.member", "@bar:example.com"}));
    REQUIRE(next.stateEvents[KeyOfState{"m.room.member", "@bar:example.com"}].content().get()["membership"] == "join");
}
