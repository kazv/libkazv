/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>
#include <sstream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <asio-promise-handler.hpp>
#include "factory.hpp"
#include "client-test-util.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("RoomModel::undecryptedEvents", "[client][room][encryption]")
{
    auto events = EventList{
        // not encrypted events, not taken into account
        makeEvent(),
        makeEvent(withEventKV("/content/session_id"_json_pointer, "some-session-id")),
        // encrypted event, not decrypted, valid session id
        makeEvent(withEventId("$some-event-0") | withEventType("m.room.encrypted") | withEventKV("/content/session_id"_json_pointer, "some-session-id")),
        makeEvent(withEventId("$some-event-1") | withEventType("m.room.encrypted") | withEventKV("/content/session_id"_json_pointer, "some-other-session-id")),
        makeEvent(withEventId("$some-event-2") | withEventType("m.room.encrypted") | withEventKV("/content/session_id"_json_pointer, "some-session-id")),
        // session id is not a string, not taken into account
        makeEvent(withEventId("$some-event-3") | withEventType("m.room.encrypted") | withEventKV("/content/session_id"_json_pointer, json())),
        // decrypted events are not taken into account
        makeEvent(withEventId("$some-event-4") | withEventType("m.room.encrypted") | withEventKV("/content/session_id"_json_pointer, "some-other-session-id"))
        .setDecryptedJson(json::object({}), Event::Decrypted),
    };

    SECTION("it calculates for an encrypted room")
    {
        auto room = makeRoom(withRoomEncrypted(true) | withRoomTimeline(events));

        auto expected = immer::map<std::string, immer::flex_vector<std::string>>{
            {"some-session-id", {"$some-event-0", "$some-event-2"}},
            {"some-other-session-id", {"$some-event-1"}},
        };

        REQUIRE(room.undecryptedEvents == expected);
    }

    SECTION("it does not calculate for an unencrypted room")
    {
        auto room = makeRoom(withRoomEncrypted(false) | withRoomTimeline(events));
        REQUIRE(room.undecryptedEvents == immer::map<std::string, immer::flex_vector<std::string>>{});
    }
}

TEST_CASE("Serialize RoomModel with undecryptedEvents", "[client][serialization][room]")
{
    using IAr = boost::archive::text_iarchive;
    using OAr = boost::archive::text_oarchive;

    auto events = EventList{
        makeEvent(withEventId("$some-event-0") | withEventType("m.room.encrypted") | withEventKV("/content/session_id"_json_pointer, "some-session-id")),
    };

    auto expected = immer::map<std::string, immer::flex_vector<std::string>>{
        {"some-session-id", {
            "$some-event-0",
        }},
    };

    RoomModel m1 = makeRoom(withRoomEncrypted(true) | withRoomTimeline(events));
    m1.undecryptedEvents = {}; // force clear the undecrypted map
    RoomModel m2;

    WHEN("reading from an archive older than v7") {
        std::stringstream stream;

        {
            auto ar = OAr(stream);
            serialize(ar, m1, 6); // HACK to save as v6, thus bypassing event relationships
        }

        {
            auto ar = IAr(stream);
            serialize(ar, m2, 6);
        }

        THEN("it should regenerate undecryptedEvents") {
            REQUIRE(m2.undecryptedEvents == expected);
        }
    }

    WHEN("reading from an archive later or equal to v7") {
        std::stringstream stream;

        {
            auto ar = OAr(stream);
            ar << m1;
        }

        {
            auto ar = IAr(stream);
            ar >> m2;
        }

        THEN("it should not regenerate undecryptedEvents") {
            REQUIRE(m2.undecryptedEvents == immer::map<std::string, immer::flex_vector<std::string>>{});
        }
    }
}

TEST_CASE("Serialize unencrypted RoomModel with undecryptedEvents", "[client][serialization][room]")
{
    using IAr = boost::archive::text_iarchive;
    using OAr = boost::archive::text_oarchive;

    auto events = EventList{
        makeEvent(withEventId("$some-event-0") | withEventType("m.room.encrypted") | withEventKV("/content/session_id"_json_pointer, "some-session-id")),
    };

    RoomModel m1 = makeRoom(withRoomEncrypted(false) | withRoomTimeline(events));
    m1.undecryptedEvents = {}; // force clear the undecrypted map
    RoomModel m2;

    WHEN("reading from an archive older than v7") {
        std::stringstream stream;

        {
            auto ar = OAr(stream);
            serialize(ar, m1, 6); // HACK to save as v6, thus bypassing event relationships
        }

        {
            auto ar = IAr(stream);
            serialize(ar, m2, 6);
        }

        THEN("it not should regenerate undecryptedEvents") {
            REQUIRE(m2.undecryptedEvents == immer::map<std::string, immer::flex_vector<std::string>>{});
        }
    }
}
