/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_test_macros.hpp>

#include <sstream>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <asio-promise-handler.hpp>
#include "factory.hpp"

#include "client-test-util.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("Event relationships", "[client][room][event-rel]")
{
    auto events = EventList{
        makeEvent(),
        makeEvent(),
        makeEvent(withEventId("$some-event-0") | withEventRelationship("moe.kazv.mxc.some-relationship", "$some-other-event")),
        makeEvent(withEventId("$some-event-1") | withEventRelationship("moe.kazv.mxc.some-relationship", "$some-other-event")),
        makeEvent(withEventId("$some-event-2") | withEventRelationship("moe.kazv.mxc.some-other-relationship", "$some-other-event")),
        makeEvent(withEventId("$some-event-3") | withEventRelationship("moe.kazv.mxc.some-other-relationship", "$some-other-event-1")),
        makeEvent(withEventId("$some-event-4") | withEventReplyTo("$some-other-event-1")),
    };

    auto room = makeRoom(withRoomTimeline(events));

    auto expectedReverseEventRelationships = RoomModel::ReverseEventRelationshipMap{
        {"$some-other-event", {
            {"moe.kazv.mxc.some-relationship", {"$some-event-0", "$some-event-1"}},
            {"moe.kazv.mxc.some-other-relationship", {"$some-event-2"}},
        }},
        {"$some-other-event-1", {
            {"moe.kazv.mxc.some-other-relationship", {"$some-event-3"}},
            {"m.in_reply_to", {"$some-event-4"}},
        }}
    };

    REQUIRE(room.reverseEventRelationships == expectedReverseEventRelationships);

    {
        boost::asio::io_context io;
        AsioPromiseHandler ph{io.get_executor()};

        auto model = makeClient(withRoom(room));
        auto store = createTestClientStoreFrom(model, ph);
        auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }),
            store,
            std::nullopt);
        auto r = client.room(room.roomId);
        auto actualEvents = r.relatedEvents(
            lager::make_constant(std::string("$some-other-event")),
            "moe.kazv.mxc.some-relationship"
        );

        REQUIRE(actualEvents.get() == EventList{events[2], events[3]});
    }

    WHEN("adding more events") {
        auto moreEvents = EventList{
            makeEvent(),
            makeEvent(withEventId("$more-event-0") | withEventRelationship("moe.kazv.mxc.some-relationship", "$some-event-2")),
            makeEvent(withEventId("$more-event-1") | withEventRelationship("moe.kazv.mxc.some-relationship", "$some-other-event")),
        };

        withRoomTimeline(moreEvents)(room);

        auto expectedReverseEventRelationships = RoomModel::ReverseEventRelationshipMap{
            {"$some-other-event", {
                {"moe.kazv.mxc.some-relationship", {"$some-event-0", "$some-event-1", "$more-event-1"}},
                {"moe.kazv.mxc.some-other-relationship", {"$some-event-2"}},
            }},
            {"$some-other-event-1", {
                {"moe.kazv.mxc.some-other-relationship", {"$some-event-3"}},
                {"m.in_reply_to", {"$some-event-4"}},
            }},
            {"$some-event-2", {
                {"moe.kazv.mxc.some-relationship", {"$more-event-0"}},
            }},
        };

        REQUIRE(room.reverseEventRelationships == expectedReverseEventRelationships);
    }
}

TEST_CASE("Serialize RoomModel with relationships", "[client][serialization][room]")
{
    using IAr = boost::archive::text_iarchive;
    using OAr = boost::archive::text_oarchive;

    auto events = EventList{
        makeEvent(withEventId("$some-event-0") | withEventRelationship("moe.kazv.mxc.some-relationship", "$some-other-event")),
    };

    auto expectedRelationships = RoomModel::ReverseEventRelationshipMap{
        {"$some-other-event", {
            {"moe.kazv.mxc.some-relationship", {"$some-event-0"}},
        }},
    };

    RoomModel m1 = makeRoom(withRoomTimeline(events));
    m1.reverseEventRelationships = {}; // force clear the relationship map
    RoomModel m2;

    WHEN("reading from an archive older than v4") {
        std::stringstream stream;

        {
            auto ar = OAr(stream);
            serialize(ar, m1, 3); // HACK to save as v3, thus bypassing event relationships
        }

        {
            auto ar = IAr(stream);
            serialize(ar, m2, 3);
        }

        THEN("it should regenerate the event relationships") {
            REQUIRE(m2.reverseEventRelationships == expectedRelationships);
        }
    }

    WHEN("reading from an archive later or equal to v4") {
        std::stringstream stream;

        {
            auto ar = OAr(stream);
            ar << m1;
        }

        {
            auto ar = IAr(stream);
            ar >> m2;
        }

        THEN("it should not regenerate the event relationships") {
            REQUIRE(m2.reverseEventRelationships == RoomModel::ReverseEventRelationshipMap{});
        }
    }
}
