/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>
#include <lager/event_loop/boost_asio.hpp>
#include <cprjobhandler.hpp>
#include <lagerstoreeventemitter.hpp>
#include <asio-promise-handler.hpp>
#include "client-test-util.hpp"
#include "client/action-mock-utils.hpp"
#include "factory.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("Room::pinnedEvents()", "[client][room][getter]")
{
    WHEN("content is valid") {
        auto room = makeRoom(withRoomState({
            makeEvent(withEventType("m.room.pinned_events") | withEventContent(json{{"pinned", {"$1", "$2"}}})),
        }));
        auto client = makeClient(withRoom(room));
        auto cursor = lager::make_constant(SdkModel{client});
        auto nameCursor = lager::make_constant(room.roomId);
        auto r = Room(cursor, nameCursor, dumbContext());
        REQUIRE(r.pinnedEvents().get() == immer::flex_vector<std::string>{"$1", "$2"});
    }

    WHEN("content is invalid") {
        auto room = makeRoom(withRoomState({
            makeEvent(withEventType("m.room.pinned_events") | withEventContent(json{{"pinned", {"$1", "$2", 3}}})),
        }));
        auto client = makeClient(withRoom(room));
        auto cursor = lager::make_constant(SdkModel{client});
        auto nameCursor = lager::make_constant(room.roomId);
        auto r = Room(cursor, nameCursor, dumbContext());
        REQUIRE(r.pinnedEvents().get() == immer::flex_vector<std::string>{});
    }

    WHEN("there is no m.room.pinned_events in state") {
        auto room = makeRoom();
        auto client = makeClient(withRoom(room));
        auto cursor = lager::make_constant(SdkModel{client});
        auto nameCursor = lager::make_constant(room.roomId);
        auto r = Room(cursor, nameCursor, dumbContext());
        REQUIRE(r.pinnedEvents().get() == immer::flex_vector<std::string>{});
    }
}

TEST_CASE("Room::pinEvents()", "[client][room][getter]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    auto room = makeRoom(withRoomState({
        makeEvent(withEventType("m.room.pinned_events") | withEventContent(json{{"pinned", {"$1", "$2"}}})),
    }));
    ClientModel m = makeClient(withRoom(room));

    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );
    auto ctx = sdk.context();
    auto dispatcher = getMockDispatcher(
        sgph,
        ctx,
        returnEmpty<SendStateEventAction>()
    );
    auto mockContext = getMockContext(sgph, dispatcher);
    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());
    auto r = client.room(room.roomId);

    r.pinEvents({"$2", "$3", "$0"})
        .then([&](auto stat) {
            REQUIRE(stat.success());
            REQUIRE(dispatcher.template calledTimes<SendStateEventAction>() == 1);
            auto action = dispatcher.template of<SendStateEventAction>()[0];
            REQUIRE(action.roomId == room.roomId);
            REQUIRE(action.event.content().get().at("pinned") == immer::flex_vector<std::string>{"$1", "$2", "$3", "$0"});

            io.stop();
        });

    io.run();
}

TEST_CASE("Room::unpinEvents()", "[client][room][getter]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    auto room = makeRoom(withRoomState({
        makeEvent(withEventType("m.room.pinned_events") | withEventContent(json{{"pinned", {"$1", "$2"}}})),
    }));
    ClientModel m = makeClient(withRoom(room));

    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );
    auto ctx = sdk.context();
    auto dispatcher = getMockDispatcher(
        sgph,
        ctx,
        returnEmpty<SendStateEventAction>()
    );
    auto mockContext = getMockContext(sgph, dispatcher);
    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());
    auto r = client.room(room.roomId);

    r.unpinEvents({"$2", "$3", "$0"})
        .then([&](auto stat) {
            REQUIRE(stat.success());
            REQUIRE(dispatcher.template calledTimes<SendStateEventAction>() == 1);
            auto action = dispatcher.template of<SendStateEventAction>()[0];
            REQUIRE(action.roomId == room.roomId);
            REQUIRE(action.event.content().get().at("pinned") == immer::flex_vector<std::string>{"$1"});

            io.stop();
        });

    io.run();
}
