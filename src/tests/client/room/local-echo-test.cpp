/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <tuple>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_predicate.hpp>

#include <lager/event_loop/boost_asio.hpp>

#include <asio-promise-handler.hpp>
#include <cursorutil.hpp>
#include <sdk-model.hpp>
#include <client/client.hpp>
#include <crypto-util.hpp>
#include <cprjobhandler.hpp>
#include <lagerstoreeventemitter.hpp>
#include <debug.hpp>
#include <sdk.hpp>

#include "client-test-util.hpp"
#include "action-mock-utils.hpp"
#include "factory.hpp"

using namespace Kazv;
using namespace Kazv::Factory;
using Catch::Matchers::Predicate;

inline auto eventJson = json{
    {"content", {
        {"foo", "bar"},
    }},
    {"type", "m.room.message"},
};

template<class Store, class ...Handlers>
static auto makeDispatcher(SingleTypePromiseInterface<EffectStatus> &ph, Store &store, Handlers ...handlers)
{
    auto nextTxnId = std::make_shared<int>(0);
    return getMockDispatcher(
        ph,
        store,
        handlers...,
        returnEmpty<GetRoomStatesAction>(),
        returnEmpty<QueryKeysAction>(),
        returnEmpty<SendMessageAction>(),
        returnEmpty<SendMultipleToDeviceMessagesAction>(),
        returnResolved<ClaimKeysAction>(EffectStatus(true,
                json::object({{"keyEvent", json::object({})}}))),
        makeHandler<PrepareForSharingRoomKeyAction>([nextTxnId]([[maybe_unused]] auto &ph, auto &store, const PrepareForSharingRoomKeyAction &a) {
            auto txnId = std::to_string((*nextTxnId)++);
            return store.dispatch(UpdateRoomAction{
                a.roomId,
                AddPendingRoomKeyAction{
                    makePendingRoomKeyEventV0(
                        txnId,
                        Event(json{{"type", "m.room.encrypted"}, {"content", {{"whatever", "ok"}}}}),
                        // Mocking the device list
                        {{"@foo:example.com", {"device1", "device2"}}}
                    )
                }
            }).then([txnId](auto &&) {
                return EffectStatus{true, json::object({{"txnId", txnId}})};
            });
        }),
        passDown<SaveLocalEchoAction>(),
        passDown<EncryptMegOlmEventAction>(),
        passDown<RoomListAction>(),
        passDown<UpdateLocalEchoStatusAction>()
    );
}

TEST_CASE("Local echo", "[client][room]")
{
    RoomModel r;

    auto next = RoomModel::update(r, AddLocalEchoAction{{"txnId1", Event(eventJson)}});
    REQUIRE(!(next == r));
    next = RoomModel::update(next, AddLocalEchoAction{{"txnId2", Event(eventJson)}});
    REQUIRE(!(next == r));
    REQUIRE(next.localEchoes.size() == 2);
    REQUIRE_THAT(next.localEchoes[0], Predicate<LocalEchoDesc>([](const auto &desc) {
        return desc.txnId == "txnId1";
    }));
    REQUIRE_THAT(next.localEchoes[1], Predicate<LocalEchoDesc>([](const auto &desc) {
        return desc.txnId == "txnId2";
    }));
}

TEST_CASE("Remove local echo", "[client][room]")
{
    RoomModel r;
    r.localEchoes = immer::flex_vector<LocalEchoDesc>{
        {"txnId1", Event(eventJson)},
        {"txnId2", Event(eventJson)},
    };

    auto next = RoomModel::update(r, RemoveLocalEchoAction{"txnId1"});
    REQUIRE(next.localEchoes.size() == 1);
    REQUIRE_THAT(next.localEchoes[0], Predicate<LocalEchoDesc>([](const auto &desc) {
        return desc.txnId == "txnId2";
    }));
}

TEST_CASE("getLocalEchoByTxnId()", "[client][room]")
{
    RoomModel r;
    r.localEchoes = immer::flex_vector<LocalEchoDesc>{
        {"txnId1", Event(eventJson)},
        {"txnId2", Event(eventJson)},
    };

    REQUIRE(r.getLocalEchoByTxnId("txnId1").value() == r.localEchoes[0]);
    REQUIRE(!r.getLocalEchoByTxnId("txnId3").has_value());
}

TEST_CASE("Sending a message leaves a local echo", "[client][room]")
{
    ClientModel m;
    const auto roomId = "!foo:tusooa.xyz"s;
    m.roomList.rooms = m.roomList.rooms.set(roomId, RoomModel{});

    auto [next, dontCareEffect] = ClientModel::update(m, SendMessageAction{roomId, Event(eventJson)});

    auto localEchoes = next.roomList.rooms[roomId].localEchoes;
    REQUIRE(localEchoes.size() == 1);

    assert1Job(next);
    for1stJob(next, [localEchoes](const auto &job) {
        REQUIRE(job.dataStr("txnId") == localEchoes[0].txnId);
    });
}

TEST_CASE("Failed send changes the status of the local echo", "[client][room]")
{
    ClientModel m;
    const auto roomId = "!foo:tusooa.xyz"s;
    m.roomList.rooms = m.roomList.rooms.set(roomId, RoomModel{});

    auto [next, dontCareEffect] = ClientModel::update(m, SendMessageAction{roomId, Event(eventJson)});
    auto resp = makeResponse(
        "SendMessage",
        withResponseDataKV("roomId", roomId)
        | withResponseDataKV("txnId", next.roomList.rooms[roomId].localEchoes[0].txnId)
    );
    resp.statusCode = 500;
    std::tie(next, dontCareEffect) = ClientModel::update(next, ProcessResponseAction{resp});

    auto localEchoes = next.roomList.rooms[roomId].localEchoes;
    REQUIRE(localEchoes.size() == 1);
    REQUIRE(localEchoes[0].status == LocalEchoDesc::Failed);
}

TEST_CASE("Local echo with encrypted event", "[client][room]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m;
    m.crypto = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
    RoomModel room;
    room.encrypted = true;
    room.roomId = "!exampleroomid:example.com";
    m.roomList.rooms = m.roomList.rooms.set("!exampleroomid:example.com", room);
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();
    auto dispatcher = makeDispatcher(sgph, ctx);
    auto mockContext = getMockContext(sgph, dispatcher);

    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto r = client.room("!exampleroomid:example.com");

    REQUIRE(r.encrypted().make().get());

    r.sendTextMessage("test")
        .then([&io](auto) {
            kzo.client.dbg() << "ended" << std::endl;
            io.stop();
        });

    io.run();

    REQUIRE(dispatcher.template calledTimes<SaveLocalEchoAction>() == 2);
    REQUIRE(dispatcher.template calledTimes<SendMessageAction>() == 1);
    auto sendMessageAction = dispatcher.template of<SendMessageAction>()[0];
    REQUIRE(sendMessageAction.event.encrypted());
    REQUIRE(sendMessageAction.event.decrypted());
    REQUIRE(r.localEchoes().make().get().size() == 1);
    REQUIRE(r.localEchoes().make().get()[0].txnId == sendMessageAction.txnId);
}

TEST_CASE("Local echo with encrypted event, loading room member failed", "[client][room]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m;
    m.crypto = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
    RoomModel room;
    room.encrypted = true;
    room.roomId = "!exampleroomid:example.com";
    m.roomList.rooms = m.roomList.rooms.set("!exampleroomid:example.com", room);
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();
    auto dispatcher = makeDispatcher(sgph, ctx,
        returnResolved<GetRoomStatesAction>(EffectStatus(false, json::object({{"errorCode", "400"}, {"error", "Cannot get room states"}})))
    );
    auto mockContext = getMockContext(sgph, dispatcher);
    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto r = client.room("!exampleroomid:example.com");

    REQUIRE(r.encrypted().make().get());

    r.sendTextMessage("test")
        .then([&io](const auto &status) {
            kzo.client.dbg() << "ended" << std::endl;
            REQUIRE(!status.success());
            io.stop();
        });

    io.run();

    REQUIRE(dispatcher.template calledTimes<SaveLocalEchoAction>() == 1);
    REQUIRE(dispatcher.template calledTimes<SendMessageAction>() == 0);
    REQUIRE(dispatcher.template calledTimes<EncryptMegOlmEventAction>() == 0);
    REQUIRE(r.localEchoes().make().get().size() == 1);
    REQUIRE(r.localEchoes().make().get()[0].status == LocalEchoDesc::Failed);
}

TEST_CASE("Local echo with encrypted event, querying keys failed", "[client][room]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m;
    m.crypto = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
    RoomModel room;
    room.encrypted = true;
    room.roomId = "!exampleroomid:example.com";
    m.roomList.rooms = m.roomList.rooms.set("!exampleroomid:example.com", room);
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();
    auto dispatcher = makeDispatcher(sgph, ctx,
        returnResolved<QueryKeysAction>(EffectStatus(false, json::object({{"errorCode", "400"}, {"error", "Cannot get room states"}})))
    );
    auto mockContext = getMockContext(sgph, dispatcher);

    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto r = client.room("!exampleroomid:example.com");

    REQUIRE(r.encrypted().make().get());

    r.sendTextMessage("test")
        .then([&io](const auto &status) {
            kzo.client.dbg() << "ended" << std::endl;
            REQUIRE(!status.success());
            io.stop();
        });

    io.run();

    REQUIRE(dispatcher.template calledTimes<SaveLocalEchoAction>() == 1);
    REQUIRE(dispatcher.template calledTimes<SendMessageAction>() == 0);
    REQUIRE(dispatcher.template calledTimes<EncryptMegOlmEventAction>() == 0);
    REQUIRE(r.localEchoes().make().get().size() == 1);
    REQUIRE(r.localEchoes().make().get()[0].status == LocalEchoDesc::Failed);
}

TEST_CASE("Encrypted room: Resend encrypted local echo", "[client][room]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m;
    m.crypto = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
    RoomModel room;
    room.encrypted = true;
    room.roomId = "!exampleroomid:example.com";
    auto encryptedEvent = Event{json{
        {"content", {{"foo", "bar"}}},
        {"type", "m.room.encrypted"},
    }};
    auto decryptedJson = json{
        {"content", {{"dec-foo", "dec-bar"}}},
        {"type", "m.room.message"},
    };
    auto event = encryptedEvent.setDecryptedJson(decryptedJson, Event::Decrypted);
    room.localEchoes = immer::flex_vector<LocalEchoDesc>{
        {"some-txn-id", event, LocalEchoDesc::Failed},
    };
    m.roomList.rooms = m.roomList.rooms.set("!exampleroomid:example.com", room);
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();

    auto dispatcher = makeDispatcher(sgph, ctx);
    auto mockContext = getMockContext(sgph, dispatcher);

    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto r = client.room("!exampleroomid:example.com");

    REQUIRE(r.encrypted().make().get());

    r.resendMessage("some-txn-id")
        .then([&io](auto) {
            kzo.client.dbg() << "ended" << std::endl;
            io.stop();
        });

    io.run();

    REQUIRE(dispatcher.template calledTimes<SaveLocalEchoAction>() == 0);
    REQUIRE(dispatcher.template calledTimes<SendMessageAction>() == 1);
    REQUIRE(dispatcher.template calledTimes<EncryptMegOlmEventAction>() == 0);
    REQUIRE(r.localEchoes().make().get().size() == 1);
    REQUIRE(r.localEchoes().make().get()[0].txnId == "some-txn-id");
    auto a = dispatcher.template of<SendMessageAction>()[0];
    REQUIRE(a.txnId.has_value());
    REQUIRE(a.txnId.value() == "some-txn-id");
}

TEST_CASE("Encrypted room: Resend encrypted local echo AND failed key share event", "[client][room]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m;
    m.crypto = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
    RoomModel room;
    room.encrypted = true;
    room.roomId = "!exampleroomid:example.com";
    m.roomList.rooms = m.roomList.rooms.set("!exampleroomid:example.com", room);
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();
    auto resending = false;
    auto dispatcher = makeDispatcher(sgph, ctx,
        makeHandler<SendMultipleToDeviceMessagesAction>([&resending](auto &&ph, [[maybe_unused]] auto &&store, SendMultipleToDeviceMessagesAction) {
            if (!resending) {
                auto sendKeyData = json{
                    {"error", "Bad request"},
                    {"errorCode", "400"},
                };
                return HandlerResult<typename Client::PromiseT>{
                    ph.createResolved(EffectStatus(/* succ = */ false, sendKeyData))
                };
            }
            return HandlerResult<typename Client::PromiseT>{std::nullopt};
        })
    );

    auto mockContext = getMockContext(sgph, dispatcher);

    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto r = client.room("!exampleroomid:example.com");

    REQUIRE(r.encrypted().make().get());

    r.sendTextMessage("test")
        .then([&io](auto) {
            kzo.client.dbg() << "ended" << std::endl;
            io.stop();
        });

    io.run();

    REQUIRE(dispatcher.template calledTimes<SaveLocalEchoAction>() == 2);
    REQUIRE(dispatcher.template calledTimes<EncryptMegOlmEventAction>() == 1);
    REQUIRE(dispatcher.template calledTimes<SendMultipleToDeviceMessagesAction>() == 1);
    REQUIRE(dispatcher.template calledTimes<PrepareForSharingRoomKeyAction>() == 1);
    REQUIRE(dispatcher.template calledTimes<SendMessageAction>() == 0);

    REQUIRE(r.localEchoes().make().get().size() == 1);
    auto savedEvent = r.localEchoes().make().get()[0];
    auto txnId = savedEvent.txnId;
    REQUIRE(savedEvent.event.encrypted());

    dispatcher.clear();
    resending = true;

    r.resendMessage(txnId)
        .then([&io](auto) {
            kzo.client.dbg() << "resent" << std::endl;
            io.stop();
        });

    io.restart();
    io.run();

    REQUIRE(dispatcher.template calledTimes<SaveLocalEchoAction>() == 0);
    REQUIRE(dispatcher.template calledTimes<EncryptMegOlmEventAction>() == 0);
    REQUIRE(dispatcher.template calledTimes<SendMessageAction>() == 1);
    REQUIRE(dispatcher.template calledTimes<PrepareForSharingRoomKeyAction>() == 0);
    REQUIRE(dispatcher.template calledTimes<SendMultipleToDeviceMessagesAction>() == 1);
    REQUIRE(r.pendingRoomKeyEvents().make().get().size() == 0);
}

TEST_CASE("Encrypted room: Resend unencrypted local echo", "[client][room]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m;
    m.crypto = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
    RoomModel room;
    room.encrypted = true;
    room.roomId = "!exampleroomid:example.com";
    auto event = Event{json{
        {"content", {{"dec-foo", "dec-bar"}}},
        {"type", "m.room.message"},
    }};
    room.localEchoes = immer::flex_vector<LocalEchoDesc>{
        {"some-txn-id", event, LocalEchoDesc::Failed},
        {"some-other-txn-id", event, LocalEchoDesc::Failed},
    };
    m.roomList.rooms = m.roomList.rooms.set("!exampleroomid:example.com", room);
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();

    auto dispatcher = makeDispatcher(sgph, ctx);

    auto mockContext = getMockContext(sgph, dispatcher);
    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto r = client.room("!exampleroomid:example.com");

    REQUIRE(r.encrypted().make().get());

    r.resendMessage("some-txn-id")
        .then([&io](auto st) {
            REQUIRE(st.success());
            kzo.client.dbg() << "ended" << std::endl;
            io.stop();
        });

    io.run();


    REQUIRE(dispatcher.template calledTimes<SaveLocalEchoAction>() == 2);
    REQUIRE(dispatcher.template calledTimes<RoomListAction>() == 2); // once for removing the existing local echo, once for removing pending key event
    REQUIRE(dispatcher.template calledTimes<EncryptMegOlmEventAction>() == 1);
    REQUIRE(dispatcher.template calledTimes<SendMessageAction>() == 1);
    auto sendMessageAction = dispatcher.template of<SendMessageAction>()[0];
    REQUIRE(sendMessageAction.txnId.has_value());
    REQUIRE(r.localEchoes().make().get().size() == 2);
    REQUIRE(r.localEchoes().make().get()[0].txnId == "some-other-txn-id");
    REQUIRE(r.localEchoes().make().get()[1].txnId == sendMessageAction.txnId.value());
}

TEST_CASE("Unencrypted room: Resend unencrypted local echo", "[client][room]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m;
    m.crypto = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
    RoomModel room;
    room.encrypted = false;
    room.roomId = "!exampleroomid:example.com";
    auto event = Event{json{
        {"content", {{"dec-foo", "dec-bar"}}},
        {"type", "m.room.message"},
    }};
    room.localEchoes = immer::flex_vector<LocalEchoDesc>{
        {"some-txn-id", event, LocalEchoDesc::Failed},
    };
    m.roomList.rooms = m.roomList.rooms.set("!exampleroomid:example.com", room);
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();
    auto dispatcher = makeDispatcher(sgph, ctx);
    auto mockContext = getMockContext(sgph, dispatcher);
    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto r = client.room("!exampleroomid:example.com");

    REQUIRE(!r.encrypted().make().get());

    r.resendMessage("some-txn-id")
        .then([&io](auto) {
            kzo.client.dbg() << "ended" << std::endl;
            io.stop();
        });

    io.run();

    REQUIRE(dispatcher.template calledTimes<SaveLocalEchoAction>() == 0);
    REQUIRE(dispatcher.template calledTimes<EncryptMegOlmEventAction>() == 0);
    REQUIRE(dispatcher.template calledTimes<SendMessageAction>() == 1);
    REQUIRE(r.localEchoes().make().get().size() == 1);
    auto a = dispatcher.template of<SendMessageAction>()[0];
    REQUIRE(a.txnId.value() == "some-txn-id");
    REQUIRE(r.localEchoes().make().get()[0].txnId == "some-txn-id");
}

TEST_CASE("makePendingRoomKeyEventV0()", "[client][room][local-echo]")
{
    auto devices = immer::map<std::string, immer::flex_vector<std::string>>{
        {"@foo:example.com", {"device1"}},
        {"@bar:example.com", {"device2", "device3"}},
    };
    std::string txnId = "xxx";
    Event event = json{{"type", "m.room.encrypted"}, {"content", {{"whatever", "ok"}}}};
    auto e = makePendingRoomKeyEventV0(txnId, event, devices);
    auto expected = immer::map<std::string, immer::map<std::string, Event>>{
        {"@foo:example.com", {{"device1", event}}},
        {"@bar:example.com", {{"device2", event}, {"device3", event}}},
    };
    REQUIRE(e.messages == expected);
}
