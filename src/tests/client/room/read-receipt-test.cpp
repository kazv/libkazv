/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <boost/asio.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_range_equals.hpp>

#include <lager/event_loop/boost_asio.hpp>

#include <asio-promise-handler.hpp>
#include <room/room-model.hpp>
#include <sdk-model.hpp>
#include <client/client.hpp>
#include <client/actions/ephemeral.hpp>
#include <cprjobhandler.hpp>
#include <lagerstoreeventemitter.hpp>

#include <testfixtures/factory.hpp>

#include "client-test-util.hpp"
#include "action-mock-utils.hpp"
#include "push-rules-test-util.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

TEST_CASE("Adding an m.receipt ephemeral event", "[client][room][receipt]")
{
    auto room = makeRoom();
    auto receiptEvent = makeEvent(
        withEventType("m.receipt")
        // https://spec.matrix.org/v1.8/client-server-api/#events-5
        | withEventContent(R"({
            "$1435641916114394fHBLK:matrix.org": {
              "m.read": {
                "@rikj:jki.re": {
                  "ts": 1436451550453
                }
              },
              "m.read.private": {
                "@self:example.org": {
                  "ts": 1661384801651
                }
              }
            }
          })"_json));

    room = RoomModel::update(room, AddEphemeralAction{{receiptEvent}});

    auto receipt = room.readReceipts.at("@rikj:jki.re");
    REQUIRE(
        room.readReceipts.at("@rikj:jki.re")
        ==
        ReadReceipt{"$1435641916114394fHBLK:matrix.org", 1436451550453}
    );
    REQUIRE(
        room.eventReadUsers.at("$1435641916114394fHBLK:matrix.org")
        ==
        immer::flex_vector<std::string>{"@rikj:jki.re", "@self:example.org"}
    );

    auto anotherReceiptEvent = makeEvent(
        withEventType("m.receipt")
        | withEventContent(json{
            {"$123", {
                {"m.read", {
                    {"@foo:example.com", {{"ts", 1796451550450}}}
                }},
            }},
        }));

    room = RoomModel::update(room, AddEphemeralAction{{anotherReceiptEvent}});
    // the receipt that was there should still be there.
    REQUIRE(
        room.readReceipts.at("@rikj:jki.re")
        ==
        ReadReceipt{"$1435641916114394fHBLK:matrix.org", 1436451550453}
    );

    REQUIRE(
        room.readReceipts.at("@foo:example.com")
        ==
        ReadReceipt{"$123", 1796451550450}
    );

    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    auto model = makeClient(withRoom(room));
    auto store = createTestClientStoreFrom(model, ph);
    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }),
        store,
        std::nullopt);
    auto r = client.room(room.roomId);
    auto readers1 = r.eventReaders(lager::make_constant<std::string>("$1435641916114394fHBLK:matrix.org")).make().get();
    auto expected1 = immer::flex_vector<EventReader>{{"@rikj:jki.re", 1436451550453}, {"@self:example.org", 1661384801651}};
    REQUIRE_THAT(readers1, Catch::Matchers::UnorderedRangeEquals(expected1));

    auto readers2 = r.eventReaders(lager::make_constant<std::string>("$123")).make().get();
    auto expected2 = immer::flex_vector<EventReader>{{"@foo:example.com", 1796451550450}};
    REQUIRE(readers2 == expected2);
}

TEST_CASE("Update a receipt for some user", "[client][room][receipt]")
{
    auto room = makeRoom();
    auto receiptEvent = makeEvent(
        withEventType("m.receipt")
        // https://spec.matrix.org/v1.8/client-server-api/#events-5
        | withEventContent(R"({
            "$1435641916114394fHBLK:matrix.org": {
              "m.read": {
                "@rikj:jki.re": {
                  "ts": 1436451550453
                }
              }
            }
          })"_json));

    room = RoomModel::update(room, AddEphemeralAction{{receiptEvent}});

    auto anotherReceiptEvent = makeEvent(
        withEventType("m.receipt")
        | withEventContent(json{
            {"$123", {
                {"m.read", {
                    {"@rikj:jki.re", {{"ts", 1796451550450}}}
                }},
            }},
        }));

    room = RoomModel::update(room, AddEphemeralAction{{anotherReceiptEvent}});
    REQUIRE(
        room.readReceipts.at("@rikj:jki.re")
        ==
        ReadReceipt{"$123", 1796451550450}
    );

    REQUIRE(room.eventReadUsers.count("$1435641916114394fHBLK:matrix.org") == 0);
    REQUIRE(room.eventReadUsers.count("$123") == 1);

    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    auto model = makeClient(withRoom(room));
    auto store = createTestClientStoreFrom(model, ph);
    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }),
        store,
        std::nullopt);
    auto r = client.room(room.roomId);
    auto readers1 = r.eventReaders(lager::make_constant<std::string>("$1435641916114394fHBLK:matrix.org")).make().get();
    auto expected1 = immer::flex_vector<EventReader>{};
    REQUIRE(readers1 == expected1);

    auto readers2 = r.eventReaders(lager::make_constant<std::string>("$123")).make().get();
    auto expected2 = immer::flex_vector<EventReader>{{"@rikj:jki.re", 1796451550450}};
    REQUIRE(readers2 == expected2);
}

TEST_CASE("localReadMarker", "[client][room][receipt]")
{
    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    auto tl = EventList{
        makeEvent(),
    };
    auto room = makeRoom(withRoomTimeline(tl));
    room.localReadMarker = tl[0].id();
    auto model = makeClient(withRoom(room));
    auto store = createTestClientStoreFrom(model, ph);
    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }),
        store,
        std::nullopt);
    auto r = client.room(room.roomId);
    REQUIRE(r.localReadMarker().get() == tl[0].id());
}

TEST_CASE("Posting receipts", "[client][room][receipt]")
{
    auto r = makeRoom();
    auto m = makeClient(withRoom(r));

    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};
    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );

    auto ctx = sdk.context();

    auto dispatcher = getMockDispatcher(
        sgph,
        ctx,
        returnEmpty<PostReceiptAction>()
    );
    auto mockContext = getMockContext(sgph, dispatcher);
    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());

    auto room = client.room(r.roomId);

    room.postReceipt("$1")
        .then([&io](auto) {
            io.stop();
        });
    io.run();

    REQUIRE(dispatcher.template calledTimes<PostReceiptAction>() == 1);
    auto action = dispatcher.template of<PostReceiptAction>()[0];
    REQUIRE(action.roomId == r.roomId);
    REQUIRE(action.eventId == "$1");
}

TEST_CASE("PostReceiptAction", "[client][room][receipt]")
{
    auto m = makeClient();
    auto [next, _ignore] = updateClient(m, PostReceiptAction{"!someroom:example.com", "$someevent"});
    assert1Job(next);
    for1stJob(next, [](const BaseJob &job) {
        REQUIRE(job.jobId() == "PostReceipt");
        REQUIRE(job.url().find("rooms/!someroom:example.com/receipt/m.read/$someevent") != std::string::npos);
        REQUIRE(json::parse(std::get<Bytes>(job.requestBody())) == json::object());
    });
}

static auto pushRules = PushRulesDesc(Event(R"({
  "type": "m.push_rules",
  "content": {
    "global": {
      "override": [{
        "rule_id": "moe.kazv.mxc.some-rule",
        "default": true,
        "enabled": true,
        "conditions": [{
          "kind": "event_match",
          "key": "type",
          "pattern": "moe.kazv.mxc.test-event"
        }],
        "actions": ["notify"]
      }]
    }
  }
})"_json));

TEST_CASE("AddLocalNotificationsAction", "[client][room][receipt]")
{
    auto myUserId = "@mew:example.com"s;

    auto newEvents = EventList{
        makeEvent(withEventType("moe.kazv.mxc.test-event")),
        makeEvent(),
        makeEvent(withEventType("moe.kazv.mxc.test-event")),
        makeEvent(withEventType("moe.kazv.mxc.test-event") | withEventSenderId(myUserId)),
        makeEvent(withEventType("moe.kazv.mxc.test-event")),
    };
    auto r = makeRoom(withRoomTimeline(newEvents));
    r.readReceipts = {{myUserId, ReadReceipt{newEvents[0].id(), 0}}};

    WHEN("push rules does not notify") {
        auto next = RoomModel::update(r, AddLocalNotificationsAction{
            newEvents,
            PushRulesDesc(Event()),
            myUserId,
        });
        REQUIRE(next.unreadNotificationEventIds == immer::flex_vector<std::string>{});
    }

    WHEN("push rules notifies some") {
        auto next = RoomModel::update(r, AddLocalNotificationsAction{
            newEvents,
            pushRules,
            myUserId,
        });
        REQUIRE(
            next.unreadNotificationEventIds
            == immer::flex_vector<std::string>{
                newEvents[2].id(),
                newEvents[4].id(),
            }
        );
    }

    WHEN("push rules notifies some but is before local read marker, only things after local read marker is added") {
        r.localReadMarker = newEvents[3].id();
        auto next = RoomModel::update(r, AddLocalNotificationsAction{
            newEvents,
            pushRules,
            myUserId,
        });

        REQUIRE(
            next.unreadNotificationEventIds
            == immer::flex_vector<std::string>{
                newEvents[4].id(),
            }
        );
    }
}

TEST_CASE("PostReceiptAction should update local read marker and remove read notifications")
{
    auto myUserId = "@mew:example.com"s;

    auto newEvents = EventList{
        makeEvent(withEventType("moe.kazv.mxc.test-event")),
        makeEvent(withEventType("moe.kazv.mxc.test-event")),
        makeEvent(withEventType("moe.kazv.mxc.test-event")),
    };
    auto r = makeRoom(withRoomTimeline(newEvents));
    r.readReceipts = {{myUserId, ReadReceipt{newEvents[0].id(), 0}}};
    r = RoomModel::update(r, AddLocalNotificationsAction{
        newEvents,
        pushRules,
        myUserId,
    });

    auto client = makeClient(withRoom(r));
    client.userId = myUserId;
    auto receiptPos = newEvents[1].id();
    auto [next, _] = ClientModel::update(client, PostReceiptAction{r.roomId, receiptPos});
    auto nextRoom = next.roomList.rooms.at(r.roomId);
    REQUIRE(nextRoom.localReadMarker == receiptPos);
    REQUIRE(nextRoom.unreadNotificationEventIds == immer::flex_vector<std::string>{newEvents[2].id()});
}

TEST_CASE("RemoveReadLocalNotificationsAction", "[client][room][receipt]")
{
    auto myUserId = "@mew:example.com"s;

    auto oldEvents = EventList{
        makeEvent(),
        makeEvent(),
    };
    auto newEvents = EventList{
        makeEvent(),
        makeEvent(),
        makeEvent(),
        makeEvent(),
        makeEvent(),
        makeEvent(),
    };
    auto timeline = oldEvents + newEvents;
    auto r = makeRoom(withRoomTimeline(timeline));
    r.unreadNotificationEventIds = intoImmer(
        immer::flex_vector<std::string>(),
        zug::map(&Event::id),
        newEvents.erase(5)
    );

    WHEN("a read receipt is in the middle") {
        r.readReceipts = {{myUserId, ReadReceipt{newEvents[2].id(), 0}}};
        auto next = RoomModel::update(r, RemoveReadLocalNotificationsAction{myUserId});
        REQUIRE(next.unreadNotificationEventIds == immer::flex_vector<std::string>{newEvents[3].id(), newEvents[4].id()});
    }

    WHEN("a read receipt is before the beginning") {
        r.readReceipts = {{myUserId, ReadReceipt{oldEvents[0].id(), 0}}};
        auto next = RoomModel::update(r, RemoveReadLocalNotificationsAction{myUserId});
        REQUIRE(next.unreadNotificationEventIds == r.unreadNotificationEventIds);
    }

    WHEN("a read receipt does not exist") {
        r.readReceipts = {};
        auto next = RoomModel::update(r, RemoveReadLocalNotificationsAction{myUserId});
        REQUIRE(next.unreadNotificationEventIds == r.unreadNotificationEventIds);
    }

    WHEN("a read receipt is at the end") {
        r.readReceipts = {{myUserId, ReadReceipt{newEvents[5].id(), 0}}};
        auto next = RoomModel::update(r, RemoveReadLocalNotificationsAction{myUserId});
        REQUIRE(next.unreadNotificationEventIds == immer::flex_vector<std::string>{});
    }

    WHEN("a read receipt is after the end") {
        r.readReceipts = {{myUserId, ReadReceipt{newEvents[5].id(), 0}}};
        auto next = RoomModel::update(r, RemoveReadLocalNotificationsAction{myUserId});
        REQUIRE(next.unreadNotificationEventIds == immer::flex_vector<std::string>{});
    }
}

TEST_CASE("Room::unreadNotificationEventIds()", "[client][room][receipt]")
{
    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    auto room = makeRoom();
    room.unreadNotificationEventIds = {"$foo", "$bar"};
    auto model = makeClient(withRoom(room));
    auto store = createTestClientStoreFrom(model, ph);
    auto client = Client(store.reader().map([](auto c) { return SdkModel{c}; }),
        store,
        std::nullopt);

    auto r = client.room(room.roomId);
    REQUIRE(r.unreadNotificationEventIds().get()
        == room.unreadNotificationEventIds);
}
