/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 April Simone <apr3vau@outlook.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <lager/event_loop/boost_asio.hpp>
#include <cprjobhandler.hpp>
#include <lagerstoreeventemitter.hpp>

#include <catch2/catch_all.hpp>
#include <boost/asio.hpp>
#include <asio-promise-handler.hpp>
#include <cursorutil.hpp>
#include <sdk-model.hpp>
#include <client/client.hpp>
#include <client/client-model.hpp>

#include "client-test-util.hpp"
#include "action-mock-utils.hpp"
#include "factory.hpp"

using namespace Kazv::Factory;

TEST_CASE("Send logout job", "[client][auth]")
{
    ClientModel loggedInModel = makeClient({});
    auto [resModel, dontCareEffect] = ClientModel::update(
        loggedInModel, HardLogoutAction{});
    assert1Job(resModel);
    for1stJob(resModel, [] (const auto &job) {
        REQUIRE(job.jobId() == "Logout");
        REQUIRE(job.url() == "example.com/_matrix/client/v3/logout");
    });
}

TEST_CASE("Process logout response", "[client][auth]")
{
    boost::asio::io_context io;
    AsioPromiseHandler ph{io.get_executor()};

    auto store = createTestClientStore(ph);

    WHEN("Success response")
    {
        auto succResponse = makeResponse("Logout");
        store.dispatch(ProcessResponseAction{succResponse})
            .then([&] (auto stat) {
                REQUIRE(stat.success());
                REQUIRE(store.reader().get().token == "");
                REQUIRE(store.reader().get().loggedIn == false);
            });
    }
    WHEN("Failed response")
    {
        auto failResponse = makeResponse("Logout", withResponseStatusCode(404));
        store.dispatch(ProcessResponseAction{failResponse})
            .then([] (auto stat) {
                REQUIRE(!stat.success());
            });
    }
    io.run();
}

TEST_CASE("Test for stop syncing", "[client][auth]")
{
    boost::asio::io_context io;
    SingleTypePromiseInterface<EffectStatus> sgph{AsioPromiseHandler{io.get_executor()}};

    ClientModel m = makeClient({});

    auto jh = Kazv::CprJobHandler{io.get_executor()};
    auto ee = Kazv::LagerStoreEventEmitter(lager::with_boost_asio_event_loop{io.get_executor()});

    auto sdk = Kazv::makeSdk(
        SdkModel{m},
        jh,
        ee,
        Kazv::AsioPromiseHandler{io.get_executor()},
        zug::identity
    );
    auto ctx = sdk.context();
    auto dispatcher = getMockDispatcher(
        sgph,
        ctx,
        passDown<SetShouldSyncAction>(),
        returnEmpty<HardLogoutAction>()
    );
    auto mockContext = getMockContext(sgph, dispatcher);
    auto client = Client(Client::InEventLoopTag{}, mockContext, sdk.context());
    client.logout()
        .then([&] (auto stat) {
            REQUIRE(stat.success());
            REQUIRE(client.shouldSync().make().get() == false);
            io.stop();
        });
    io.run();
}
