/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <vector>
#include <boost/hana.hpp>
#include <boost/core/demangle.hpp>
#include <zug/into_vector.hpp>
#include <zug/transducer/map.hpp>
#include <zug/transducer/filter.hpp>
#include <store/store.hpp>
#include <client/client.hpp>

template<class Action>
struct PassDownTag
{};

template<class Action>
constexpr PassDownTag<Action> passDown()
{
    return {};
};

template<class Action, class DataT = Kazv::EffectStatus>
struct ReturnResolvedTag
{
    DataT data;
};

template<class Action, class DataT = Kazv::EffectStatus>
constexpr ReturnResolvedTag<Action, DataT> returnResolved(DataT data)
{
    return {data};
}

template<class Action, class DataT = Kazv::EffectStatus>
constexpr ReturnResolvedTag<Action, DataT> returnEmpty()
{
    return {{}};
}

template<class SubAction, class FuncT>
struct HandlerTag : public FuncT
{
    HandlerTag(FuncT f) : FuncT(std::move(f)) {}
};

template<class SubAction, class FuncT>
constexpr auto makeHandler(FuncT f)
{
    return HandlerTag<SubAction, FuncT>(std::move(f));
};

template<class R, class F, class = int, class ...Args>
struct IsExactInvocableHelper : public std::false_type
{};

template<class R, class F, class ...Args>
struct IsExactInvocableHelper<
    R,
    F,
    std::enable_if_t<std::is_same_v<std::invoke_result_t<F, Args...>, R>, int>,
    Args...> : public std::true_type
{};

template<class R, class F, class ...Args>
constexpr auto isExactInvocable = IsExactInvocableHelper<R, F, int, Args...>::value;

static_assert(isExactInvocable<int, std::function<int(long long)>, long long>);

template<class T>
std::string getTypeName(const T &a)
{
    return boost::core::demangle(typeid(a).name());
}

template<class Variant>
std::string getVariantTypeName(const Variant &v)
{
    return std::visit([](const auto &a) {
        return getTypeName(a);
    }, v);
}

template<class Promise>
struct HandlerResult
{
    std::optional<Promise> retVal;
};

template<class PH, class Context, class Action = Kazv::Client::ActionT>
struct MockDispatcher
{
    using ContextT = Context;
    using ActionT = Action;
    using PromiseT = typename ContextT::PromiseT;
    using DataT = typename PromiseT::DataT;
    using HandlerResultT = HandlerResult<PromiseT>;

    /**
     * A handler for mock dispatchers.
     *
     * It can be constructed from a function with the signature
     * `HandlerResult<ContextT::PromiseT>(PH &, ContextT &, ActionT)`
     * or it can be constructed from the following convenient
     * forms called handler tags, where `ActionType` is a type in the variant ActionT:
     *
     * - `passDown<ActionType>()` will dispatch the action to the underlying context, and return the promise returned from the dispatch. Actions other than ActionType will not be matched.
     * - `returnResolved<ActionType>(DataT data)` will return a Promise that is resolved and contains `data` when the action is of ActionType. Actions other than ActionType will not be matched.
     * - `returnEmpty<ActionType>()` is equivalent to `returnResolved<ActionType>({}).
     * - `makeHandler<ActionType>(Func func)`, where `func` has any of the following signature:
     *   1. `HandlerResult<PromiseT>(PH &, ContextT &, ActionType)`. It will be called when the action is of ActionType, and the return value from it will be the result. Actions other than ActionType will not be matched and will not be executed on this function.
     *   2. `PromiseT(PH &, ContextT &, ActionType)`. It will be called  when the action is of ActionType, and in this case it will always be a match, and the result is a HandlerResult with the returned promise. Actions other than ActionType will not be matched and will not be executed on this function.
     *   3. `HandlerResult<PromiseT>(ActionType)`. Same as the first case, but only the action is passed to the function, and not the promise handler and the context.
     *   4. `PromiseT(ActionType)`. Same as the second case, but only the action is passed to the function, and not the promise handler and the context.
     *   5. `DataT(ActionType)`. Same as the fourth case, but the promise returned is one that is resolved and contains the return value of the function.
     *   6. `void(ActionType)`. Same as the fourth case, but the promise returned is one that is resolved and contains a default-constructed `DataT`.
     */
    struct Handler : public std::function<HandlerResultT(PH &, ContextT &, ActionT)>
    {
        using BaseT = std::function<HandlerResultT(PH &, ContextT &, ActionT)>;
        using BaseT::BaseT;
        using BaseT::operator();

        template<class SubAction, class FuncT>
        static BaseT make(FuncT func)
        {
            using Func = std::decay_t<FuncT>;
            if constexpr (isExactInvocable<HandlerResultT, Func, PH &, ContextT &, SubAction>) {
                return [func](PH &ph, ContextT &ctx, ActionT action) mutable -> HandlerResultT {
                    static_assert(boost::hana::is_valid([]() {
                        return boost::hana::type_c<
                            decltype(std::get<SubAction>(action))>;
                    })(),
                        "SubAction must be a variant alternative of ActionT");
                    if (std::holds_alternative<SubAction>(action)) {
                        return func(ph, ctx, std::get<SubAction>(action));
                    }
                    return {std::nullopt};
                };
            } else if constexpr (isExactInvocable<PromiseT, Func, PH &, ContextT &, SubAction>) {
                return make<SubAction>([func](PH &ph, ContextT &ctx, SubAction subAction) mutable {
                    return HandlerResultT{func(ph, ctx, subAction)};
                });
            } else if constexpr (isExactInvocable<HandlerResultT, Func, SubAction>) {
                return make<SubAction>([func](PH &, ContextT &, SubAction subAction) mutable {
                    return func(subAction);
                });
            } else if constexpr (isExactInvocable<PromiseT, Func, SubAction>) {
                return make<SubAction>([func](PH &, ContextT &, SubAction subAction) mutable {
                    return func(subAction);
                });
            } else if constexpr (isExactInvocable<DataT, Func, SubAction>) {
                return make<SubAction>([func](PH &ph, ContextT &, SubAction subAction) mutable {
                    return ph.createResolved(func(subAction));
                });
            } else if constexpr (isExactInvocable<void, Func, SubAction>) {
                return make<SubAction>([func](PH &ph, ContextT &, SubAction subAction) mutable {
                    func(subAction);
                    return ph.createResolved({});
                });
            } else {
                // This is a trick to avoid compilers reporting failure
                // even when this branch is never executed for any Func
                // https://stackoverflow.com/questions/38304847/how-does-a-failed-static-assert-work-in-an-if-constexpr-false-block
                static_assert(!sizeof(Func), "Function is not convertible to an action handler");
                return [func](PH &, ContextT &, ActionT) mutable -> HandlerResultT {
                    return {std::nullopt};
                };
            }
        }

        template<class SubAction>
        Handler(PassDownTag<SubAction>)
            : BaseT(make<SubAction>([](PH &, ContextT &ctx, SubAction action) mutable {
                Kazv::kzo.client.dbg() << "PassDown" << std::endl;
                return ctx.dispatch(action);
            }))
        {}

        template<class SubAction>
        Handler(ReturnResolvedTag<SubAction, DataT> tag)
            : BaseT(make<SubAction>([data=tag.data](SubAction) mutable {
                Kazv::kzo.client.dbg() << "ReturnResolved" << std::endl;
                return data;
            }))
        {}

        template<class SubAction, class FuncT>
        Handler(HandlerTag<SubAction, FuncT> func)
            : BaseT(make<SubAction>(func))
        {}
    };

    PromiseT operator()(ActionT action)
    {
        actions->push_back(action);
        std::string typeName = getVariantTypeName(action);
        Kazv::kzo.client.dbg() << "Handling action " << typeName << std::endl;
        for (auto &handler : handlers) {
            auto res = handler(ph, ctx, action);
            if (res.retVal.has_value()) {
                return res.retVal.value();
            }
        }
        throw std::runtime_error{"unhandled action: " + typeName};
    }

    template<class SubAction>
    auto calledTimes()
    {
        return std::accumulate(actions->begin(), actions->end(),
            0,
            [](auto acc, auto cur) {
                return acc + (std::holds_alternative<SubAction>(cur) ? 1 : 0);
            }
        );
    }

    template<class SubAction>
    std::vector<SubAction> of()
    {
        return zug::into_vector(
            zug::filter([](const auto &a) { return std::holds_alternative<SubAction>(a); })
            | zug::map([](const auto &a) { return std::get<SubAction>(a); }),
            *actions
        );
    }

    auto clear() { actions->clear(); }

    PH &ph;
    ContextT &ctx;
    std::vector<Handler> handlers;
    std::shared_ptr<std::vector<ActionT>> actions{std::make_shared<std::vector<ActionT>>()};
};

/**
 * This is the main entry for getting a mocked dispatcher.
 *
 * The handlers are added sequentially, and each of them will
 * be run with the action as the argument. It will stop at the
 * first handler that *declares* a match. See
 * MockDispatcher::Handler for more information on how to make
 * a handler.
 *
 * A handler is a function of the signature `HandlerResult<ContextT::PromiseT>(PH &, ContextT &, ActionT)`.
 * In the case of making a dispatcher for `Client`, `ActionT` is `ClientAction`.
 *
 * For a handler to match, it needs to return a `HandlerResult`
 * that contains a Promise. If it returns a `HandlerResult`
 * with `std::nullopt`, it is considered a no-match and the
 * next handlers will continue execute until it gets a match.
 * If no matches are found, it will throw an exception saying
 * `unhandled action: <type>`.
 *
 * For example,
 * ```
 * getMockDispatcher(ph, ctx,
 *     passDown<A>(),
 *     returnResolved<B>({false, {}}),
 *     makeHandler<C>([](C) {
 *         std::cerr << "C" << std::endl;
 *         return HandlerResult<Promise>(std::nullopt);
 *     }),
 *     makeHandler<D>([&](D) {
 *         std::cerr << "D" << std::endl;
 *         return ctx.createResolved({});
 *     }),
 *     returnEmpty<B>())
 * ```
 *
 * This should pass down action `A` to `ctx`, return a resolved
 * failed promise for action `B`. For action `C`, it will print out "C" and throw an exception saying `unhandled action: C`
 * (because the handler does not match, as it returns a result
 * containing std::nullopt). For action `D`, it will print out
 * "D" and return a resolved empty promise. For all other actions
 * it will throw an exception saying `unhandled action: <type>`.
 *
 * @param ph The promise handler.
 * @param ctx The original context that can be used to dispatch
 * actions. This is needed for `passDown` handlers.
 * @param handlers The handlers you want to add.
 * @return A mocked dispatcher.
 */
template<class PH, class ContextT, class ...Handlers>
auto getMockDispatcher(PH &ph, ContextT &ctx, Handlers ...handlers)
{
    using ResType = MockDispatcher<PH, ContextT>;
    return ResType{
        ph,
        ctx,
        std::vector<typename ResType::Handler>{ {handlers...} }
    };
}

template<
    class ContextT = typename Kazv::Client::ContextT,
    class ActionT = typename Kazv::Client::ActionT,
    class PH,
    class Func>
auto getMockContext(PH &ph, Func &&func)
{
    return ContextT(std::forward<Func>(func), ph, lager::deps<>{});
};
