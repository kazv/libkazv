/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_all.hpp>
#include <client/actions/encryption.hpp>
#include <client-model.hpp>

#include "client-test-util.hpp"
#include "factory.hpp"

using namespace Kazv::Factory;

namespace
{
    struct CreateE2EESessionResult
    {
        ClientModel receiver1;
        ClientModel receiver2;
        ClientModel sender;
    };

    struct CreateMegOlmSessionResult
    {
        Event encryptedRoomEvent;
        Event encryptedKeyEvent;
        Event unencryptedKeyEvent;
    };
}

static CreateE2EESessionResult createE2EESession()
{
    auto makeDeviceInfo = [](const ClientModel &client) {
        auto [next, _] = updateClient(client, UploadIdentityKeysAction{});
        return json::parse(std::get<Bytes>(next.nextJobs[0].requestBody()))["device_keys"];
    };

    auto r1Crypto = makeCrypto();
    r1Crypto.genOneTimeKeysWithRandom(genRandomData(Crypto::genOneTimeKeysRandomSize(1)), 1);
    auto r1 = makeClient(withCrypto(r1Crypto));
    r1.userId = "@receiver:example.com";
    r1.deviceId = "device1";
    auto r2Crypto = makeCrypto();
    r2Crypto.genOneTimeKeysWithRandom(genRandomData(Crypto::genOneTimeKeysRandomSize(1)), 1);
    auto r2 = makeClient(withCrypto(r2Crypto));
    r2.userId = "@receiver:example.com";
    r2.deviceId = "device2";

    auto oneTimeKeys1 = r1Crypto.unpublishedOneTimeKeys();
    auto cv25519Key1 = oneTimeKeys1["curve25519"].items().begin().value().template get<std::string>();
    auto oneTimeKeys2 = r2Crypto.unpublishedOneTimeKeys();
    auto cv25519Key2 = oneTimeKeys2["curve25519"].items().begin().value().template get<std::string>();

    auto queryKeysRespJsonSender = json{
        {"device_keys", {{"@receiver:example.com", {
            {"device1", makeDeviceInfo(r1)},
            {"device2", makeDeviceInfo(r2)},
        }}}},
    };

    // Query keys
    auto client = makeClient(withCrypto(makeCrypto()));
    client.userId = "@sender:example.com";
    client.deviceId = "device1";
    auto queryKeysRespJsonReceiver = json{
        {"device_keys", {{"@sender:example.com", {
            {"device1", makeDeviceInfo(client)}
        }}}},
    };

    std::tie(client, std::ignore) = processResponse(client, QueryKeysResponse(
        makeResponse("QueryKeys", withResponseJsonBody(queryKeysRespJsonSender))
    ));
    std::tie(r1, std::ignore) = processResponse(r1, QueryKeysResponse(
        makeResponse("QueryKeys", withResponseJsonBody(queryKeysRespJsonReceiver))
    ));
    std::tie(r2, std::ignore) = processResponse(r2, QueryKeysResponse(
        makeResponse("QueryKeys", withResponseJsonBody(queryKeysRespJsonReceiver))
    ));

    // Claim keys
    client.withCrypto([&](auto &c) { c.createOutboundSessionWithRandom(genRandomData(Crypto::createOutboundSessionRandomSize()), r1Crypto.curve25519IdentityKey(), cv25519Key1); });
    client.withCrypto([&](auto &c) { c.createOutboundSessionWithRandom(genRandomData(Crypto::createOutboundSessionRandomSize()), r2Crypto.curve25519IdentityKey(), cv25519Key2); });

    return {
        r1,
        r2,
        client,
    };
}

static CreateMegOlmSessionResult createMegOlmSession(ClientModel &sender, ClientModel &receiver, std::string roomId, Event plainText)
{
    auto sessionKey = sender.withCrypto([&](auto &c) {
        return c.rotateMegOlmSessionWithRandom(genRandomData(Crypto::rotateMegOlmSessionRandomSize()), 0, roomId);
    });

    auto mod = withRoom(makeRoom(withRoomId(roomId) | withRoomEncrypted(true)));
    mod(sender);
    mod(receiver);

    auto [encrypted, _noSessionKey] = sender.megOlmEncrypt(plainText, roomId, 0,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize()));
    auto sessionId = encrypted.originalJson().get()["content"]["session_id"].template get<std::string>();

    auto keyEventJson = json{
        {"content", {{"algorithm", CryptoConstants::megOlmAlgo},
                     {"room_id", roomId},
                     {"session_id", sessionId},
                     {"session_key", sessionKey}}},
        {"type", "m.room_key"}
    };

    auto res = sender.olmEncryptSplit(Event(keyEventJson),
        {{receiver.userId, {receiver.deviceId}}},
        genRandomData(Crypto::encryptOlmMaxRandomSize() * 2));

    auto encryptedKeyEventJson = res[receiver.userId][receiver.deviceId].originalJson().get();
    encryptedKeyEventJson["sender"] = sender.userId;
    auto encryptedKeyEvent = Event(encryptedKeyEventJson);
    std::cerr << "room event" << encrypted.originalJson().get().dump() << std::endl;
    std::cerr << "key event" << encryptedKeyEvent.originalJson().get().dump() << std::endl;

    keyEventJson["keys"] = json{
        {CryptoConstants::ed25519, sender.constCrypto().ed25519IdentityKey()},
    };
    keyEventJson["sender"] = sender.userId;
    return {encrypted, encryptedKeyEvent, Event(keyEventJson)};
}

static Response syncResponseFromToDevice(Event toDevice)
{
    auto j = json{
        {"next_batch", "something"},
        {"to_device", {
            {"events", json::array({toDevice.originalJson().get()})},
        }},
    };

    return makeResponse(
        "Sync",
        withResponseJsonBody(j)
        | withResponseDataKV("is", "incremental"));
}

TEST_CASE("PrepareForSharingRoomKeyAction: adds the encrypted event to pending events", "[client][encryption]")
{
    ClientModel m;
    m.crypto = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
    RoomModel room;
    room.encrypted = true;
    room.roomId = "!exampleroomid:example.com";
    m.roomList.rooms = m.roomList.rooms.set("!exampleroomid:example.com", room);

    auto event = Event{json{
        {"type", "m.room_key"},
        {"content", {{"some", "thing"}}},
    }};

    auto [next, dontCareEffect] = ClientModel::update(m, PrepareForSharingRoomKeyAction{"!exampleroomid:example.com", {}, event, {}});

    auto nextRoom = next.roomList.rooms.at("!exampleroomid:example.com");
    REQUIRE(nextRoom.pendingRoomKeyEvents.size() == 1);
}

TEST_CASE("encrypted event will keep a copy of m.relates_to in plaintext", "[client][encryption]")
{
    auto room = makeRoom(withRoomEncrypted(true));
    auto client = makeClient(
        withCrypto(makeCrypto())
        | withRoom(room)
    );

    auto eventToEncrypt = makeEvent(
        withEventType("m.room.message")
        | withEventRelationship("moe.kazv.mxc.custom-rel-type", "$some-event-id")
    );

    auto [encryptedEvent, maybeKey] = client.megOlmEncrypt(
        eventToEncrypt,
        room.roomId,
        0,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize())
    );

    // because we do not have session key yet, it should always be rotated
    REQUIRE(maybeKey.has_value());

    // check we can still access relationship
    REQUIRE(encryptedEvent.relationship() == std::pair<std::string, std::string>{"moe.kazv.mxc.custom-rel-type", "$some-event-id"});

    // check that the relationship is also in plaintext
    REQUIRE(encryptedEvent.originalJson().get()["content"]["m.relates_to"] == json{
        {"rel_type", "moe.kazv.mxc.custom-rel-type"},
        {"event_id", "$some-event-id"},
    });
}

TEST_CASE("encrypting event without relationship should not put m.relates_to key in plaintext", "[client][encryption]")
{
    auto room = makeRoom(withRoomEncrypted(true));
    auto client = makeClient(
        withCrypto(makeCrypto())
        | withRoom(room)
    );

    auto eventToEncrypt = makeEvent(
        withEventType("m.room.message")
    );

    auto [encryptedEvent, maybeKey] = client.megOlmEncrypt(
        eventToEncrypt,
        room.roomId,
        0,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize())
    );

    REQUIRE(maybeKey.has_value());

    REQUIRE(!encryptedEvent.originalJson().get()["content"].contains("m.relates_to"));
}

TEST_CASE("ClientModel::olmEncryptSplit()", "[client][encryption]")
{
    auto r = createE2EESession();
    auto client = r.sender;
    auto receiver1 = r.receiver1.constCrypto();
    auto receiver2 = r.receiver2.constCrypto();

    // encrypt
    auto res = client.olmEncryptSplit(Event(json::object()),
        {{"@receiver:example.com", {"device1", "device2"}}},
        genRandomData(Crypto::encryptOlmMaxRandomSize() * 2));

    REQUIRE(res["@receiver:example.com"]["device1"].originalJson().get().at("content").at("ciphertext").size() == 1);
    REQUIRE(res["@receiver:example.com"]["device1"].originalJson().get().at("content").at("ciphertext").contains(receiver1.curve25519IdentityKey()));
    REQUIRE(res["@receiver:example.com"]["device2"].originalJson().get().at("content").at("ciphertext").size() == 1);
    REQUIRE(res["@receiver:example.com"]["device2"].originalJson().get().at("content").at("ciphertext").contains(receiver2.curve25519IdentityKey()));
}

TEST_CASE("tryDecryptEvents()", "[client][encryption]")
{
    auto roomId = "!someroom:example.com";
    auto room = makeRoom(
        withRoomEncrypted(true)
        | withRoomId(roomId)
    );
    auto client = makeClient(
        withCrypto(makeCrypto())
        | withRoom(room)
    );

    auto plainText = makeEvent();
    auto [encrypted, sessionId] = client.megOlmEncrypt(plainText, roomId, 1719196953000,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize()));
    auto plainText2 = makeEvent();
    // verify that we can decrypt events without sender_key or device_id
    auto [encrypted2, sessionId2] = client.megOlmEncrypt(plainText2, roomId, 1719196953000,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize()));
    auto j = encrypted2.originalJson().get();
    j["content"].erase("sender_key");
    j["content"].erase("device_id");
    encrypted2 = Event(j);

    auto events = EventList{
        makeEvent(),
        makeEvent(),
        encrypted,
        encrypted2,
    };

    withRoomTimeline(events)(room);
    withRoom(room)(client);

    auto nextClient = tryDecryptEvents(client);
    auto decryptedEvent = nextClient.roomList.rooms[roomId].messages[encrypted.id()];
    REQUIRE(decryptedEvent.encrypted());
    REQUIRE(decryptedEvent.decrypted());
    REQUIRE(decryptedEvent.type() == plainText.type());
    REQUIRE(decryptedEvent.content() == plainText.content());

    auto decryptedEvent2 = nextClient.roomList.rooms[roomId].messages[encrypted2.id()];
    REQUIRE(decryptedEvent2.encrypted());
    REQUIRE(decryptedEvent2.decrypted());
    REQUIRE(decryptedEvent2.type() == plainText2.type());
    REQUIRE(decryptedEvent2.content() == plainText2.content());

    REQUIRE(nextClient.roomList.rooms[roomId].undecryptedEvents
        ==
        immer::map<std::string, immer::flex_vector<std::string>>{});
}

TEST_CASE("tryDecryptEvents() will decrypt to-device events and add group session key", "[client][encryption]")
{
    auto r = createE2EESession();
    auto sender = r.sender;
    auto receiver = r.receiver1;
    std::string roomId = "!someroom:example.com";

    Event plainText = json{
        {"type", "m.room.message"},
        {"content", {
            {"body", "mew"},
        }},
        {"room_id", roomId},
    };

    auto [encryptedRoomEvent, encryptedKeyEvent, unencryptedKeyEvent] = createMegOlmSession(sender, receiver, roomId, plainText);
    auto sessionId = encryptedRoomEvent.originalJson().get()["content"]["session_id"].template get<std::string>();

    SECTION("Process key event") {
        auto resp = syncResponseFromToDevice(encryptedKeyEvent);
        auto [next, _dontCareEffect] = ClientModel::update(receiver, ProcessResponseAction{resp});

        REQUIRE(next.constCrypto().hasInboundGroupSession(KeyOfGroupSession{roomId, sessionId}));
        REQUIRE(next.toDevice.size() == 0);
    }

    SECTION("Reject unencrypted key event") {
        auto resp = syncResponseFromToDevice(unencryptedKeyEvent);
        auto [next, _dontCareEffect] = ClientModel::update(receiver, ProcessResponseAction{resp});

        REQUIRE(!next.constCrypto().hasInboundGroupSession(KeyOfGroupSession{roomId, sessionId}));
        REQUIRE(next.toDevice.size() == 0);
    }
}

TEST_CASE("tryDecryptEvents() will update room.undecryptedEvents", "[client][encryption]")
{
    auto roomId = "!someroom:example.com";
    auto room = makeRoom(
        withRoomEncrypted(true)
        | withRoomId(roomId)
    );
    auto client = makeClient(
        withCrypto(makeCrypto())
        | withRoom(room)
    );

    auto plainText = makeEvent();
    auto [encrypted, sessionId] = client.megOlmEncrypt(plainText, roomId, 1719196953000,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize()));
    auto plainText2 = makeEvent();
    auto [encrypted2, sessionId2] = client.megOlmEncrypt(plainText2, roomId, 1719196953000,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize()));
    auto j = encrypted2.originalJson().get();
    // simulate an undecryptable event with a known session id
    j["content"]["/////"];
    encrypted2 = Event(j);
    // simulate an undecryptable event with an unknown session id
    auto plainText3 = makeEvent();
    auto [encrypted3, sessionId3] = client.megOlmEncrypt(plainText3, roomId, 1719196953000,
        genRandomData(EncryptMegOlmEventAction::maxRandomSize()));
    j["content"]["session_id"] = "some-session-id";
    encrypted3 = Event(j);

    auto events = EventList{
        makeEvent(),
        makeEvent(),
        encrypted,
        encrypted2,
        encrypted3,
    };

    withRoomTimeline(events)(room);
    withRoom(room)(client);

    auto nextClient = tryDecryptEvents(client);
    auto decryptedEvent = nextClient.roomList.rooms[roomId].messages[encrypted.id()];
    REQUIRE(decryptedEvent.encrypted());
    REQUIRE(decryptedEvent.decrypted());
    REQUIRE(decryptedEvent.type() == plainText.type());
    REQUIRE(decryptedEvent.content() == plainText.content());

    auto decryptedEvent2 = nextClient.roomList.rooms[roomId].messages[encrypted2.id()];
    REQUIRE(decryptedEvent2.encrypted());
    REQUIRE(!decryptedEvent2.decrypted());

    auto decryptedEvent3 = nextClient.roomList.rooms[roomId].messages[encrypted2.id()];
    REQUIRE(decryptedEvent3.encrypted());
    REQUIRE(!decryptedEvent3.decrypted());

    REQUIRE(nextClient.roomList.rooms[roomId].undecryptedEvents
        ==
        immer::map<std::string, immer::flex_vector<std::string>>{
            {encrypted.originalJson().get()["content"]["session_id"], {encrypted2.id()}},
            {encrypted3.originalJson().get()["content"]["session_id"], {encrypted3.id()}},
        });
}
