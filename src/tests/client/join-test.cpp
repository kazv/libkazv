/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <catch2/catch_all.hpp>
#include <boost/asio.hpp>
#include <asio-promise-handler.hpp>
#include <cursorutil.hpp>
#include <sdk-model.hpp>
#include <client/client.hpp>

#include "client-test-util.hpp"
#include "factory.hpp"

using namespace Kazv::Factory;

TEST_CASE("Send join job with alias", "[client][membership]")
{
    ClientModel loggedInModel = makeClient({});
    auto [resModel, dontCareEffect] = ClientModel::update(
        loggedInModel, JoinRoomAction{"#room:example.com", {}});

    assert1Job(resModel);
    for1stJob(resModel, [] (const auto &job) {
        REQUIRE(job.jobId() == "JoinRoom");
        REQUIRE(job.url().find("%23room:example.com") != std::string::npos);
    });
}

TEST_CASE("Send join job with id", "[client][membership]")
{
    ClientModel loggedInModel = makeClient({});
    auto [resModel, dontCareEffect] = ClientModel::update(
        loggedInModel, JoinRoomAction{"!room:example.com", {}});

    assert1Job(resModel);
    for1stJob(resModel, [] (const auto &job) {
        REQUIRE(job.jobId() == "JoinRoom");
        REQUIRE(job.url().find("!room:example.com") != std::string::npos);
    });
}
