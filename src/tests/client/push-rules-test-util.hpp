/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>

#include <types.hpp>

using namespace Kazv;

// adapted from https://spec.matrix.org/v1.8/client-server-api/#default-override-rules
inline const auto masterRule = R"({
    "rule_id": ".m.rule.master",
    "default": true,
    "enabled": true,
    "conditions": [],
    "actions": []
})"_json;

inline const auto suppressNoticesRule = R"({
    "rule_id": ".m.rule.suppress_notices",
    "default": true,
    "enabled": true,
    "conditions": [
        {
            "kind": "event_match",
            "key": "content.msgtype",
            "pattern": "m.notice"
        }
    ],
    "actions": []
})"_json;

inline const auto inviteForMeRule = R"({
    "rule_id": ".m.rule.invite_for_me",
    "default": true,
    "enabled": true,
    "conditions": [
        {
            "key": "type",
            "kind": "event_match",
            "pattern": "m.room.member"
        },
        {
            "key": "content.membership",
            "kind": "event_match",
            "pattern": "invite"
        },
        {
            "key": "state_key",
            "kind": "event_match",
            "pattern": "@foo:example.com"
        }
    ],
    "actions": [
       "notify",
        {
            "set_tweak": "sound",
            "value": "default"
        }
    ]
})"_json;

inline const auto memberEventRule = R"({
    "rule_id": ".m.rule.member_event",
    "default": true,
    "enabled": true,
    "conditions": [
        {
            "key": "type",
            "kind": "event_match",
            "pattern": "m.room.member"
        }
    ],
    "actions": []
})"_json;

inline const auto isUserMentionRule = R"({
    "rule_id": ".m.rule.is_user_mention",
    "default": true,
    "enabled": true,
    "conditions": [
        {
            "kind": "event_property_contains",
            "key": "content.m\\.mentions.user_ids",
            "value": "@foo:example.com"
        }
    ],
    "actions": [
        "notify",
        {
            "set_tweak": "sound",
            "value": "default"
        },
        {
            "set_tweak": "highlight",
            "value": true
        }
    ]
})"_json;

inline const auto callRule = R"({
    "rule_id": ".m.rule.call",
    "default": true,
    "enabled": true,
    "conditions": [
        {
            "key": "type",
            "kind": "event_match",
            "pattern": "m.call.invite"
        }
    ],
    "actions": [
        "notify",
        {
            "set_tweak": "sound",
            "value": "ring"
        }
    ]
})"_json;

inline const auto encryptedOneToOneRule = R"({
    "rule_id": ".m.rule.encrypted_room_one_to_one",
    "default": true,
    "enabled": true,
    "conditions": [
        {
            "kind": "room_member_count",
            "is": "2"
        },
        {
            "kind": "event_match",
            "key": "type",
            "pattern": "m.room.encrypted"
        }
    ],
    "actions": [
        "notify",
        {
            "set_tweak": "sound",
            "value": "default"
        }
    ]
})"_json;

inline const auto oneToOneRule = R"({
    "rule_id": ".m.rule.room_one_to_one",
    "default": true,
    "enabled": true,
    "conditions": [
        {
            "kind": "room_member_count",
            "is": "2"
        },
        {
            "kind": "event_match",
            "key": "type",
            "pattern": "m.room.message"
        }
    ],
    "actions": [
        "notify",
        {
            "set_tweak": "sound",
            "value": "default"
        }
    ]
})"_json;

inline const auto messageRule = R"({
     "rule_id": ".m.rule.message",
     "default": true,
     "enabled": true,
     "conditions": [
         {
             "kind": "event_match",
             "key": "type",
             "pattern": "m.room.message"
         }
     ],
     "actions": [
         "notify"
     ]
})"_json;

inline const auto encryptedRule = R"({
     "rule_id": ".m.rule.encrypted",
     "default": true,
     "enabled": true,
     "conditions": [
         {
             "kind": "event_match",
             "key": "type",
             "pattern": "m.room.encrypted"
         }
     ],
     "actions": [
         "notify"
     ]
})"_json;

inline const auto catchAllRule = R"({
    "rule_id": "moe.kazv.mxc.catch_all",
    "default": true,
    "enabled": true,
    "conditions": [],
    "actions": ["notify"]
})"_json;

inline const auto emptyRulesContent = json{
    {"global", {
        {"override", {}},
        {"content", {}},
        {"room", {}},
        {"sender", {}},
        {"underride", {}},
    }},
};
