/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>

#include <push-rules-desc-p.hpp>
#include <factory.hpp>

#include "push-rules-test-util.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

static Event makePushRulesEvent(const json &pushRulesContent)
{
    return makeEvent(
        withEventContent(pushRulesContent)
        | withEventType("m.push_rules")
    );
}

TEST_CASE("splitPath()", "[client][push-rules]")
{
    REQUIRE(splitPath("content.body") == std::vector<std::string>{"content", "body"});
    REQUIRE(splitPath("content.m\\.relates_to") == std::vector<std::string>{"content", "m.relates_to"});
    REQUIRE(splitPath("content.m\\\\foo") == std::vector<std::string>{"content", "m\\foo"});
    REQUIRE(splitPath("content.m\\x") == std::vector<std::string>{"content", "m\\x"});
}

TEST_CASE("matchGlob()", "[client][push-rules]")
{
    REQUIRE(matchGlob("m.notice", "m.notice"));
    REQUIRE(!matchGlob("m.notice", "m.+notice"));
    REQUIRE(!matchGlob("m.notice", "m.(notice)"));
    REQUIRE(!matchGlob("mxnotice", "m.notice"));
    REQUIRE(!matchGlob("m.notice", "^m.notice"));
    REQUIRE(!matchGlob("m.notice", "m.notice$"));
    REQUIRE(!matchGlob("m.notice", "m.notice|m.notice"));
    REQUIRE(!matchGlob("m.notice", "m.notic[e]"));
    REQUIRE(!matchGlob("m.notice", "m.notic\\e"));

    REQUIRE(matchGlob("m\nnotice", "m?notice"));
    REQUIRE(matchGlob("Lunch plans", "lunc?*"));
    REQUIRE(matchGlob("LUNCH", "lunc?*"));
    REQUIRE(!matchGlob(" lunch", "lunc?*"));
    REQUIRE(!matchGlob("lunc", "lunc?*"));
}

TEST_CASE("validateRule()", "[client][push-rules]")
{
    json rule = masterRule;

    SECTION("minimum push rule")
    {
        auto validated = validateRule("override", rule);
        REQUIRE(validated == std::make_pair(true, rule));
    }

    SECTION("strips extra properties")
    {
        rule["something"] = "else";
        auto validated = validateRule("override", rule);
        REQUIRE(validated == std::make_pair(true, masterRule));
    }

    SECTION("must have rule_id")
    {
        rule.erase("rule_id");
        auto validated = validateRule("override", rule);
        REQUIRE(!validated.first);
    }

    SECTION("must have default")
    {
        rule.erase("default");
        auto validated = validateRule("override", rule);
        REQUIRE(!validated.first);
    }

    SECTION("must have enabled")
    {
        rule.erase("enabled");
        auto validated = validateRule("override", rule);
        REQUIRE(!validated.first);
    }

    SECTION("must have conditions array")
    {
        rule["conditions"] = json::object();
        auto validated = validateRule("override", rule);
        REQUIRE(!validated.first);
    }

    SECTION("add empty array if conditions not present")
    {
        rule.erase("conditions");
        auto validated = validateRule("override", rule);
        REQUIRE(validated.first);
        REQUIRE(validated.second == masterRule);
    }

    SECTION("reject if there is unknown condition")
    {
        rule["conditions"] = json::array({{{"kind", "moe.kazv.mxc.unknown-condition"}}});
        auto validated = validateRule("override", rule);
        REQUIRE(!validated.first);
    }

    SECTION("event_match condition")
    {
        rule["conditions"] = json::array({{
            {"kind", "event_match"},
            {"key", "some"},
            {"pattern", "patt*n"},
        }});

        auto validated = validateRule("override", rule);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);

        auto ruleWithExtraProp = rule;
        ruleWithExtraProp["conditions"][0]["some"] = "thing";

        validated = validateRule("override", ruleWithExtraProp);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);
    }

    SECTION("room_member_count condition")
    {
        rule["conditions"] = json::array({{
            {"kind", "room_member_count"},
            {"is", "2"},
        }});

        auto validated = validateRule("override", rule);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);

        rule["conditions"][0]["is"] = "==2";
        validated = validateRule("override", rule);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);

        rule["conditions"][0]["is"] = ">2";
        validated = validateRule("override", rule);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);

        rule["conditions"][0]["is"] = "2!";
        validated = validateRule("override", rule);
        REQUIRE(!validated.first);

        rule["conditions"][0]["is"] = "!=2";
        validated = validateRule("override", rule);
        REQUIRE(!validated.first);

        rule["conditions"][0]["is"] = "017";
        validated = validateRule("override", rule);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);
    }

    SECTION("action set_tweak sound")
    {
        rule["actions"] = json::array({{
            {"set_tweak", "sound"},
            {"value", "default"},
        }});

        auto validated = validateRule("override", rule);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);

        auto ruleWithoutValue = rule;
        ruleWithoutValue["actions"][0].erase("value");

        validated = validateRule("override", ruleWithoutValue);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);
    }

    SECTION("action set_tweak highlight")
    {
        rule["actions"] = json::array({{
            {"set_tweak", "highlight"},
            {"value", true},
        }});

        auto validated = validateRule("override", rule);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);

        auto ruleWithoutValue = rule;
        ruleWithoutValue["actions"][0].erase("value");

        validated = validateRule("override", ruleWithoutValue);
        REQUIRE(validated.first);
        REQUIRE(validated.second == rule);
    }
}

TEST_CASE("validatePushRules()", "[client][push-rules]")
{
    json content = {
        {"global", {
            {"override", {masterRule, suppressNoticesRule, isUserMentionRule}},
            {"content", {}},
            {"room", {}},
            {"sender", {}},
            {"underride", {}},
        }},
    };

    auto e = Event{json{
        {"type", "m.push_rules"},
        {"content", content},
    }};

    SECTION("nothing unrecognized")
    {
        auto validated = validatePushRules(e);
        REQUIRE(validated == e);
    }

    SECTION("extra param in content")
    {
        content["something"] = "else";
        auto extra = Event{json{
            {"type", "m.push_rules"},
            {"content", content},
        }};
        auto validated = validatePushRules(extra);
        REQUIRE(validated == e);
    }

    SECTION("extra param in global")
    {
        content["global"]["something"] = "else";
        auto extra = Event{json{
            {"type", "m.push_rules"},
            {"content", content},
        }};
        auto validated = validatePushRules(extra);
        REQUIRE(validated == e);
    }

    SECTION("extra param in rule")
    {
        content["global"]["override"][0]["something"] = "else";
        auto extra = Event{json{
            {"type", "m.push_rules"},
            {"content", content},
        }};
        auto validated = validatePushRules(extra);
        REQUIRE(validated == e);
    }

    SECTION("extra param in condition")
    {
        content["global"]["override"][1]["conditions"][0]["something"] = "else";
        auto extra = Event{json{
            {"type", "m.push_rules"},
            {"content", content},
        }};
        auto validated = validatePushRules(extra);
        REQUIRE(validated == e);
    }

    SECTION("unrecognized condition")
    {
        content["global"]["override"][1]["conditions"].push_back(json::object({{"kind", "moe.kazv.mxc.unknown-condition"}}));
        auto extra = Event{json{
            {"type", "m.push_rules"},
            {"content", content},
        }};

        content["global"]["override"].erase(1);
        auto expected = Event{json{
            {"type", "m.push_rules"},
            {"content", content},
        }};
        auto validated = validatePushRules(extra);
        REQUIRE(validated == expected);
    }

    SECTION("unrecognized action")
    {
        content["global"]["override"][1]["actions"].push_back("moe.kazv.mxc.unknown-action");
        auto extra = Event{json{
            {"type", "m.push_rules"},
            {"content", content},
        }};
        auto validated = validatePushRules(extra);
        REQUIRE(validated == e);
    }
}

TEST_CASE("PushRulesDescPrivate::matchP()", "[client][push-rules]")
{
    PushRulesDescPrivate rp{Event()};
    auto room = makeRoom();

    SECTION("master rule")
    {
        REQUIRE(rp.matchP("override", masterRule, makeEvent(), room));
    }

    SECTION("suppress notices rule")
    {
        auto notice = makeEvent(withEventContent(json{{"msgtype", "m.notice"}}));
        REQUIRE(rp.matchP("override", suppressNoticesRule, notice, room));

        auto notNotice = makeEvent(withEventContent(json{{"msgtype", "m.noticexxx"}}));
        REQUIRE(!rp.matchP("override", suppressNoticesRule, notNotice, room));
    }

    SECTION("invite for me rule")
    {
        auto invite = makeEvent(
            withEventContent(json{{"membership", "invite"}})
            | withEventType("m.room.member")
            | withStateKey("@foo:example.com")
        );
        REQUIRE(rp.matchP("override", inviteForMeRule, invite, room));

        auto inviteForOthers = makeEvent(
            withEventContent(json{{"membership", "invite"}})
            | withEventType("m.room.member")
            | withStateKey("@foo:example.com.tw")
        );
        REQUIRE(!rp.matchP("override", inviteForMeRule, inviteForOthers, room));
    }

    SECTION("member event rule")
    {
        auto member = makeEvent(
            withEventContent(json{{"membership", "join"}})
            | withEventType("m.room.member")
            | withStateKey("@foo:example.com")
        );
        REQUIRE(rp.matchP("override", memberEventRule, member, room));
    }

    SECTION("is user mention rule")
    {
        auto mentionedEvent = makeEvent(
            withEventContent(json{
                {"m.mentions", {
                    {"user_ids", {"@foo:example.com"}}
                }}
            })
        );

        auto notMentionedEvent = makeEvent(
            withEventContent(json{
                {"m.mentions", {
                    {"user_ids", {"@foo:example.com.tw"}}
                }}
            })
        );

        REQUIRE(rp.matchP("override", isUserMentionRule, mentionedEvent, room));
        REQUIRE(!rp.matchP("override", isUserMentionRule, notMentionedEvent, room));
    }

    SECTION("one to one rule")
    {
        auto event = makeEvent();
        auto withJoinedMemberCount = [](auto count) {
            return [count](RoomModel &r) {
                r.joinedMemberCount = count;
            };
        };
        auto withInvitedMemberCount = [](auto count) {
            return [count](RoomModel &r) {
                r.invitedMemberCount = count;
            };
        };
        SECTION("no one in room")
        {
            REQUIRE(!rp.matchP("override", oneToOneRule, event, room));
        }

        SECTION("two people in room")
        {
            withJoinedMemberCount(2)(room);
            REQUIRE(rp.matchP("override", oneToOneRule, event, room));
        }

        SECTION("two joined and one invited")
        {
            withJoinedMemberCount(2)(room);
            withInvitedMemberCount(1)(room);
            REQUIRE(rp.matchP("override", oneToOneRule, event, room));
        }

        SECTION("three joined")
        {
            withJoinedMemberCount(3)(room);
            REQUIRE(!rp.matchP("override", oneToOneRule, event, room));
        }
    }
}

TEST_CASE("Push rules works with predefined rules", "[client][push-rules]")
{
    auto rulesContent = emptyRulesContent;
    auto room = makeRoom();

    SECTION("master rule")
    {
        rulesContent["global"]["override"].push_back(masterRule);
        rulesContent["global"]["underride"].push_back(catchAllRule);
        auto pushRules = PushRulesDesc(makePushRulesEvent(rulesContent));
        auto result = pushRules.handle(makeEvent(), room);
        REQUIRE(!result.shouldNotify);
    }

    SECTION("suppress notices rule")
    {
        rulesContent["global"]["override"].push_back(suppressNoticesRule);
        rulesContent["global"]["underride"].push_back(catchAllRule);
        auto pushRules = PushRulesDesc(makePushRulesEvent(rulesContent));
        auto result = pushRules.handle(makeEvent(withEventContent(json{{"msgtype", "m.notice"}})), room);
        REQUIRE(!result.shouldNotify);
    }

    SECTION("invite for me rule")
    {
        rulesContent["global"]["override"].push_back(inviteForMeRule);
        auto pushRules = PushRulesDesc(makePushRulesEvent(rulesContent));
        auto result = pushRules.handle(makeEvent(
            withEventContent(json{{"membership", "invite"}})
            | withEventType("m.room.member")
            | withStateKey("@foo:example.com")
        ), room);
        REQUIRE(result.shouldNotify);
    }

    SECTION("member event rule")
    {
        rulesContent["global"]["override"].push_back(memberEventRule);
        rulesContent["global"]["underride"].push_back(catchAllRule);
        auto pushRules = PushRulesDesc(makePushRulesEvent(rulesContent));
        auto result = pushRules.handle(makeEvent(
            withEventContent(json{{"membership", "invite"}})
            | withEventType("m.room.member")
            | withStateKey("@foo:example.com")
        ), room);
        REQUIRE(!result.shouldNotify);
    }

    SECTION("is user mention rule")
    {
        auto mentionedEvent = makeEvent(
            withEventContent(json{
                {"m.mentions", {
                    {"user_ids", {"@foo:example.com"}}
                }}
            })
        );

        auto notMentionedEvent = makeEvent(
            withEventContent(json{
                {"m.mentions", {
                    {"user_ids", {"@foo:example.com.tw"}}
                }}
            })
        );

        rulesContent["global"]["override"].push_back(isUserMentionRule);
        auto pushRules = PushRulesDesc(makePushRulesEvent(rulesContent));

        REQUIRE(pushRules.handle(mentionedEvent, room).shouldNotify);
        REQUIRE(!pushRules.handle(notMentionedEvent, room).shouldNotify);
    }

    SECTION("one to one rule")
    {
        auto event = makeEvent();
        rulesContent["global"]["override"].push_back(oneToOneRule);
        auto pushRules = PushRulesDesc(makePushRulesEvent(rulesContent));

        SECTION("no one in room")
        {
            REQUIRE(!pushRules.handle(event, room).shouldNotify);
        }

        SECTION("two people in room")
        {
            room.joinedMemberCount = 2;
            REQUIRE(pushRules.handle(event, room).shouldNotify);
        }
    }
}

TEST_CASE("Push rules would not crash with broken rules", "[client][push-rules]")
{
    auto rulesContent = emptyRulesContent;
    auto room = makeRoom();
    REQUIRE(!PushRulesDesc(Event()).handle(makeEvent(), room).shouldNotify);

    SECTION("rule missing required param")
    {
        auto rule = catchAllRule;
        rule.erase("rule_id");
        rulesContent["global"]["override"].push_back(rule);
        REQUIRE(!PushRulesDesc(makePushRulesEvent(rulesContent)).handle(makeEvent(), room).shouldNotify);
    }

    SECTION("rule containing invalid condition")
    {
        auto rule = catchAllRule;
        rule["conditions"].push_back("something");
        rulesContent["global"]["override"].push_back(rule);
        REQUIRE(!PushRulesDesc(makePushRulesEvent(rulesContent)).handle(makeEvent(), room).shouldNotify);
    }

    SECTION("rule containing unknown action")
    {
        auto rule = catchAllRule;
        rule["actions"].push_back("something");
        rulesContent["global"]["override"].push_back(rule);
        REQUIRE(PushRulesDesc(makePushRulesEvent(rulesContent)).handle(makeEvent(), room).shouldNotify);
    }

    SECTION("action set_tweak sound")
    {
        auto rule = catchAllRule;
        rule["actions"].push_back(json{
            {"set_tweak", "sound"},
            {"value", "some"},
        });
        rulesContent["global"]["override"].push_back(rule);
        REQUIRE(PushRulesDesc(makePushRulesEvent(rulesContent)).handle(makeEvent(), room).sound == "some");
    }

    SECTION("action set_tweak highlight")
    {
        auto rule = catchAllRule;
        rule["actions"].push_back(json{
            {"set_tweak", "highlight"},
            {"value", true},
        });
        rulesContent["global"]["override"].push_back(rule);
        REQUIRE(PushRulesDesc(makePushRulesEvent(rulesContent)).handle(makeEvent(), room).shouldHighlight);
    }
}
