/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>
#include <session.hpp>
#include <crypto.hpp>
#include "crypto-test-resource.hpp"

using namespace Kazv;

static const auto resource = cryptoDumpResource();
static const auto sessionAJson = resource["a"]["knownSessions"]["wklSkg9T1gz0pnYDZUmKrVqQXEDGW8HnQV0JDWqzYVs"];
static const auto sessionBJson = resource["b"]["knownSessions"]["L75F3cTDIM/bw/n11zm73o0+LpNZ5ll+FCbCH8+OETQ"];

TEST_CASE("Session conversion from libolm to vodozemac")
{
    auto sessionA = sessionAJson.template get<Session>();

    auto sessionB = sessionBJson.template get<Session>();

    REQUIRE(sessionA.valid());
    REQUIRE(sessionB.valid());

    {
        auto [type, message] = sessionA.encryptWithRandom(genRandomData(sessionA.encryptRandomSize()), "mew 1");
        auto res = sessionB.decrypt(type, message);
        REQUIRE(res.has_value());
        REQUIRE(res.value() == "mew 1");
    }

    {
        auto [type, message] = sessionB.encryptWithRandom(genRandomData(sessionA.encryptRandomSize()), "mew 2");
        auto res = sessionA.decrypt(type, message);
        REQUIRE(res.has_value());
        REQUIRE(res.value() == "mew 2");
    }
}

TEST_CASE("Session::from_json error handling")
{
    auto j = sessionAJson;
    j["data"] = "AAAAAAAAAA";
    auto sessionA = j.template get<Session>();

    REQUIRE(!sessionA.valid());
}

TEST_CASE("Session::decrypt error handling")
{
    auto sessionA = sessionAJson.template get<Session>();

    auto res = sessionA.decrypt(1, "xxx");
    REQUIRE(!res.has_value());
}

TEST_CASE("Session serialization roundtrip")
{
    auto sessionA = sessionAJson.template get<Session>();
    auto j = json(sessionA);
    sessionA = j.template get<Session>();

    auto sessionB = sessionBJson.template get<Session>();
    j = json(sessionB);
    sessionB = j.template get<Session>();

    REQUIRE(sessionA.valid());
    REQUIRE(sessionB.valid());

    {
        auto [type, message] = sessionA.encryptWithRandom(genRandomData(sessionA.encryptRandomSize()), "mew 1");
        auto res = sessionB.decrypt(type, message);
        REQUIRE(res.has_value());
        REQUIRE(res.value() == "mew 1");
    }
}
