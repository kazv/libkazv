/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>
#include <outbound-group-session.hpp>
#include <crypto.hpp>
#include "crypto-test-resource.hpp"

using namespace Kazv;

static const auto resource = cryptoDumpResource();

TEST_CASE("OutboundGroupSession conversion from libolm to vodozemac")
{
    auto sessionJson = resource["a"]["outboundGroupSessions"]["!foo:example.com"];
    auto session = sessionJson.template get<OutboundGroupSession>();
    REQUIRE(session.valid());
    REQUIRE(session.initialSessionKey() == sessionJson["initialSessionKey"]);
}

TEST_CASE("OutboundGroupSession serialization roundtrip")
{
    auto session = OutboundGroupSession(RandomTag{}, genRandomData(OutboundGroupSession::constructRandomSize()), 0);
    json j = session;
    auto session2 = j.template get<OutboundGroupSession>();
    REQUIRE(session2.valid());
    REQUIRE(session.initialSessionKey() == session2.initialSessionKey());
    REQUIRE(session.sessionId() == session2.sessionId());
}

TEST_CASE("OutboundGroupSession::from_json error handling")
{
    auto sessionJson = resource["a"]["outboundGroupSessions"]["!foo:example.com"];
    sessionJson["session"] = "AAAAAAAAAA";
    auto session = sessionJson.template get<OutboundGroupSession>();
    REQUIRE(!session.valid());
}

TEST_CASE("OutboundGroupSession ctor")
{
    auto session = OutboundGroupSession();
    REQUIRE(!session.valid());
}

TEST_CASE("OutboundGroupSession::encrypt error handling")
{
    // invalid utf8 will cause rust::Str to throw
    // https://stackoverflow.com/questions/1301402/example-invalid-utf8-string
    std::string plainText = "\xc3\x28";
    auto session = OutboundGroupSession(RandomTag{}, genRandomData(OutboundGroupSession::constructRandomSize()), 0);
    auto originalIndex = session.messageIndex();
    auto res = session.encrypt(plainText);
    REQUIRE(res.empty());
    REQUIRE(originalIndex == session.messageIndex());
}

TEST_CASE("invalid OutboundGroupSession is copyable")
{
    OutboundGroupSession session;
    REQUIRE(!session.valid());
    auto session2 = session;
    REQUIRE(!session.valid());
}
