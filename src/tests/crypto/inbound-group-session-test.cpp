/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <catch2/catch_test_macros.hpp>
#include <inbound-group-session.hpp>
#include <outbound-group-session.hpp>
#include <crypto.hpp>
#include "crypto-test-resource.hpp"

using namespace Kazv;

static const auto resource = cryptoDumpResource();

TEST_CASE("InboundGroupSession conversion from libolm to vodozemac")
{
    auto sessionJson = resource["a"]["inboundGroupSessions"][0][1];
    auto session = sessionJson.template get<InboundGroupSession>();
    REQUIRE(session.valid());
    REQUIRE(session.ed25519Key() == sessionJson["ed25519Key"]);
    auto encrypted = resource["megolmEncrypted"];
    auto plainText = resource["megolmPlainText"];
    auto a = Crypto();
    a.loadJson(resource["a"]);
    auto decrypted = a.decrypt(encrypted);
    REQUIRE(decrypted.has_value());
    auto decryptedJson = json::parse(decrypted.value());
    REQUIRE(decryptedJson == plainText);
}

TEST_CASE("InboundGroupSession::from_json error handling")
{
    auto sessionJson = resource["a"]["inboundGroupSessions"][0][1];
    sessionJson["session"] = "AAAAAAAAAA";
    auto session = sessionJson.template get<InboundGroupSession>();
    REQUIRE(!session.valid());
}

TEST_CASE("InboundGroupSession::decrypt error handling")
{
    auto sessionJson = resource["a"]["inboundGroupSessions"][0][1];
    auto session = sessionJson.template get<InboundGroupSession>();
    WHEN("message not decryptable") {
        auto res = session.decrypt("AAAAAA", "$1", 1234);
        REQUIRE(!res);
    }

    WHEN("message is not valid base64") {
        auto res = session.decrypt("喵喵喵", "$1", 1234);
        REQUIRE(!res);
    }

    WHEN("message is before the index") {
        auto ogs = OutboundGroupSession(RandomTag{}, genRandomData(OutboundGroupSession::constructRandomSize()), 0);
        auto encrypted1 = ogs.encrypt("text");
        auto igs = InboundGroupSession(ogs.sessionKey(), "placeholder");
        auto res = igs.decrypt(encrypted1, "$1", 1234);
        REQUIRE(!res.has_value());
    }
}

TEST_CASE("InboundGroupSession constructor error handling")
{
    WHEN("key not valid") {
        auto session = InboundGroupSession("AAAAAA", "ed25519Key");
        REQUIRE(!session.valid());
    }

    WHEN("key is not valid base64") {
        auto session = InboundGroupSession("喵喵喵", "ed25519Key");
        REQUIRE(!session.valid());
    }
}

TEST_CASE("invalid InboundGroupSession is copyable")
{
    InboundGroupSession session("AAAAAA", "ed25519Key");
    REQUIRE(!session.valid());
    auto session2 = session;
    REQUIRE(!session.valid());
}
