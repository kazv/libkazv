/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>
#include <fstream>
#include <streambuf>
#include <nlohmann/json.hpp>
#include "kazvtest-respath.hpp"

inline const std::string cryptoDumpPath = resPath + "/libolm-crypto-dump";

inline nlohmann::json getCryptoDumpResource()
{
    std::ifstream s(cryptoDumpPath);
    std::string str((std::istreambuf_iterator<char>(s)),
                 std::istreambuf_iterator<char>());
    return nlohmann::json::parse(str);
}

inline nlohmann::json cryptoDumpResource()
{
    static nlohmann::json res = getCryptoDumpResource();
    return res;
};
