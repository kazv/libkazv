/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "factory.hpp"

namespace Kazv::Factory
{
    static std::string generateUserId()
    {
        static std::size_t next = 0;
        ++next;
        return "@u" + std::to_string(next) + ":example.com";
    }

    static std::string generateEventId()
    {
        static std::size_t next = 0;
        ++next;
        std::stringstream s;
        s << std::setw(40) << std::setfill('0') << next;
        return "$" + s.str();
    }

    static std::string generateRoomId()
    {
        static std::size_t next = 0;
        ++next;
        return "!" + std::to_string(next) + ":example.com";
    }

    static std::string generateDeviceId()
    {
        static std::size_t next = 0;
        ++next;
        return "dev" + std::to_string(next);
    }

    ClientModel makeClient(const ComposedModifier<ClientModel> &mod)
    {
        ClientModel m;
        m.serverUrl = "example.com";
        m.userId = "@bob:example.com";
        m.token = "exampletoken";
        m.deviceId = "exampledevice";
        m.loggedIn = true;
        mod(m);
        return m;
    }

    ComposedModifier<ClientModel> withRoom(RoomModel room)
    {
        return [room](ClientModel &m) {
            m.roomList.rooms = m.roomList.rooms.set(room.roomId, room);
        };
    }

    ComposedModifier<ClientModel> withAccountData(immer::flex_vector<Event> accountDataEvent)
    {
        return [accountDataEvent](ClientModel &m) {
            m.accountData = merge(std::move(m.accountData), accountDataEvent, keyOfAccountData);
        };
    }

    ComposedModifier<ClientModel> withCrypto(const Crypto &crypto)
    {
        return [crypto](ClientModel &m) {
            m.crypto = crypto;
        };
    }

    ComposedModifier<ClientModel> withDevice(std::string userId, DeviceKeyInfo info)
    {
        return [userId, info](ClientModel &m) {
            m.deviceLists.deviceLists = std::move(m.deviceLists.deviceLists)
                .update(userId, [=](auto deviceMap) {
                    return std::move(deviceMap).set(info.deviceId, info);
                });
        };
    }

    DeviceKeyInfo makeDeviceKeyInfo(const ComposedModifier<DeviceKeyInfo> &mod)
    {
        DeviceKeyInfo m;
        mod(m);
        if (m.deviceId.empty()) {
            withDeviceId(generateDeviceId())(m);
        }
        if (m.ed25519Key.empty()) {
            m.ed25519Key = m.deviceId + "ed25519";
        }
        if (m.curve25519Key.empty()) {
            m.curve25519Key = m.deviceId + "curve25519";
        }
        return m;
    }

    ComposedModifier<DeviceKeyInfo> withDeviceId(std::string deviceId)
    {
        return [deviceId](DeviceKeyInfo &m) { m.deviceId = deviceId; };
    }

    ComposedModifier<DeviceKeyInfo> withDeviceDisplayName(std::string displayName)
    {
        return [displayName](DeviceKeyInfo &m) { m.displayName = displayName; };
    }

    ComposedModifier<DeviceKeyInfo> withDeviceTrustLevel(DeviceTrustLevel trustLevel)
    {
        return [trustLevel](DeviceKeyInfo &m) { m.trustLevel = trustLevel; };
    }

    Crypto makeCrypto(const ComposedModifier<Crypto> &mod)
    {
        auto m = Crypto(RandomTag{}, genRandomData(Crypto::constructRandomSize()));
        mod(m);
        return m;
    }

    RoomModel makeRoom(const ComposedModifier<RoomModel> &mod)
    {
        RoomModel room;
        room.roomId = generateRoomId();
        mod(room);
        return room;
    }

    ComposedModifier<RoomModel> withRoomId(std::string id)
    {
        return [id](RoomModel &m) { m.roomId = id; };
    }

    ComposedModifier<RoomModel> withRoomAccountData(immer::flex_vector<Event> accountDataEvent)
    {
        return [accountDataEvent](RoomModel &m) {
            m.accountData = merge(std::move(m.accountData), accountDataEvent, keyOfAccountData);
        };
    }

    ComposedModifier<RoomModel> withRoomState(immer::flex_vector<Event> stateEvent)
    {
        return [stateEvent](RoomModel &m) {
            m.stateEvents = merge(std::move(m.stateEvents), stateEvent, keyOfState);
        };
    }

    ComposedModifier<RoomModel> withRoomTimeline(immer::flex_vector<Event> timelineEvents)
    {
        return [timelineEvents](RoomModel &m) {
            m = RoomModel::update(m, AddToTimelineAction{timelineEvents, std::nullopt, std::nullopt, std::nullopt});
        };
    }

    ComposedModifier<RoomModel> withRoomTimelineGaps(immer::map<std::string, std::string> timelineGaps)
    {
        return [timelineGaps](RoomModel &m) {
            m.timelineGaps = timelineGaps;
        };
    }

    ComposedModifier<RoomModel> withRoomMembership(RoomMembership membership)
    {
        return [membership](RoomModel &m) {
            m.membership = membership;
        };
    }

    ComposedModifier<RoomModel> withRoomEncrypted(bool encrypted)
    {
        return [encrypted](RoomModel &m) {
            m.encrypted = encrypted;
        };
    }

    Event makeEvent(const ComposedModifier<Event> &mod)
    {
        Event e = json{
            {"type", "m.room.message"},
            {"sender", "@foo:tusooa.xyz"},
            {"content", {
                {"msgtype", "m.text"},
                {"body", "test"},
            }},
            {"origin_server_ts", 1000},
            {"event_id", generateEventId()},
        };
        mod(e);
        return e;
    }

    Event makeMemberEvent(const ComposedModifier<Event> &mod)
    {
        auto userId = generateUserId();
        auto event = makeEvent(
            withEventType("m.room.member")
            | withStateKey(userId)
            | withEventContent(json::object())
            | withMembership("join")
            | mod
        );
        if (event.sender().empty()) {
            withEventSenderId(event.stateKey());
        }
        return event;
    }

    ComposedModifier<Event> withEventJson(const json &j)
    {
        return [j](Event &e) {
            e = Event(j);
        };
    }

    ComposedModifier<Event> withEventKV(const json::json_pointer &k, const json &v)
    {
        return [k, v](Event &e) {
            auto j = e.originalJson().get();
            j[k] = v;
            withEventJson(j)(e);
        };
    }

    ComposedModifier<Event> withEventId(std::string id)
    {
        return withEventKV("/event_id"_json_pointer, id);
    }

    ComposedModifier<Event> withEventType(std::string type)
    {
        return withEventKV("/type"_json_pointer, type);
    }

    ComposedModifier<Event> withEventContent(const json &content)
    {
        return withEventKV("/content"_json_pointer, content);
    }

    ComposedModifier<Event> withStateKey(std::string id)
    {
        return withEventKV("/state_key"_json_pointer, id);
    }

    ComposedModifier<Event> withMembership(std::string membership)
    {
        return withEventKV("/content/membership"_json_pointer, membership);
    }

    ComposedModifier<Event> withMemberDisplayName(std::string displayName)
    {
        return withEventKV("/content/displayname"_json_pointer, displayName);
    }

    ComposedModifier<Event> withMemberAvatarUrl(std::string avatarUrl)
    {
        return withEventKV("/content/avatar_url"_json_pointer, avatarUrl);
    }

    ComposedModifier<Event> withEventSenderId(std::string sender)
    {
        return withEventKV("/sender"_json_pointer, sender);
    }

    ComposedModifier<Event> withEventRelationship(std::string relType, std::string eventId)
    {
        return withEventKV("/content/m.relates_to"_json_pointer, {
            {"rel_type", relType},
            {"event_id", eventId},
        });
    }

    ComposedModifier<Event> withEventReplyTo(std::string eventId)
    {
        return withEventKV("/content/m.relates_to"_json_pointer, {
            {"m.in_reply_to", {{"event_id", eventId}}},
        });
    }

    Response makeResponse(std::string jobId, const ComposedModifier<Response> &mod)
    {
        Response r;
        (withResponseStatusCode(200)
            | withResponseJsonBody(json::object())
            | withResponseDataKV("-job-id", jobId)
            | mod)(r);
        return r;
    }

    ComposedModifier<Response> withResponseStatusCode(int code)
    {
        return withAttr(&Response::statusCode, code);
    }

    ComposedModifier<Response> withResponseJsonBody(const json &body)
    {
        return withAttr(&Response::body, JsonWrap(body));
    }

    ComposedModifier<Response> withResponseBytesBody(const Bytes &body)
    {
        return withAttr(&Response::body, body);
    }

    ComposedModifier<Response> withResponseFileBody(const FileDesc &body)
    {
        return withAttr(&Response::body, body);
    }

    ComposedModifier<Response> withResponseDataKV(std::string k, const json &v)
    {
        return [k, v](Response &m) {
            auto data = m.extraData.get();
            if (!data.is_object()) {
                data = json::object();
            }
            data[k] = v;
            m.extraData = data;
        };
    }
}
