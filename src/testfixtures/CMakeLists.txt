add_library(kazvtestfixtures
  factory.cpp
)
add_library(libkazv::kazvtestfixtures ALIAS kazvtestfixtures)
set_target_properties(kazvtestfixtures PROPERTIES VERSION ${libkazv_VERSION_STRING} SOVERSION ${libkazv_SOVERSION})

target_include_directories(
  kazvtestfixtures
  PRIVATE ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(kazvclient
  INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<INSTALL_INTERFACE:include/kazv/testfixtures>
)

target_link_libraries(
  kazvtestfixtures
  PUBLIC kazvclient
)
install(TARGETS kazvtestfixtures EXPORT libkazvTargets LIBRARY)
