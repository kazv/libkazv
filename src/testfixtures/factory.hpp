/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <libkazv-config.hpp>

#include <client-model.hpp>

namespace Kazv
{
    namespace Factory
    {
        template<class ModelType>
        struct ComposedModifier : std::function<void(ModelType &)>
        {
            using BaseT = std::function<void(ModelType &)>;
            using BaseT::BaseT;
            using BaseT::operator();

            ComposedModifier() : BaseT([](ModelType &) {}) {}

            ComposedModifier operator|(const ComposedModifier &that) const
            {
                return ComposedModifier([that, *this](ModelType &model) {
                    (*this)(model);
                    that(model);
                });
            }
        };

        template<class PointerToMember>
        struct AttrModifier
        {};

        template<class ModelType, class DataType>
        struct AttrModifier<DataType ModelType::*> : public ComposedModifier<ModelType>
        {
            using BaseT = ComposedModifier<ModelType>;
            using PtrT = DataType ModelType::*;
            using ModelT = ModelType;
            using DataT = DataType;

            AttrModifier(PtrT ptr, const DataType &data)
                : BaseT([ptr, data](ModelType &m) {
                    m.*ptr = data;
                })
            {}
        };

        template<class PointerToMember>
        AttrModifier<PointerToMember> withAttr(PointerToMember p, const typename AttrModifier<PointerToMember>::DataT &data)
        {
            return AttrModifier<PointerToMember>(p, data);
        }

        ClientModel makeClient(const ComposedModifier<ClientModel> &mod = {});
        ComposedModifier<ClientModel> withRoom(RoomModel room);
        ComposedModifier<ClientModel> withAccountData(immer::flex_vector<Event> accountDataEvent);
        ComposedModifier<ClientModel> withCrypto(const Crypto &crypto);
        ComposedModifier<ClientModel> withDevice(std::string userId, DeviceKeyInfo info);

        DeviceKeyInfo makeDeviceKeyInfo(const ComposedModifier<DeviceKeyInfo> &mod = {});
        ComposedModifier<DeviceKeyInfo> withDeviceId(std::string deviceId);
        ComposedModifier<DeviceKeyInfo> withDeviceDisplayName(std::string displayName);
        ComposedModifier<DeviceKeyInfo> withDeviceTrustLevel(DeviceTrustLevel trustLevel);

        Crypto makeCrypto(const ComposedModifier<Crypto> &mod = {});

        RoomModel makeRoom(const ComposedModifier<RoomModel> &mod = {});

        ComposedModifier<RoomModel> withRoomId(std::string id);
        ComposedModifier<RoomModel> withRoomAccountData(immer::flex_vector<Event> accountDataEvent);
        ComposedModifier<RoomModel> withRoomState(immer::flex_vector<Event> stateEvent);
        ComposedModifier<RoomModel> withRoomInviteState(immer::flex_vector<Event> stateEvent);
        ComposedModifier<RoomModel> withRoomTimeline(immer::flex_vector<Event> timelineEvents);
        ComposedModifier<RoomModel> withRoomTimelineGaps(immer::map<std::string, std::string> timelineGaps);
        ComposedModifier<RoomModel> withRoomMembership(RoomMembership membership);
        ComposedModifier<RoomModel> withRoomEncrypted(bool encrypted);

        Event makeEvent(const ComposedModifier<Event> &mod = {});
        Event makeMemberEvent(const ComposedModifier<Event> &mod = {});
        ComposedModifier<Event> withEventJson(const json &j);
        ComposedModifier<Event> withEventKV(const json::json_pointer &k, const json &v);
        ComposedModifier<Event> withEventId(std::string id);
        ComposedModifier<Event> withEventType(std::string type);
        ComposedModifier<Event> withEventContent(const json &content);
        ComposedModifier<Event> withStateKey(std::string id);
        ComposedModifier<Event> withMembership(std::string membership);
        ComposedModifier<Event> withMemberDisplayName(std::string displayName);
        ComposedModifier<Event> withMemberAvatarUrl(std::string avatarUrl);
        ComposedModifier<Event> withEventSenderId(std::string sender);
        ComposedModifier<Event> withEventRelationship(std::string relType, std::string eventId);
        ComposedModifier<Event> withEventReplyTo(std::string eventId);

        Response makeResponse(std::string jobId, const ComposedModifier<Response> &mod = {});
        ComposedModifier<Response> withResponseStatusCode(int code);
        ComposedModifier<Response> withResponseJsonBody(const json &body);
        ComposedModifier<Response> withResponseBytesBody(const Bytes &body);
        ComposedModifier<Response> withResponseFileBody(const FileDesc &body);
        ComposedModifier<Response> withResponseDataKV(std::string k, const json &v);
    }
}
