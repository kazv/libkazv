/*
 * This file is part of libkazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <libkazv-config.hpp>
#include <iostream>
#include <crypto.hpp>
#include <crypto-util.hpp>
#include <base64.hpp>
#include <types.hpp>

using namespace Kazv;
using namespace Kazv::CryptoConstants;

// This program generates a dump of olm accounts, sessions,
// and megolm sessions. It is intended to be linked against
// the libolm implementation of libkazvcrypto.
//
// The generated file is at src/tests/resources/libolm-crypto-dump .
// It is used to test the conversion from a libolm Crypto
// to a future vodozemac Crypto.

static json convert(std::unordered_map<std::string, RandomData> seeds)
{
    auto res = json::object();
    for (auto [k, v] : seeds) {
        res[k] = encodeBase64(v);
    }
    return res;
}

static void verify(MaybeString s)
{
    if (!s.has_value()) {
        throw std::runtime_error{s.reason()};
    }
}

int main()
{
    auto seeds = std::unordered_map<std::string, RandomData>{};
    seeds["construct1"] = genRandomData(Crypto::constructRandomSize());
    auto a = Crypto(RandomTag{}, seeds["construct1"]);

    seeds["construct2"] = genRandomData(Crypto::constructRandomSize());
    auto b = Crypto(RandomTag{}, seeds["construct2"]);

    seeds["oneTimeKeys1"] = genRandomData(Crypto::genOneTimeKeysRandomSize(1));
    a.genOneTimeKeysWithRandom(seeds["oneTimeKeys1"], 1);

    auto k = a.unpublishedOneTimeKeys();
    a.markOneTimeKeysAsPublished();

    seeds["oneTimeKeys2"] = genRandomData(Crypto::genOneTimeKeysRandomSize(1));
    a.genOneTimeKeysWithRandom(seeds["oneTimeKeys2"], 1);
    auto k2 = a.unpublishedOneTimeKeys();

    auto oneTimeKey = k[curve25519].begin().value();
    auto aIdKey = a.curve25519IdentityKey();
    auto bIdKey = b.curve25519IdentityKey();

    seeds["createOutbound"] = genRandomData(Crypto::createOutboundSessionRandomSize());
    b.createOutboundSessionWithRandom(seeds["createOutbound"], aIdKey, oneTimeKey);

    auto origJson = json{{"test", "mew"}};
    seeds["encryptOlm1"] = genRandomData(Crypto::encryptOlmMaxRandomSize());
    auto encryptedMsg = b.encryptOlmWithRandom(seeds["encryptOlm1"], origJson, aIdKey);
    auto encJson = json{{"content", {
        {"algorithm", olmAlgo},
        {"ciphertext", encryptedMsg},
        {"sender_key", b.curve25519IdentityKey()}
    }}};

    auto decryptedOpt = a.decrypt(encJson);
    verify(decryptedOpt);

    seeds["encryptOlm2"] = genRandomData(Crypto::encryptOlmMaxRandomSize());
    auto encryptedMsg2 = a.encryptOlmWithRandom(seeds["encryptOlm2"], origJson, bIdKey);
    auto encJson2 = json{{"content", {
        {"algorithm", olmAlgo},
        {"ciphertext", encryptedMsg2},
        {"sender_key", a.curve25519IdentityKey()}
    }}};

    decryptedOpt = b.decrypt(encJson2);
    verify(decryptedOpt);

    seeds["rotateMegOlm1"] = genRandomData(Crypto::rotateMegOlmSessionRandomSize());

    a.rotateMegOlmSessionWithRandom(seeds["rotateMegOlm1"], 0, "!foo:example.com");

    auto megolmPlainText = json{
        {"type", "m.room.message"},
        {"room_id", "!foo:example.com"},
        {"content", {
            {"mew", "mew"}
        }}
    };
    auto encryptedContent = a.encryptMegOlm(megolmPlainText);

    auto encrypted = json{
        {"event_id", "$1"},
        {"origin_server_ts", 1234},
        {"room_id", "!foo:example.com"},
        {"content", encryptedContent},
    };

    decryptedOpt = a.decrypt(encrypted);
    verify(decryptedOpt);

    std::cout << json{
        {"seeds", convert(seeds)},
        {"oneTimeKeys1", k},
        {"oneTimeKeys2", k2},
        {"olmPlainText", origJson},
        {"olmEncrypted1", encJson},
        {"olmEncrypted2", encJson2},
        {"megolmPlainText", megolmPlainText},
        {"megolmEncrypted", encrypted},
        {"aIdKey", aIdKey},
        {"a", a.toJson()},
        {"bIdKey", bIdKey},
        {"b", b.toJson()},
    }.dump();
}
